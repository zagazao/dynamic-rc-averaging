FROM ubuntu:18.04

ARG UID=1000
ARG GID=1000
ARG USER_NAME="lukas"

# Limit threads for container.
ENV OMP_NUM_THREADS="1" \
    PX_USE64BIT="True"

RUN apt-get update      && \
    apt-get install -y     \
        python3            \
        python3-pip        \
        vim                \
        wget               \
        git             && \
    rm -rf /var/lib/apt/lists/*

# Install docker (The client is used to run experiments automatically) (Only required since s876home has no pip and py34)
RUN cd /tmp                                                                             && \
    wget https://download.docker.com/linux/static/stable/x86_64/docker-17.06.2-ce.tgz   && \
    tar -xvzf docker-17.06.2-ce.tgz                                                     && \
    mv docker/* /usr/bin                                                                && \
    rm -rf docker-17.06.2-ce.tgz

# Install all requirements beforehand
ADD requirements.txt /tmp/
RUN /usr/bin/python3 -m pip install -r /tmp/requirements.txt

# Check for changes in git
ADD https://api.github.com/repos/zagazao/dlplatform/git/refs/heads/master /tmp/version.json

# Install dlplatform
RUN cd /tmp/                                                                        && \
    git clone https://github.com/zagazao/dlplatform.git                             && \
    cd dlplatform                                                                   && \
    /usr/bin/python3 -m pip install .                                               && \
    cd .. && rm -rf dlplatform

# Install pxpy and dlplatform_extension
ADD . /tmp/dynamic-rc-averaging/
RUN /usr/bin/python3 -m pip install /tmp/dynamic-rc-averaging/lib/pxpy-1.0a22-py3-none-any.whl	&& \
    /usr/bin/python3 -m pip install /tmp/dynamic-rc-averaging/

# Add an user and setup passwords
RUN groupadd --gid ${GID} dynamic-rc-group                           && \
    useradd -ms /bin/bash --uid ${UID} --gid ${GID} ${USER_NAME}     && \
    echo "root:root" | chpasswd                                      && \
    echo "${USER_NAME}:root" | chpasswd
