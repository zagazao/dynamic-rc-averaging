# README #

Implementation of MRF Learner for DL Platform.

## Setup

First of all, the message broker [RabbitMQ](https://www.rabbitmq.com/) has to be setup. You can either download and install it from
```bash
https://www.rabbitmq.com/download.html
```
Alternatively, you can use the docker image. 
```bash
# Build custom image (extended with a user to grant access via non localhost)
cd docker/rabbitmq
docker build -t rabbitmq:3-management-custom

docker run -d -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management-custom
```

Install python stuff:
```bash
pip3 install -r requirements.txt
pip3 install lib/pxpy-1.0a22_zaga-py3-none-any.whl

pip3 install .

git clone https://github.com/zagazao/dlplatform.git
pip3 install dlpatform
```

## Run

Initially, the datasets have to be downloaded and prepared. This can be achieved by invoking the following command:
```bash
prep_datasets --states 10 ${DATA_DIRECTORY}
```

**Notice**: You have to limit the number of OpenMP-Threads, otherwise the px lib will result in a deadlock (GIL?). 
### Setup of the environment
```bash
# Either set the following variables by hand
export OMP_NUM_THREADS=1
export PX_USE64BIT=True

# or directly use the provided script.
source set-env.sh
```

## Run in docker

### Step 1: Build the container

```bash
# Build the dlplatform container (workdir = project root level)
docker build -t dlplatform:0.5 .

# Build the rabbit mq container
docker build -t rabbitmq:3-management-custom  docker/rabbitmq/
``` 

### Download the datasets with the container
```bash
# Create path on the host as desired user

mkdir -p $HOME/data/docker
docker run -it --rm -v /$HOME/data/docker:/data/in -u $USER dlplatform:0.5 prep_datasets /data/in/
```

## Run the baseline
```bash

export SRC_DIR="$HOME/workspace/dynamic-rc-averaging/"
export OUT_DIR="$HOME/data/dynamic-rc-results/"
export IN_DIR="$HOME/data/dynamic-rc-data/"

mkdir -p ${OUT_DIR}

docker run -it --rm -v ${IN_DIR}:/data/in -v ${OUT_DIR}:/data/out -v ${SRC_DIR}:/data/src -u $(id -u):$(id -g) dlplatform:0.5 /usr/bin/python3 /data/src/environments/baseline.py --root-dir /data/in --out-dir /data/out
```
    
## Baseline (Centralized Models)

- Graph structure estimated via Chow-Liu on a holdout set of 10.000.
- Numeric columns have been discretized to 10 states.
- Test / Train split of 80/20.

```bash
cd src/
# Optional use [--int|--mrf] to run a subset of the environments.
python3 -m baseline --max-iter 1000 -f 0.8 --root-dir $HOME/data/dynamic-rc-data/
```

Accuracy 5-Fold-CV

SUSY: Mean acc: 0.731492741935484
DOTA2: Mean acc: 0.5331727460426703
COVERTYPE: Mean acc: 0.6677878571024367

