#!/usr/bin/env bash

img_name="s876cnsm:5000/heppe/dlplatform:0.5"

# Remove images in background
docker -H s876cnsm:2377 rmi ${img_name} &
# Build new image on s876cn10:2375
docker -H s876cn10:2375 build --build-arg UID=$(id -u)  --build-arg GID=$(id -g) -t ${img_name} .
docker -H s876cn10:2375 push ${img_name}
