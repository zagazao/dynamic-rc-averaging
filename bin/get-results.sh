#!/usr/bin/env bash
set -e

cd /home/lukas/workspace/dynamic-rc-averaging/

echo "Zipping results on cluster.."
ssh s876home /bin/bash -c 'cd /rdata/s01b_ls8_000/heppe/ && tar -czf /rdata/s01b_ls8_000/heppe/dynamic-rc-results.tgz -C /rdata/s01b_ls8_000/heppe/ dynamic-rc-results/'

echo "Copy results from cluster..."
scp s876home:/rdata/s01b_ls8_000/heppe/dynamic-rc-results.tgz .
echo "Extracting results"
tar -xzf "dynamic-rc-results.tgz"