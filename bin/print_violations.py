#!/usr/bin/env
import os
import subprocess

base_dir = '/rdata/s01b_ls8_000/heppe/dynamic-rc-results'

# base_dir = '/home/lukas/workspace/dynamic-rc-averaging/dynamic-rc-results'


def line_count(filename):
    return int(subprocess.check_output(['wc', '-l', filename]).split()[0])


for mode in ['equal', 'spatial']:
    for agg in ['CENTRALIZED', 'PULBIC', 'PRIVATE']:
        for dataset in ['SUSY']:
            for exp_type in ['normal', 'rc']:
                for sync_type in ['dynamic', 'periodic', 'nosync']:
                    path = os.path.join(base_dir, mode, agg, dataset, exp_type, sync_type, 'nodes16')
                    print(path)
                    if not os.path.exists(path):
                        print('--- Skip:', path)
                        continue

                    # Iterate each experiment
                    for file in os.listdir(path):
                        long_path = os.path.join(path, file)
                        # Now check losses and violations for each worker

                        loss_cnts = []
                        vio_cnts = []
                        worker_dirs = [os.path.join(long_path, dir_name) for dir_name in os.listdir(long_path) if dir_name.startswith('worker')]
                        for worker in worker_dirs:
                            loss_cnts.append(line_count(os.path.join(worker, 'losses.txt')))
                            vio_cnts.append(line_count(os.path.join(worker, 'violations.txt')))
                        print(sum(loss_cnts), loss_cnts)
                        print(sum(vio_cnts), vio_cnts)
                        print(long_path, sum(loss_cnts), sum(vio_cnts), sum(loss_cnts) / max(1, sum(vio_cnts)), '\n')
