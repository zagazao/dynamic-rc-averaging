import argparse
import itertools
import logging
import os
import socket
import time
import uuid
from collections import deque

import docker
import numpy as np
from docker.types import Mount

"""""""""""""""
Configuration
"""""""""""""""

NETWORK_NAME = 'dynamic-rc-network'
IMAGE = 'dlplatform:0.5'
CONTAINER_PREFIX = 'heppe-dynamic-rc-'
RABBIT_MQ_IMAGE = 'rabbitmq:3-management-custom'

S876_IMG_PREFIX = 's876cnsm:5000/heppe/'

options = {
    'dataset': ['DOTA2', 'COVERTYPE', 'SUSY'],
    'experiment_type': ['normal', 'rc'],
    'sync_type': ['periodic', 'dynamic'],
    'nodes': np.power(2, np.arange(1, 5))
}

"""""""""""""""
s876 cluster helper
"""""""""""""""


def fix_cluster_images():
    _hostname = socket.gethostname()
    if _hostname.startswith('s876') or _hostname.startswith('gwkilab'):
        global IMAGE
        global RABBIT_MQ_IMAGE
        IMAGE = S876_IMG_PREFIX + IMAGE
        RABBIT_MQ_IMAGE = S876_IMG_PREFIX + RABBIT_MQ_IMAGE


def get_rabbit_mq_kwargs():
    _hostname = socket.gethostname()
    if _hostname.startswith('s876') or _hostname.startswith('gwkilab'):
        return {'cpu_shares': 1, 'mem_limit': '5G'}
    else:
        return {}


def get_exp_container_kwargs(_args):
    _container_kwargs = {
        'mounts': setup_mounts(_args),
        'environment': {'OMP_NUM_THREADS': '1',
                        'PX_USE64BIT': 'True',
                        'constraint:nodetype': '=cpu'},
        #                'constraint:nodetype': '!phi'},
        # 'constraint:nodetype': '=gpu'},
        'user': '{}:{}'.format(os.getuid(), os.getgid()),
        'auto_remove': False,
        'detach': True,
    }
    _hostname = socket.gethostname()
    if _hostname.startswith('s876') or _hostname.startswith('gwkilab'):
        _container_kwargs.update({
            'cpu_shares': 2,
            'mem_limit': '20G'
        })
    return _container_kwargs


"""""""""""""""
Helper functions
"""""""""""""""


def select_option_subset(_args, _options, _key):
    if getattr(_args, _key) is not None:
        _options[_key] = [getattr(_args, _key)]


def generate_combinations(_options):
    keys = options.keys()
    values = (options[key] for key in keys)
    return [dict(zip(keys, combination)) for combination in itertools.product(*values)]


def container_name(_name):
    return CONTAINER_PREFIX + _name


def setup_network(_client, _network_name):
    for network in _client.networks.list():
        if network.name == _network_name:
            print('Network exists.')
            return network
    return _client.networks.create(name=_network_name, driver="overlay", attachable=True)  # Changed from bridge


def setup_rabbit_mq(_client, _name=None, network_name=None):
    for _container in _client.containers.list():
        if _container.name == _name:
            print('Rabbit-MQ-Container exists.')
            return _container

    return _client.containers.run(RABBIT_MQ_IMAGE, detach=True, name=_name, environment={  # network=network_name,
        'RABBITMQ_DEFAULT_USER': 'nutzer32',
        'RABBITMQ_DEFAULT_PASS': 'passwort87'
    }, **get_rabbit_mq_kwargs())


def setup_mounts(_args):
    _mount_in = (_args.root_dir, '/data/in/')
    _mount_src = (_args.src_dir, '/data/src/')
    _mount_out = (_args.out_dir, '/data/out/')

    _mounts = [Mount(source=_mount_in[0], target=_mount_in[1], type='bind', read_only=True),
               Mount(source=_mount_src[0], target=_mount_src[1], type='bind', read_only=True),
               Mount(source=_mount_out[0], target=_mount_out[1], type='bind')]

    return _mounts


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    # Source dir? Or do we put sources into container??
    _base_parser = argparse.ArgumentParser()
    _base_parser.add_argument('-i', '--root-dir', help='Directory to datasets.', required=True)
    _base_parser.add_argument('-o', '--out-dir', default='/tmp/dynamic-rc/', type=str, help='Base directory for results.')
    _base_parser.add_argument('--src-dir', default='/home/lukas/workspace/dynamic-rc-averaging', type=str, help='Path to dynamic-rc-repo.', required=True)

    # General config
    _base_parser.add_argument('-e', '--experiment-type', choices=options['experiment_type'])
    _base_parser.add_argument('-d', '--dataset', choices=options['dataset'])

    _base_parser.add_argument('-m', '--max-samples', default=10000, type=int, help='Number of streamed samples for each learner.')
    _base_parser.add_argument('-b', '--batch-size', default=32, type=int)
    _base_parser.add_argument('-n', '--nodes', default=None, type=int)

    # Sync config
    _base_parser.add_argument('-t', '--delta', default=1, type=float)
    _base_parser.add_argument('-s', '--sync-type', choices=['periodic', 'dynamic'], default=None)
    _base_parser.add_argument('-p', '--sync-period', default=1, type=int)

    _base_parser.add_argument('-l', '--nodes-lower', default=0, type=int, help='Exponent for nodes lower.')
    _base_parser.add_argument('-u', '--nodes-upper', default=5, type=int, help='Exponent for nodes upper.')

    _base_parser.add_argument('--limit', default=64)

    args, unknown = _base_parser.parse_known_args()

    options['nodes'] = np.power(2, np.arange(args.nodes_lower, args.nodes_upper))

    # Subset selection of the options to rerun experiments.
    select_option_subset(args, options, 'dataset')
    select_option_subset(args, options, 'experiment_type')
    select_option_subset(args, options, 'sync_type')
    select_option_subset(args, options, 'nodes')

    print('args', args)

    # "Fix" global setup
    fix_cluster_images()

    # Setup client
    client = docker.from_env()

    container_kwargs = get_exp_container_kwargs(args)

    combinations = generate_combinations(options)
    # Sort descending by number of nodes...
    combinations = deque(sorted(combinations, key=lambda d: d['nodes'], reverse=True))

    max_nodes = combinations[0]['nodes']
    nodes_limit = args.limit
    if nodes_limit < max_nodes:
        nodes_limit = max_nodes

    print('Max nodes per exp:', max_nodes)

    print(len(combinations))

    d = vars(args)

    del d['src_dir']
    del d['limit']
    d['root_dir'] = '/data/in'
    d['out_dir'] = os.path.join('/data/out', time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime(time.time())))

    rabbit_mq_host = 's876cn10'

    running_container = []
    active_nodes = 0

    cpu_core_mapping = {
        1: 1,
        2: 2,
        4: 4,
        8: 8,
        16: 8,
        32: 16,
        64: 16
    }

    # 5 mins timeout..
    time_out = 15
    while combinations:
        _combination = combinations.popleft()
        _required_nodes = _combination['nodes']

        # Check if we have the required space left
        # If we don't check for containers to finish.
        while _required_nodes > (nodes_limit - active_nodes):
            # Check for finished container
            exited_containers = []
            for container in running_container:
                container.reload()
                if container.status == 'exited':
                    exited_containers.append(container)
            # Remove exited cotainers and free their resources
            for container in exited_containers:
                # Check for used nodes.
                _occupied_nodes = int(container.labels['nodes'])
                container.remove()
                active_nodes = active_nodes - _occupied_nodes
                running_container.remove(container)
            time.sleep(time_out)

        _exp_id = uuid.uuid4()
        _experiment_container_name = container_name('shared-exp-{}'.format(_exp_id))

        # continue
        d.update(_combination)

        container_kwargs.update({'name': _experiment_container_name,
                                 'labels': {'nodes': str(_combination['nodes'])},
                                 'cpu_shares': cpu_core_mapping[_combination['nodes']]})

        cmd = ['/usr/bin/python3', '/data/src/environments/shared_exp.py', '--rabbitmq-host', rabbit_mq_host]

        for key, val in d.items():
            cmd.append('--{} {}'.format(key.replace('_', '-'), val))

        print(' '.join(cmd))

        cont = client.containers.run(IMAGE, ' '.join(cmd), **container_kwargs)
        # Append to running containers
        running_container.append(cont)
        active_nodes += _required_nodes
