import argparse
import itertools
import logging
import os
import time
import uuid

import docker
import numpy as np
from docker.types import Mount

"""""""""""""""
Configuration
"""""""""""""""

NETWORK_NAME = 'dynamic-rc-network'
IMAGE = 'dlplatform:0.5'
CONTAINER_PREFIX = 'heppe-dynamic-rc-'
RABBIT_MQ_IMAGE = 'rabbitmq:3-management-custom'

S876_IMG_PREFIX = 's876cnsm:5000/heppe/'

options = {
    'dataset': ['DOTA2', 'COVERTYPE', 'SUSY'],
    'experiment_type': ['normal', 'rc'],
    'sync_type': ['periodic', 'dynamic'],
    'nodes': np.power(2, np.arange(1, 5))
}

"""""""""""""""
s876 cluster helper
"""""""""""""""


def get_rabbit_mq_kwargs():
    return {}


def get_exp_container_kwargs(_args):
    _container_kwargs = {
        'mounts': setup_mounts(_args),
        'environment': {'OMP_NUM_THREADS': '1',
                        'PX_USE64BIT': 'True', },
        #                'constraint:nodetype': '!phi'},
        # 'constraint:nodetype': '=gpu'},
        'user': '{}:{}'.format(os.getuid(), os.getgid()),
        'auto_remove': False,
        'detach': True,
    }
    return _container_kwargs


"""""""""""""""
Helper functions
"""""""""""""""


def select_option_subset(_args, _options, _key):
    if getattr(_args, _key) is not None:
        _options[_key] = [getattr(_args, _key)]


def generate_combinations(_options):
    keys = options.keys()
    values = (options[key] for key in keys)
    return [dict(zip(keys, combination)) for combination in itertools.product(*values)]


def container_name(_name):
    return CONTAINER_PREFIX + _name


def setup_network(_client, _network_name):
    for network in _client.networks.list():
        if network.name == _network_name:
            print('Network exists.')
            return network
    return _client.networks.create(name=_network_name, driver="bridge")  # Changed from bridge


def setup_rabbit_mq(_client, _name=None, network_name=None):
    for _container in _client.containers.list():
        if _container.name == _name:
            print('Rabbit-MQ-Container exists.')
            return _container

    return _client.containers.run(RABBIT_MQ_IMAGE, detach=True, network=network_name, name=_name, environment={
        'RABBITMQ_DEFAULT_USER': 'nutzer32',
        'RABBITMQ_DEFAULT_PASS': 'passwort87'
    }, **get_rabbit_mq_kwargs())


def setup_mounts(_args):
    _mount_in = (_args.root_dir, '/data/in/')
    _mount_src = (_args.src_dir, '/data/src/')
    _mount_out = (_args.out_dir, '/data/out/')

    _mounts = [Mount(source=_mount_in[0], target=_mount_in[1], type='bind', read_only=True),
               Mount(source=_mount_src[0], target=_mount_src[1], type='bind', read_only=True),
               Mount(source=_mount_out[0], target=_mount_out[1], type='bind')]

    return _mounts


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    # Source dir? Or do we put sources into container??
    _base_parser = argparse.ArgumentParser()
    _base_parser.add_argument('-i', '--root-dir', help='Directory to datasets.', required=True)
    _base_parser.add_argument('-o', '--out-dir', default='/tmp/dynamic-rc/', type=str, help='Base directory for results.')
    _base_parser.add_argument('--src-dir', default='/home/lukas/workspace/dynamic-rc-averaging', type=str, help='Path to dynamic-rc-repo.', required=True)

    # General config
    _base_parser.add_argument('-e', '--experiment-type', choices=options['experiment_type'])
    _base_parser.add_argument('-d', '--dataset', choices=options['dataset'])

    _base_parser.add_argument('-m', '--max-samples', default=10000, type=int, help='Number of streamed samples for each learner.')
    _base_parser.add_argument('-b', '--batch-size', default=32, type=int)
    _base_parser.add_argument('-n', '--nodes', default=None, type=int)

    # Sync config
    _base_parser.add_argument('-t', '--delta', default=1, type=float)
    _base_parser.add_argument('-s', '--sync-type', choices=['periodic', 'dynamic'], default=None)
    _base_parser.add_argument('-p', '--sync-period', default=1, type=int)

    args, unknown = _base_parser.parse_known_args()

    # Subset selection of the options to rerun experiments.
    select_option_subset(args, options, 'dataset')
    select_option_subset(args, options, 'experiment_type')
    select_option_subset(args, options, 'sync_type')
    select_option_subset(args, options, 'nodes')

    print('args', args)

    # Setup client
    client = docker.from_env()

    container_kwargs = get_exp_container_kwargs(args)

    combinations = generate_combinations(options)[:4]

    print(len(combinations))
    # exit(1)

    d = vars(args)
    del d['src_dir']
    d['root_dir'] = '/data/in'
    d['out_dir'] = os.path.join('/data/out', time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime(time.time())))

    # Create network
    _exp_network = setup_network(client, NETWORK_NAME)

    _exp_id = uuid.uuid4()
    _rabbit_mq_container_name = container_name('rabbit-mq-{}'.format(_exp_id))
    rabbit_mq_container = setup_rabbit_mq(client, network_name=_exp_network.name, _name=_rabbit_mq_container_name)

    print('Waiting for rabbit mq containers...')
    # Wait until users are created in each container..
    time.sleep(10)
    print('Go...')

    for _idx, _combination in enumerate(combinations):
        # _exp_network, rabbit_mq_container, _experiment_container_name = tpls[_idx]
        _experiment_container_name = container_name('shared-exp-{}'.format(uuid.uuid4()))

        # continue
        d.update(_combination)

        print(d)
        # continue

        container_kwargs.update({'name': _experiment_container_name,
                                 'network': _exp_network.name})

        # Reload to retrieve network settings
        rabbit_mq_container.reload()
        # Setup cmd for container
        cmd = ['/usr/bin/python3', '/data/src/environments/shared_exp.py']
        cmd += ['--rabbitmq-host', rabbit_mq_container.attrs['NetworkSettings']['Networks'][_exp_network.name]['IPAddress']]

        for key, val in d.items():
            cmd.append('--{} {}'.format(key.replace('_', '-'), val))

        print(' '.join(cmd))

        cont = client.containers.run(IMAGE, ' '.join(cmd), **container_kwargs)
