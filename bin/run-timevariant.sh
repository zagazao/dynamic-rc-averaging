#!/usr/bin/env bash

set -x

script='/data/src/experiments/time_variant_experiment.py'
python3=$(which python3)

samples=10000
batchsize=1
nodes=32
default_ops='-r /data/in -o /data/out'

# -r /data/in -o /data/out -l4 -u6 --drift-prob 0.005 -b 1 16 32 -p 1 10 20 -m 10000 --rabbitmq-host s876cn09
${python3} ${script} ${default_ops} "--nodes${nodes}" "-b${batchsize}" --drift-prob 0.0005 --rabbitmq-host s876cn10 -m10000
