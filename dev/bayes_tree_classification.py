import numpy as np
import pxpy
from sklearn.metrics import accuracy_score, f1_score

from dlplatform_extension.learner.px_helper import generate_bayes_tree, calc_dim


def prob(_model, _sample, _A=None):
    phi = _model.phi(_sample)
    if _A is None:
        _, _A = model.infer()
    return np.exp(np.dot(phi, _model.weights) - _A)


n_nodes = 10
n_states = 2
num_samples = 10000
pxpy.set_seed(1000)

edge_list = generate_bayes_tree(num_nodes=n_nodes)

states = np.full(shape=n_nodes, fill_value=n_states, dtype=np.uint64)
states[0] = 2

dim = calc_dim(edge_list, states)

weights = np.random.RandomState(42).gumbel(0, 0.5, dim)

model = pxpy.Model(weights, pxpy.create_graph(edge_list), states)

MAP_STATE = model.MAP()[0]

print(model.dim)

probs, A = model.infer()

z = [[i for i in range(n_states)]] * (n_nodes - 1)

states = [[0, 1]]
states.extend(z)

# print(list(itertools.product(*states)))

# p = 0.0
# max_p = 0.0
# for x in itertools.product(*states):
#     _px = prob(model, np.array(x), A)
#     max_p = max(max_p, _px)
#     print(_px)
#     p += _px
#
# map_prob = prob(model, MAP_STATE, A)
# print(p, max_p, map_prob)

# exit()


# print(prob(model, MAP_STATE, A))
x = model.sample(num_samples=num_samples, sampler=pxpy.SamplerType.gibbs)

# for sample in x:
#     print(prob(model, sample, A))

mu = model.phi_faster(x) / num_samples

print('Likelihood:', A - np.dot(mu, weights))

print(x)

target = np.copy(x[:, 0])
feat = x[:, 1:]

print(np.bincount(target) / num_samples)

fitted = pxpy.train(data=x, graph=pxpy.create_graph(edge_list), mode=pxpy.ModelType.integer)

# print('Distance between solutions:', np.linalg.norm(fitted.weights - weights))
#
# print(fitted.obj)

preds = []


def _pred_model(_data, _model):
    pred = np.zeros(n_nodes)
    for idx in range(num_samples):
        pred = _data[idx]
        pred[0] = pxpy.MISSING_VALUE
        # pred[1:] = target[idx]
        pred = fitted.predict(pred)
        truth = target[idx]
        preds.append(pred[0])


for idx in range(num_samples):
    pred = x[idx]

    pred[0] = pxpy.MISSING_VALUE
    # pred[1:] = target[idx]
    pred = fitted.predict(pred)
    truth = target[idx]
    preds.append(pred[0])

print(accuracy_score(preds, target))
print(f1_score(preds, target))
