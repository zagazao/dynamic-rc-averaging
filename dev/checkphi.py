import os

import numpy as np
import pandas as pd
import pxpy

from dlplatform_extension.datasource.decoder import CSVTypedDecoder
from dlplatform_extension.datasource.standardDataSourceFactories import FileDataSourceFactory
from dlplatform_extension.learner import estimate_structure, generate_model, MRFIncrementalLearnerFactory
from dlplatform_extension.px_dtype_cfg import STATES

N_NODES = 16

dataset = 'PGM'
dataset_path = os.path.join('/home/lukas/data/dynamic-rc-data/{}/{}.disc.csv'.format(dataset, dataset))

hold_out_path = dataset_path + '.holdout'

edge_list = estimate_structure(pd.read_csv(hold_out_path, index_col=None, header=None))
_model = generate_model(pd.read_csv(hold_out_path, index_col=None, header=None), STATES[dataset], mode=pxpy.ModelType.mrf, structure=edge_list)
holdout_mu = np.copy(np.ctypeslib.as_array(_model.empirical_stats, shape=(_model.dimension,)))
_model.graph.delete()
_model.delete()

print(holdout_mu / 10000)
fs_factory = FileDataSourceFactory(filename=dataset_path,
                                   decoder=CSVTypedDecoder(labelCol=0, dtype=np.uint16),
                                   numberOfNodes=N_NODES, shuffle=True, cache=True)

learner_factory = MRFIncrementalLearnerFactory(states=STATES[dataset],
                                               edge_list=edge_list,
                                               batch_size=1,
                                               sync_period=1,
                                               holdout_mu=holdout_mu)

fs = fs_factory.getDataSource(0).prepare()
learners = [learner_factory.getLearner() for _ in range(N_NODES)]

# learner = learner_factory.getLearner()

holdout_mu = holdout_mu / 10000

mus = np.zeros(shape=(N_NODES, learners[0].model.dim))

for i in range(1, 10000):
    print('-' * 35)
    for node_id in range(N_NODES):
        learner = learners[node_id]

        tpl = fs.getNext()
        mus[node_id, :] += learner.model.phi(tpl[0])
        # print(mu / i)
        print(i, node_id, np.dot(mus[node_id] / i, holdout_mu) / (np.linalg.norm(mus[node_id] / i) * np.linalg.norm(holdout_mu)))
