import itertools
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def extract_subset(_df, _key):
    return _df[['node{}_loss_{}'.format(i, _key) for i in range(nodes)]]


def extract_losses(path):
    """
    Extract the losses for one experiment.
    :param path:
    :return:
    """

    files = os.listdir(path)
    worker_dirs = [os.path.join(path, f) for f in files if f.startswith('worker')]
    losses_df = []
    for idx, f in enumerate(worker_dirs):
        f = os.path.join(f, 'losses.txt')
        df = pd.read_csv(f, sep='\t', header=None, index_col=None, names=['time', 'loss_data', 'loss_batch', 'loss_holdout'])
        del df['time']

        df['loss_cum'] = df['loss_batch'].cumsum()
        df = df.add_prefix('node{}_'.format(idx))

        losses_df.append(df)

    return pd.concat(losses_df, axis=1)


def compute_avg(_df):
    n_worker = _df.shape[1] // 4

    col_data = ['node{}_loss_data'.format(i) for i in range(n_worker)]
    col_local = ['node{}_loss_batch'.format(i) for i in range(n_worker)]
    col_holdout = ['node{}_loss_holdout'.format(i) for i in range(n_worker)]
    col_cumulative = ['node{}_loss_cum'.format(i) for i in range(n_worker)]

    avg_data = _df[col_data].mean(axis=1)
    avg_local = _df[col_local].mean(axis=1)
    avg_holdout = _df[col_holdout].mean(axis=1)
    avg_cum = _df[col_cumulative].mean(axis=1)

    # print(avg_data)
    df_new = pd.DataFrame(data=(avg_data, avg_local, avg_holdout, avg_cum)).T
    df_new.columns = ['data', 'batch', 'holdout', 'cum']
    return df_new


# mode, delta, examples = 'normal', 0.5, 500
# mode, delta, examples = 'normal', 0.5, 2500


def get_exp_string(dataset, mode, sync_op, nodes, examples, period=1, delta=0.0):
    return f'/home/lukas/rc-results/{dataset}/{mode}/{sync_op}/nodes{nodes}/period{period}_examples{examples}_batchsize1_delta{delta}/'
    # return f'/home/lukas/workspace/dynamic-rc-averaging/dynamic-rc-results/{dataset}/{mode}/{sync_op}/nodes{nodes}/period{period}_examples{examples}_batchsize1_delta{delta}/'


baseline_results = {
    ('CSUSY', 'rc'): 4.028405666669150520e+01,
    ('CSUSY', 'normal'): 3.427764429308776073e+01,
    ('CDOTA2', 'rc'): 5.656380414610265461e+01,
    ('CDOTA2', 'normal'): 4.170428214453318105e+01,
    ('CCOVERTYPE', 'rc'): 3.307903005731182589e+01,
    ('CCOVERTYPE', 'normal'): 2.428175784334152354e+01
}

mode = 'normal'
examples = 10000
# dataset = 'CCOVERTYPE'
dataset = 'CDOTA2'
# dataset = 'CSUSY'

nodes = [16]  # 1, 2, 4, 8,  # , 32, 64
period = [1]
deltas = [1.0]  # [0.1, 0.01, 0.5, 0.25]  # [10.0, 25.0, 50.0, 75.0] # [0.1, 0.5] # [100.0]  # [10.0, 25.0, 50.0, 75.0]  # 100.0
# deltas = [500.0]
# deltas = [10.0, 25.0, 50.0]

print(list(itertools.product(period, nodes)))
# coms = itertools.combinations([nodes,period])
combs = list(itertools.product(period, nodes))

dynamic_combs = [(*x, y) for x, y in list(itertools.product(combs, deltas))]
print(dynamic_combs)

exp_str_nosync_p1 = {(p, i): get_exp_string(dataset, mode, 'nosync', i, examples, p) for (p, i) in combs}
exp_str_periodic_p1 = {(p, i): get_exp_string(dataset, mode, 'periodic', i, examples, p) for (p, i) in combs}
exp_str_dynamic_p1 = {(p, i, d): get_exp_string(dataset, mode, 'dynamic', i, examples, p, delta=d) for (p, i, d) in dynamic_combs}

df_nosync_p1 = {c: extract_losses(exp_str_nosync_p1[c]) for c in combs}
df_periodic_p1 = {c: extract_losses(exp_str_periodic_p1[c]) for c in combs}
df_dynamic_p1 = {c: extract_losses(exp_str_dynamic_p1[c]) for c in dynamic_combs}

avg_nosync_p1 = {c: compute_avg(df_nosync_p1[c]) for c in combs}
avg_periodic_p1 = {c: compute_avg(df_periodic_p1[c]) for c in combs}
avg_dynamic_p1 = {c: compute_avg(df_dynamic_p1[c]) for c in dynamic_combs}

plot_lim = 5000


def plot_with_std(_ax, _df, color='black', label='NoLabel'):
    _mean, _std = _df.mean(axis=1), _df.std(axis=1)

    print(_std)
    t = np.arange(len(_mean))
    ax.plot(t, _mean, '-.', color=color, label=label)
    ax.fill_between(t, _mean + _std, _mean - _std, facecolor=color, alpha=0.5)


for node in nodes:
    fig, axs = plt.subplots(3, 1)
    plt.title(node)
    # fig.set_title(node)
    for ax, _type in zip(axs, ['data', 'batch', 'holdout']):
        ax.axhline(baseline_results[(dataset, mode)], label='Centralized.', color='black')

        ax.set_title(_type)
        plot_with_std(ax, df_nosync_p1[(1, node)].filter(regex=_type), label='Nosync', color='orange')
        for (p, i) in combs:
            if i == node:
                plot_with_std(ax, df_periodic_p1[(p, i)].filter(regex=_type), label='({}) - Periodic-{}'.format(p, i), color='blue')
        for (p, i, d) in dynamic_combs:
            if i == node:
                plot_with_std(ax, df_dynamic_p1[(p, i, d)].filter(regex=_type), label='({}) - Dynamic-{}-{}'.format(p, i, d), color='green')

    handles, labels = plt.gca().get_legend_handles_labels()
    # Put a legend below current axis
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.25),
              fancybox=True, shadow=True)

    plt.subplots_adjust(bottom=0.1)
    plt.tight_layout()
    # Add dataset
    plt.savefig('/tmp/{}_nodes_{}_{}_{}.pdf'.format(dataset, mode, node, plot_lim))
    plt.clf()

# plt.show()
