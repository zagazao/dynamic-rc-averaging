import itertools
import os

import matplotlib.pyplot as plt
import pandas as pd


def extract_subset(_df, _key):
    return _df[['node{}_loss_{}'.format(i, _key) for i in range(nodes)]]


def extract_losses(path):
    """
    Extract the losses for one experiment.
    :param path:
    :return:
    """

    files = os.listdir(path)
    worker_dirs = [os.path.join(path, f) for f in files if f.startswith('worker')]
    losses_df = []
    for idx, f in enumerate(worker_dirs):
        f = os.path.join(f, 'losses.txt')
        df = pd.read_csv(f, sep='\t', header=None, index_col=None, names=['time', 'loss_data', 'loss_batch', 'loss_holdout'])
        del df['time']

        df['loss_cum'] = df['loss_batch'].cumsum()
        df = df.add_prefix('node{}_'.format(idx))

        losses_df.append(df)

    return pd.concat(losses_df, axis=1)


def compute_avg(_df):
    n_worker = _df.shape[1] // 4

    col_data = ['node{}_loss_local'.format(i) for i in range(n_worker)]
    col_local = ['node{}_loss_batch'.format(i) for i in range(n_worker)]
    col_holdout = ['node{}_loss_val'.format(i) for i in range(n_worker)]
    col_cumulative = ['node{}_loss_cum'.format(i) for i in range(n_worker)]

    avg_data = _df[col_data].mean(axis=1)
    avg_local = _df[col_local].mean(axis=1)
    avg_holdout = _df[col_holdout].mean(axis=1)
    avg_cum = _df[col_cumulative].mean(axis=1)

    # print(avg_data)
    df_new = pd.DataFrame(data=(avg_data, avg_local, avg_holdout, avg_cum)).T
    df_new.columns = ['data', 'batch', 'holdout', 'cumulative']
    return df_new


# mode, delta, examples = 'normal', 0.5, 500
# mode, delta, examples = 'normal', 0.5, 2500

mode = 'normal'
examples = 5000
dataset = 'INTPGM'


def get_exp_string(dataset, mode, sync_op, nodes, examples, period=1, delta=0.0):
    return f'/home/lukas/workspace/dynamic-rc-averaging/dynamic-rc-results/{dataset}/{mode}/{sync_op}/nodes{nodes}/period{period}_examples{examples}_batchsize1_delta{delta}/'


# nodes = [1, 2, 4, 8, 16]  # , 32, 64
# period = [1]
# deltas = [0.1, 0.01, 0.5, 0.25]  # [10.0, 25.0, 50.0, 75.0] # [0.1, 0.5] # [100.0]  # [10.0, 25.0, 50.0, 75.0]  # 100.0
#
# print(list(itertools.product(period, nodes)))
# # coms = itertools.combinations([nodes,period])
# combs = list(itertools.product(period, nodes))
#
# dynamic_combs = [(*x, y) for x, y in list(itertools.product(combs, deltas))]
# print(dynamic_combs)
#
# exp_str_nosync_p1 = {(p, i): get_exp_string(dataset, mode, 'nosync', i, examples, p) for (p, i) in combs}
# exp_str_periodic_p1 = {(p, i): get_exp_string(dataset, mode, 'periodic', i, examples, p) for (p, i) in combs}
# exp_str_dynamic_p1 = {(p, i, d): get_exp_string(dataset, mode, 'dynamic', i, examples, p, delta=d) for (p, i, d) in dynamic_combs}
#
# df_nosync_p1 = {c: extract_losses(exp_str_nosync_p1[c]) for c in combs}
# df_periodic_p1 = {c: extract_losses(exp_str_periodic_p1[c]) for c in combs}
# df_dynamic_p1 = {c: extract_losses(exp_str_dynamic_p1[c]) for c in dynamic_combs}
#
# avg_nosync_p1 = {c: compute_avg(df_nosync_p1[c]) for c in combs}
# avg_periodic_p1 = {c: compute_avg(df_periodic_p1[c]) for c in combs}
# avg_dynamic_p1 = {c: compute_avg(df_dynamic_p1[c]) for c in dynamic_combs}

plot_lim = 5000

base_dir = '/home/lukas/workspace/dynamic-rc-averaging/data'


def get_deltas(_mode, _sync):
    if _sync != 'dynamic':
        return [0.0]
    if _mode == 'rc':
        return [10.0, 25.0, 50.0, 75.0, 100.0]
    else:
        return [0.1, 0.01, 0.25, 0.5]


for dataset in ['DOTA2', 'COVERTYPE', 'SUSY', 'PGM', 'INTPGM']:
    dataset = dataset.lower()
    path = os.path.join(base_dir, dataset.lower())

    print(os.listdir(path))

    nodes = sorted([int(s[5:]) for s in os.listdir(path)])

    print(nodes)

    for node in nodes:
        for mode in ['normal', 'rc']:
            fig, axs = plt.subplots(3, 1)
            for sync in ['periodic', 'nosync', 'dynamic']:
                # Start new plot here...

                for delta in get_deltas(mode, sync):
                    df_name = f'losses_{mode}_{sync}_period1_examples5000_batchsize1_delta{delta}.csv'
                    print(df_name)
                    _df_path = os.path.join(base_dir, dataset, f'nodes{node}', df_name)
                    if not os.path.exists(_df_path):
                        continue
                    _df = pd.read_csv(_df_path, index_col=None, header=0)
                    df_avg = compute_avg(_df)
                    print(df_avg)
                    for ax, type in zip(axs, ['data', 'batch', 'holdout']):
                        ax.plot(df_avg[type], label=f'{sync}-{delta}')
                        ax.set_title(type)

                    # Now get avg shit.....
            # fig.
            # fig.suptitle(f'{dataset}-{node}-{mode}')
            plt.tight_layout()
            axs[0].legend(loc='upper right')
            plt.savefig(f'/tmp/eval-{dataset}-nodes{node}-{mode}.pdf')

            # plt.show()
            plt.clf()
            # for ax, type in zip(axs, ['data', 'batch', 'holdout', 'cumulative']):
#
#                 df_name = f'losses_{mode}_{sync}_period1_examples5000_batchsize1_delta{delta}.csv'
#                 df_path = os.path.join(base_dir, dataset, f'nodes{node}', df_name)
#                 if not os.path.exists(df_path):
#                     print('Skip', df_path)
#                     continue
#
#             for sync in ['periodic', 'nosync', 'dynamic']:
#                 print('FOTZE')
#
# for node in nodes:
#     fig, axs = plt.subplots(4, 1)
#     plt.title(node)
#     # fig.set_title(node)
#     for ax, type in zip(axs, ['data', 'batch', 'holdout', 'cumulative']):
#         ax.set_title(type)
#         # for n in nodes:
#         ax.plot(avg_nosync_p1[(1, node)][type][:plot_lim], '-.', label='Nosync', color='black')
#         for (p, i) in combs:
#             if i == node:
#                 # for nodes in [4, 8, 16]:
#                 ax.plot(avg_periodic_p1[(p, i)][type][:plot_lim], label='({}) - Periodic-{}'.format(p, i))  # , color='red'
#         for (p, i, d) in dynamic_combs:
#             if i == node:
#                 ax.plot(avg_dynamic_p1[(p, i, d)][type][:plot_lim], label='({}) - Dynamic-{}-{}'.format(p, i, d))  # , color='blue'
#
#     handles, labels = ax.get_legend_handles_labels()
#     lg = fig.legend(handles, labels, bbox_to_anchor=(1, 0.5))
#     # plt.subplots_adjust(right=0.7)
#     # axs[0].legend(loc='upper center', bbox_to_anchor=(0.5, 1.05),
#     #               ncol=3, fancybox=True, shadow=True)
#     # plt.legend()
#     plt.tight_layout()
#     # Add dataset
#     plt.savefig('/tmp/{}_nodes_{}_{}_{}.pdf'.format(dataset, mode, node, plot_lim),
#                 bbox_extra_artists=(lg,),
#                 bbox_inches="tight")
#     plt.clf()
