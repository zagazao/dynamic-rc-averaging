import numpy as np
import pandas as pd
import pxpy
from networkx import fast_gnp_random_graph, random_tree

# from dlplatform_extension.learner.px_helper import network_x_to_edgelist
# from dlplatform_extension.learner.px_helper import set_weights, set_weights_int
from dlplatform_extension.learner.px_helper import network_x_to_edgelist, set_weights_int, set_weights

df = pd.read_csv('/home/lukas/data/dynamic-rc-data/PGM/PGM.disc.csv', header=None, index_col=None)
df_holdout = pd.read_csv('/home/lukas/data/dynamic-rc-data/PGM/PGM.disc.csv.holdout', header=None, index_col=None)

data = np.ascontiguousarray(df.values, dtype=np.uint16)
data_holdout = np.ascontiguousarray(df_holdout.values, dtype=np.uint16)


def random_structure(n, p, seed):
    g = fast_gnp_random_graph(n, p, seed=seed)
    edge_list = network_x_to_edgelist(g)
    return edge_list


tree = random_tree(25, seed=876)
edge_list = network_x_to_edgelist(tree)

G = pxpy.create_graph(edge_list)
model = pxpy.train(data=data, graph=G, mode=pxpy.ModelType.integer, iters=0, k=8)

model2 = pxpy.train(data=data, graph=pxpy.create_graph(model.graph.edgelist), iters=0)

for i in range(10000):
    initialization = np.random.randint(low=0, high=6, size=model.dim)
    set_weights_int(model, initialization.astype(np.uint64))
    model = pxpy.train(input_model=model, graph=G, mode=pxpy.ModelType.integer, iters=1000, k=8)
    scaled_weights = np.log(2) * np.copy(model.weights)
    # print(model.eval_ll())  # set_stats(model2, mu_holdout)

    set_weights(model2, scaled_weights)
    print(model2.eval_ll())
    print(model2.LL())
# 57.56462732485098
