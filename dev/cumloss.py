import os

import matplotlib.pyplot as plt
import pandas as pd

base_dir = '/home/lukas/workspace/dynamic-rc-averaging/data'


def get_deltas(_mode, _sync):
    if _sync != 'dynamic':
        return [0.0]
    if _mode == 'rc':
        return [10.0, 25.0, 50.0, 75.0, 100.0]
    else:
        return [0.1, 0.01, 0.25, 0.5]


line_style_mode = {
    'normal': '-',
    'rc': ':',
}

for dataset in ['intpgm', 'pgm', 'susy', 'covertype', 'dota2']:

    path = os.path.join(base_dir, dataset)

    print(os.listdir(path))

    nodes = sorted([int(s[5:]) for s in os.listdir(path)])

    print(nodes)

    for node in nodes:
        for sync in ['periodic', 'nosync', 'dynamic']:
            for mode in ['normal', 'rc']:
                for delta in get_deltas(mode, sync):
                    df_name = f'losses_{mode}_{sync}_period1_examples5000_batchsize1_delta{delta}.csv'
                    df_path = os.path.join(base_dir, dataset, f'nodes{node}', df_name)
                    if not os.path.exists(df_path):
                        print('Skip', df_path)
                        continue
                    df = pd.read_csv(df_path, header=0, index_col=None)

                    col_cumulative = ['node{}_loss_cum'.format(i) for i in range(node)]
                    col_cumulative = df[col_cumulative].mean(axis=1)
                    # print(all(col_cumulative == sorted(col_cumulative)))
                    label_str = f'{node}-{sync}-{mode}'
                    if mode == 'dynamic':
                        label_str += f'-d{delta}'
                    line_style = line_style_mode[mode]
                    plt.plot(col_cumulative, line_style, label=f'{node}-{sync}-{mode}')
        plt.xlabel('Number of samples')
        plt.ylabel('Cumulative loss')

        plt.legend()
        plt.savefig(f'/tmp/{dataset}_cumulative_loss_nodes{node}.pdf')
        plt.clf()
