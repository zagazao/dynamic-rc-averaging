import os

import numpy as np
import pandas as pd
from DLplatform.parameters.vectorParameters import VectorParameter
from DLplatform.synchronizing import DynamicHedgeSync

from dlplatform_extension.averaging.int_average import IntegerAverage
from dlplatform_extension.datasource.decoder import CSVTypedDecoder
from dlplatform_extension.datasource.standardDataSourceFactories import FileDataSourceFactory
from dlplatform_extension.learner import estimate_structure, IntegerMRFIncrementalLearnerFactory, MRFIncrementalLearnerFactory
from dlplatform_extension.px_dtype_cfg import STATES, DATA_DIR

if __name__ == '__main__':
    batch_size = 5
    dataset = 'SUSY'

    # Setup of model and data-set
    n_samples = 10000

    mv_factory = fs_factory = FileDataSourceFactory(filename=os.path.join(DATA_DIR, f'{dataset}/{dataset}.disc.csv'),
                                                    decoder=CSVTypedDecoder(labelCol=0),
                                                    numberOfNodes=1)
    mv_ds = mv_factory.getDataSource(0).prepare()

    df_holdout = pd.read_csv(os.path.join(DATA_DIR, f'{dataset}/{dataset}.disc.csv.holdout'))

    structure = estimate_structure(np.ascontiguousarray(df_holdout.values, dtype=np.uint16))

    agg = IntegerAverage()

    # learner_factory = IntegerMRFIncrementalLearnerFactory(STATES[f'{dataset}'], structure, k=8, sync_period=1, batch_size=batch_size)
    learner_factory = MRFIncrementalLearnerFactory(STATES[f'{dataset}'], structure)
    learner = learner_factory.getLearner()
    learner.setSynchronizer(DynamicHedgeSync(1))
    learner._synchronizer.setAggregator(agg)

    # Stream 10.000 examples to the learner...
    batch = []
    i = 0
    learner.setModel(VectorParameter(learner.model.weights), {'setReference': True})

    w = []

    for i in range(1000):
        batch.append(mv_ds.getNext())
        i += 1
        if i % batch_size == 0:  # Batch size of 32..
            i = 0
            learner.update(batch)
            batch = []
            w.append(learner.getParameters().get())
    print(np.array(w))
    print(np.sum(np.array(w)))
    w = np.array(w)
    print(w.shape)

    import scipy.spatial.distance as dist

    z = dist.pdist(w, 'euclidean')

    print(z)

    for i in range(1, w.shape[0]):
        for j in range(i):
            w_i = w[i]
            w_ii = w[j]
            diff = np.linalg.norm(w_i - w_ii) ** 2
            print(f'Dist {i} - {j} = {diff}')
