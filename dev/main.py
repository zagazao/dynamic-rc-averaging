import logging
import os

import numpy as np
import pandas as pd
from DLplatform.aggregating import Average
from DLplatform.coordinator import InitializationHandler
from DLplatform.synchronizing import DynamicSync
from dlplatform_extension.stopping.never_stop import NeverStoppingCriterion

from dlplatform_extension.datasource.decoder import CSVTypedDecoder
from dlplatform_extension.datasource.standardDataSourceFactories import FileDataSourceFactory
from dlplatform_extension.learner.mrf import MRFIncrementalLearnerFactory
from dlplatform_extension.learner import estimate_structure
from dlplatform_extension import LocalExperiment

from dlplatform_extension.px_dtype_cfg import STATES, DATA_DIR

if __name__ == '__main__':
    os.environ['OMP_NUM_THREADS'] = '1'

    logging.basicConfig(level=logging.INFO)

    n_nodes = 3

    fs_factory = FileDataSourceFactory(filename=os.path.join(DATA_DIR, 'COVERTYPE/COVERTYPE.disc.csv'),
                                       decoder=CSVTypedDecoder(labelCol=0),
                                       numberOfNodes=n_nodes)
    data_source = fs_factory.getDataSource(0).prepare()

    # Setup
    sync = DynamicSync(0.1)
    syncPeriod = 1

    aggregator = Average()

    df_holdout = pd.read_csv(os.path.join(DATA_DIR, 'COVERTYPE/COVERTYPE.disc.csv.holdout'))
    structure = estimate_structure(np.ascontiguousarray(df_holdout.values, dtype=np.uint16))

    learner_factory = MRFIncrementalLearnerFactory(STATES['COVERTYPE'], structure, None, batch_size=16)

    exp = LocalExperiment(messengerHost='localhost', messengerPort=5672,
                          numberOfNodes=n_nodes, sync=sync,
                          aggregator=aggregator, learnerFactory=learner_factory,
                          dataSourceFactory=fs_factory, stoppingCriterion=NeverStoppingCriterion(), initHandler=InitializationHandler(), sleepTime=1)

    exp.run("/tmp/mrf")

    print('Done exp..')
