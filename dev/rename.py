#!/usr/bin/env python

import os

BASE = '/tmp/TEST'

BASE = '/home/lukas/workspace/dynamic-rc-averaging/dynamic-rc-results'


# BASE = '/rdata/s01b_ls8_000/heppe/dynamic-rc-results/'

def new_budget(_mode, _dataset):
    # We used num edges...
    switcher = {
        ('rc', 'SUSY'): 18,
        ('rc', 'DOTA2'): 116,
        ('rc', 'COVERTYPE'): 54,
    }
    # Normals have been trained with a set of 1
    return switcher.get((_mode, _dataset), 1)


for _env in ['equal', 'spatial', 'temporal']:
    for _agg in ['CENTRALIZED', 'PUBLIC', 'PRIVATE']:
        for _dataset in ['SUSY', 'COVERTYPE', 'DOTA2']:
            for _mode in ['normal', 'rc']:
                for _sync in ['nosync', 'periodic', 'dynamic']:

                    _dir = os.path.join(BASE, _env, _agg, _dataset, _mode, _sync)
                    if not os.path.exists(_dir):
                        print(_dir, 'does not exist.')
                        continue

                    for file in [f for f in os.listdir(_dir) if f.startswith('node')]:
                        node_dir = os.path.join(_dir, file)
                        # print(node_dir)
                        for _file in [f for f in os.listdir(node_dir)]:
                            _abs_path = os.path.join(node_dir, _file)
                            if 'budget' in _abs_path:
                                continue
                            _new_path = _abs_path + '_budget{}'.format(new_budget(_mode, _dataset))

                            if not os.path.exists(_new_path):
                                print(os.path.join(node_dir, _file))
                                print(_new_path)
                                print()

                                os.rename(_abs_path, _new_path)
                    # print(file)
