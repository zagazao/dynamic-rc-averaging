import time

import numpy as np
import pandas as pd

import os

from DLplatform.parameters.vectorParameters import VectorParameter
from DLplatform.synchronizing import DynamicHedgeSync
from dlplatform_extension.averaging.int_average import IntegerAverage

from dlplatform_extension.datasource.decoder import CSVTypedDecoder
from dlplatform_extension.datasource.standardDataSourceFactories import FileDataSourceFactory
from dlplatform_extension.learner import estimate_structure, IntegerMRFIncrementalLearnerFactory
from dlplatform_extension.px_dtype_cfg import STATES, DATA_DIR

if __name__ == '__main__':
    dataset = 'COVERTYPE'

    # Setup of model and data-set
    n_samples = 10000

    mv_factory = fs_factory = FileDataSourceFactory(filename=os.path.join(DATA_DIR, f'{dataset}/{dataset}.disc.csv'),
                                                    decoder=CSVTypedDecoder(labelCol=0),
                                                    numberOfNodes=1)
    mv_ds = mv_factory.getDataSource(0).prepare()

    df_holdout = pd.read_csv(os.path.join(DATA_DIR, f'{dataset}/{dataset}.disc.csv.holdout'))

    structure = estimate_structure(np.ascontiguousarray(df_holdout.values, dtype=np.uint16))

    agg = IntegerAverage()

    learner_factory = IntegerMRFIncrementalLearnerFactory(STATES[f'{dataset}'], structure, k=8, sync_period=1)
    learner = learner_factory.getLearner()
    learner.setSynchronizer(DynamicHedgeSync(1))
    learner._synchronizer.setAggregator(agg)

    # Stream 10.000 examples to the learner...
    batch = []
    i = 0
    learner.setModel(VectorParameter(learner.model.weights), {'setReference': True})

    while True:
        now = time.time()
        batch.append(mv_ds.getNext())
        i += 1
        if i % 32 == 0:  # Batch size of 32..
            i = 0
            learner.update(batch)
            batch = []
            # print(learner.checkLocalConditionHolds())
        print(time.time() -now)