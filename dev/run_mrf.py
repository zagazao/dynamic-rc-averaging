import numpy as np
import pandas as pd

from dlplatform_extension.datasource.decoder import CSVTypedDecoder
from dlplatform_extension.datasource.standardDataSourceFactories import FileDataSourceFactory
from dlplatform_extension.learner import estimate_structure, MRFIncrementalLearnerFactory
from dlplatform_extension.px_dtype_cfg import STATES, DATA_DIR

if __name__ == '__main__':
    import os

    print(os.environ)

    # Setup of model and data-set
    n_samples = 10000

    mv_factory = fs_factory = FileDataSourceFactory(filename=os.path.join(DATA_DIR, 'COVERTYPE/COVERTYPE.disc.csv'),
                                                    decoder=CSVTypedDecoder(labelCol=0),
                                                    numberOfNodes=1, cache=True)
    mv_ds = mv_factory.getDataSource(0).prepare()

    df_holdout = pd.read_csv(os.path.join(DATA_DIR, 'COVERTYPE/COVERTYPE.disc.csv.holdout'))

    structure = estimate_structure(np.ascontiguousarray(df_holdout.values, dtype=np.uint16))

    learner_factory = MRFIncrementalLearnerFactory(STATES['COVERTYPE'], structure)

    learner = learner_factory.getLearner()

    batch = []
    i = 0
    while True:
        batch.append(mv_ds.getNext())
        i += 1
        if i % 5 == 0:
            i = 0
            ll, _ = learner.update(batch)
            print(ll)
            batch = []
            # print(learner.getParameters().get()[-1])
