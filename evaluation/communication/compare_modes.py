import os

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

sns.set_palette(sns.color_palette("hls", 20))

from drift import metric_communication_plot


def extract_periodic(rows, target_metric):
    out = []
    for idx, row in rows.iterrows():
        out.append((row['BYTES_SEND'], row[target_metric], 'Periodic(p={},b={})'.format(row['PERIOD'], row['BATCH_SIZE'])))
    return out


def extract_dynamic(rows, target_metric):
    out = []
    for idx, row in rows.iterrows():
        out.append((row['BYTES_SEND'], row[target_metric], 'Dynamic(p={})'.format(row['DELTA'])))
    return out


OUT_DIR = '/tmp'

BUDGET = 10

if __name__ == '__main__':
    df = pd.read_csv('/home/lukas/workspace/dynamic-rc-averaging/plots/experiment_summary.csv')
    print(df)
    # Drop experiments with too many lost ones.
    df = df[df['NA_COUNT'] == 0]
    print(df)

    METRICS = ['WRONG_PREDS', 'CUMULATIVE_BATCH_LOSS']  # 'ACCURACY',

    for metric in METRICS:
        for _env in ['equal']:  # , 'spatial'
            for _dataset in ['SUSY', 'COVERTYPE', 'DOTA2']:
                for _mode in ['normal', 'rc']:
                    for _agg in ['PRIVATE', 'PUBLIC']:  # 'CENTRALIZED', 'PUBLIC',

                        for _batch_size in [10]:

                            query = (df['ENVIRONMENT'] == _env) & (df['AGG'] == _agg) & (df['MODE'] == _mode) & (df['DATASET'] == _dataset) & (df['BATCH_SIZE'] == _batch_size) & (
                                    df['BUDGET'] == BUDGET)
                            sub_df = df[query]
                            no_sync_row = sub_df[sub_df['SYNC'] == 'nosync']

                            no_sync_val = None
                            if len(no_sync_row) > 0:
                                no_sync_val = float(no_sync_row[metric])

                            res = []

                            periodic_rows = sub_df[sub_df['SYNC'] == 'periodic']
                            res.extend(extract_periodic(periodic_rows, metric))

                            dynamic_rows = sub_df[sub_df['SYNC'] == 'dynamic']
                            res.extend(extract_dynamic(dynamic_rows, metric))

                            ax = plt.gca()
                            metric_communication_plot(ax, res, no_sync_loss=no_sync_val)

                            lines = []
                            labels = []

                            for ax in plt.gcf().axes:
                                axLine, axLabel = ax.get_legend_handles_labels()
                                lines.extend(axLine)
                                labels.extend(axLabel)

                            plt.legend(lines, labels, loc='upper right')

                            plt.suptitle(f'{_mode}_{_agg}')

                            ax.set_xlabel('Cumulative communication (bytes)')
                            ax.set_ylabel(metric)

                            out = os.path.join(OUT_DIR, 'communication', _env, _dataset)
                            if not os.path.exists(out):
                                os.makedirs(out)

                            # plt.tight_layout()
                            plt.savefig(os.path.join(out, f'{metric}_{_mode}_{_agg}_batchsize{_batch_size}.pdf'))
                            plt.clf()
