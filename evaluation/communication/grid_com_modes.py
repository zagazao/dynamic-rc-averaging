import os

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.colors import LogNorm
from matplotlib.lines import Line2D

# sns.set_palette(sns.color_palette("hls", 30))

# palette = sns.color_palette("hls", 40)

DATASETS = ['COVERTYPE', 'SUSY', 'DOTA2']

NODES = 16

periods = [1, 10, 20, 50.0, 100.0, 200.0]

# fig, axs = plt.subplots(3, 3, figsize=(12, 5))  # width, height #  sharey='row', sharex='col',

plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}', r'\usepackage{amsmath}', r'\usepackage{amsfonts}']  # r'\usepackage{amsmath,amsfonts,amssymb}'

matplotlib.rcParams.update({'errorbar.capsize': 4})
plt.rcParams.update({
    'text.usetex': True,
    'font.size': 12,
    'font.family': 'lmodern',
    'text.latex.unicode': True,
})


def extract_periodic(rows, target_metric):
    out = []
    for idx, row in rows.iterrows():
        prefix = 'RC-' if row['MODE'] == 'rc' else ''
        if row['PERIOD'] in periods:
            out.append((row['BYTES_SEND'], 1 - row[target_metric], '{}Periodic(p={})'.format(prefix, row['PERIOD'])))
    return out


def extract_dynamic(rows, target_metric):
    out = []
    for idx, row in rows.iterrows():
        prefix = 'RC-' if row['MODE'] == 'rc' else ''
        out.append((row['BYTES_SEND'], 1 - row[target_metric], '{}Dynamic(p={})'.format(prefix, row['DELTA'])))
    return out


# Metrics vs Communication plot

def get_marker_style(_description):
    if 'RC' in _description:
        if 'Periodic' in _description:
            return 'd'
        elif 'Dynamic' in _description:
            return '*'
        else:
            return '8'
    else:
        if 'Periodic' in _description:
            return 'P'
        elif 'Dynamic' in _description:
            return 'X'
        else:
            return '8'


def metric_communication_plot(_ax, metric_list, title=None, serial_loss=None, no_sync_loss=None):
    # https://matplotlib.org/3.1.1/api/markers_api.html

    # X-Axis = Communication
    # Y-Axis = Error
    for _com, _metric, _label in metric_list:
        _ax.scatter(_com, _metric, marker=get_marker_style(_label), label=_label)

    if serial_loss is not None:
        _ax.axhline(serial_loss, linestyle='-.', label='Centralized', color='black')

    if no_sync_loss is not None:
        _ax.axhline(no_sync_loss, linestyle='--', label='No synchronisation', color='black')

    if title is not None:
        _ax.set_title(title)
    return _ax


def plot_cmap(x, y, z):
    return x, y, z


def plot_color_gradient(x, y, z, colors, ax, cmap, marker, name):
    """
    x,y,z numpy arrays with the same shape x.shape == y.shape == z.shape
    """

    ax.set_xscale('log')
    sc = ax.scatter(x, y, c=z, cmap=cmap, norm=colors, marker=marker)

    return ax, sc


xyz_batches = {}
norms = {}
cmaps = {'normal': 'autumn', 'rc': 'winter'}
titles = {"CENTRALIZED": "Centralized", "PUBLIC": "Naïve", "PRIVATE": "Privacy"}
markers = {('rc', 'dynamic'): "+", ('rc', 'periodic'): "o", ('normal', 'dynamic'): "d", ('normal', 'periodic'): "s"}
rows = DATASETS
pad = 5  # in points

df = pd.read_csv(os.path.join('..', '..', 'ergebnisse', 'experiment_summary.csv'))
# Drop experiments with too many lost ones.
df = df[df['NA_COUNT'] == 0]
query = (df['ENVIRONMENT'] == 'equal') & (df['BATCH_SIZE'] == 10) & (df['BUDGET'] == 10) & (df['NA_COUNT'] == 0)
df = df[query]

for dataset, data in df.groupby('DATASET'):
    tmp_batch = {}
    for agg, agg_data in data.groupby('AGG'):
        agg_batch = {}
        for mode, mode_data in agg_data.groupby("SYNC"):
            agg_batch[mode] = mode_data
        tmp_batch[agg] = agg_batch
    tmp_key = tmp_batch['PRIVATE']
    del tmp_batch['PRIVATE']
    tmp_batch['PRIVATE'] = tmp_key
    xyz_batches[dataset] = tmp_batch

for sync, data in df.groupby('SYNC'):
    if sync == 'dynamic':
        norms[sync] = LogNorm(data['DELTA'].min(), data['DELTA'].max())
    else:
        norms[sync] = LogNorm(data['PERIOD'].min(), data['PERIOD'].max())

plot_elements = {}
for data_name, plots in xyz_batches.items():
    agg_dict = {}
    for agg, agg_data in plots.items():
        tmp_elements = {}
        for mode, mode_data in agg_data.items():
            xyz = {}
            for sync_method, data in mode_data.groupby('MODE'):
                if mode == 'dynamic':
                    data = data[data['PERIOD'] == 20]
                    xyz[sync_method] = (data['BYTES_SEND'], 1 - data['ACCURACY'], data['DELTA'])
                else:
                    xyz[sync_method] = (data['BYTES_SEND'], 1 - data['ACCURACY'], data['PERIOD'])
            tmp_elements[mode] = xyz
        agg_dict[agg] = tmp_elements
    plot_elements[data_name] = agg_dict

rc_title = {"rc": " Resource-Constrained", "normal": ""}
for comm in ['periodic', 'dynamic']:
    for data_name, dataset in plot_elements.items():

        fig, axs = plt.subplots(1, 3, figsize=(25, 6))

        for i, (agg, agg_data) in enumerate(dataset.items()):
            if comm not in agg_data:
                continue
            scs = []
            data = agg_data[comm]
            legend_elements = []
            for sync_method, (x, y, z) in data.items():
                if sync_method == 'nosync' and agg != "CENTRALIZED":
                    axs[i].axhline(y.to_numpy(), color="black", linestyle="--")
                    legend_elements.append(Line2D([0], [0],
                                                  color='black',
                                                  label=sync_method.capitalize(),
                                                  linestyle="--"))
                    continue
                ax, sc = plot_color_gradient(x, y, z,
                                             colors=norms[comm],
                                             ax=axs[i],
                                             cmap=cmaps[sync_method],
                                             marker=get_marker_style([sync_method.upper(), comm.capitalize()]),
                                             name=comm.capitalize())
                scs.append(sc)
                legend_elements.append(Line2D([0], [0],
                                              color='w',
                                              markerfacecolor='black',
                                              label=sync_method.capitalize(),
                                              marker=get_marker_style([sync_method.upper(), comm.capitalize()]),
                                              markersize=12))
            ax.legend(handles=legend_elements, loc='upper right', prop={'size': 8})
            ax.set_title(titles[agg])
        fig.subplots_adjust(right=0.8)

        cbar_ax1 = fig.add_axes([0.82, 0.15, 0.02, 0.7])
        cbar_ax1.set_xlabel("RC")
        fig.colorbar(scs[1], cax=cbar_ax1)
        cbar_ax = fig.add_axes([0.89, 0.15, 0.02, 0.7])
        cbar_ax.set_xlabel("Normal")
        fig.colorbar(scs[0], cax=cbar_ax)
        fig.text(0.5, 0.04, 'Communication (bytes)', va='center', rotation='horizontal')
        fig.suptitle(data_name + comm.lower().capitalize())
        axs[0].set_ylabel("Missclassification rate")

        fig.savefig(os.path.join(data_name + "_" + comm + ".pdf"), bbox_inches="tight")
exit()
