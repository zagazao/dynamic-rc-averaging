import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from drift import metric_communication_plot

sns.set_palette(sns.color_palette("hls", 30))

palette = sns.color_palette("hls", 40)

TYPE = 'equal'

DATASETS = ['COVERTYPE', 'SUSY', 'DOTA2']

NODES = 16

periods = [1, 10, 20, 50.0, 100.0, 200.0]

fig, axs = plt.subplots(3, 3, figsize=(10, 5))  # width, height #  sharey='row', sharex='col',

MODE = 'normal'

metric = 'ACCURACY'


def extract_metric(row, target_metric):
    if target_metric == 'ACCURACY':
        return 1 - float(row[target_metric])
    else:
        return float(row[target_metric])


def extract_periodic(rows, target_metric):
    out = []
    for idx, row in rows.iterrows():
        if row['PERIOD'] in periods:
            out.append((row['BYTES_SEND'], extract_metric(row, target_metric), 'Periodic(p={})'.format(row['PERIOD'])))
    return out


def extract_dynamic(rows, target_metric):
    out = []
    for idx, row in rows.iterrows():
        out.append((row['BYTES_SEND'], extract_metric(row, target_metric), 'Dynamic(p={})'.format(row['DELTA'])))
    return out


rows = DATASETS
pad = 5  # in points

df = pd.read_csv('/home/lukas/workspace/dynamic-rc-averaging/plots/experiment_summary.csv')
print(df)
# Drop experiments with too many lost ones.
df = df[df['NA_COUNT'] == 0]

# https://stackoverflow.com/questions/25812255/row-and-column-headers-in-matplotlibs-subplots
# Annotate the rows with dataset names
for ax, row in zip(axs[:, 0], rows):
    ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                xycoords=ax.yaxis.label, textcoords='offset points',
                size='large', ha='right', va='center', rotation=90)

# Annotate columns with type (e.g. Centralized, public, private)
for ax, col in zip(axs[0], ['CENTRALIZED', 'PUBLIC', 'PRIVATE']):
    ax.annotate(col, xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                size='large', ha='center', va='baseline')

# plt.setp(axs[-1], xlabel='Processed batches')
# plt.setp(axs[:, 0], ylabel='Processed batches')

for _d_idx, _dataset in enumerate(DATASETS):
    # global_centralized = baseline_results[(_dataset, MODE)]
    for j, _agg_type in enumerate(['CENTRALIZED', 'PUBLIC', 'PRIVATE']):
        ax = axs[_d_idx, j]
        ax.set_xscale('log')

        # Get data
        query = (df['ENVIRONMENT'] == 'equal') & (df['AGG'] == _agg_type) & (df['MODE'] == MODE) & (df['DATASET'] == _dataset) & (df['BATCH_SIZE'] == 10) & (df['BUDGET'] == 10)
        sub_df = df[query]

        no_sync_row = sub_df[sub_df['SYNC'] == 'nosync']

        no_sync_val = None
        if len(no_sync_row) > 0:
            no_sync_val = extract_metric(no_sync_row, metric)

        res = []

        periodic_rows = sub_df[sub_df['SYNC'] == 'periodic']
        res.extend(extract_periodic(periodic_rows, metric))

        dynamic_rows = sub_df[sub_df['SYNC'] == 'dynamic']
        res.extend(extract_dynamic(dynamic_rows, metric))

        metric_communication_plot(ax, res, no_sync_loss=no_sync_val)

        print(no_sync_row)
        print(sub_df)

labelx = -0.3  # axes coords

for j in range(3):
    axs[j, 0].yaxis.set_label_coords(labelx, 0.5)

handles, labels = ax.get_legend_handles_labels()
fig.legend(handles, labels, loc=7, ncol=2)
fig.subplots_adjust(left=0.1, right=0.6, wspace=0.4, hspace=0.4)

fig.text(0.02, 0.5, 'Missclassificationrate', va='center', rotation='vertical')
fig.text(0.255, 0.04, 'Communication (bytes)', va='center', rotation='horizontal')

plt.savefig(f'/tmp/comm_{MODE}_{metric}.pdf')
plt.show()
