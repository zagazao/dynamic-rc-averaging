import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

sns.set_palette(sns.color_palette("hls", 30))

palette = sns.color_palette("hls", 40)

TYPE = 'equal'

DATASETS = ['COVERTYPE', 'SUSY', 'DOTA2']

NODES = 16

periods = [1, 10, 20, 50.0, 100.0, 200.0]

fig, axs = plt.subplots(3, 3, figsize=(12, 5))  # width, height #  sharey='row', sharex='col',


def extract_periodic(rows, target_metric):
    out = []
    for idx, row in rows.iterrows():
        prefix = 'RC-' if row['MODE'] == 'rc' else ''
        if row['PERIOD'] in periods:
            out.append((row['BYTES_SEND'], 1 - row[target_metric], '{}Periodic(p={})'.format(prefix, row['PERIOD'])))
    return out


def extract_dynamic(rows, target_metric):
    out = []
    for idx, row in rows.iterrows():
        prefix = 'RC-' if row['MODE'] == 'rc' else ''
        out.append((row['BYTES_SEND'], 1 - row[target_metric], '{}Dynamic(p={})'.format(prefix, row['DELTA'])))
    return out


# Metrics vs Communication plot

def get_marker_style(_description):
    if 'RC' in _description:
        if 'Periodic' in _description:
            return 'd'
        elif 'Dynamic' in _description:
            return '*'
        else:
            return '8'
    else:
        if 'Periodic' in _description:
            return 'P'
        elif 'Dynamic' in _description:
            return 'X'
        else:
            return '8'


def metric_communication_plot(_ax, metric_list, title=None, serial_loss=None, no_sync_loss=None):
    # https://matplotlib.org/3.1.1/api/markers_api.html

    # X-Axis = Communication
    # Y-Axis = Error
    for _com, _metric, _label in metric_list:
        _ax.scatter(_com, _metric, marker=get_marker_style(_label), label=_label)

    if serial_loss is not None:
        _ax.axhline(serial_loss, linestyle='-.', label='Centralized', color='black')

    if no_sync_loss is not None:
        _ax.axhline(no_sync_loss, linestyle='--', label='No synchronisation', color='black')

    if title is not None:
        _ax.set_title(title)
    return _ax


rows = DATASETS
pad = 5  # in points

df = pd.read_csv('/tmp/experiment_summary.csv')
print(df)
# Drop experiments with too many lost ones.
df = df[df['NA_COUNT'] < 150]

# https://stackoverflow.com/questions/25812255/row-and-column-headers-in-matplotlibs-subplots
# Annotate the rows with dataset names
for ax, row in zip(axs[:, 0], rows):
    ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                xycoords=ax.yaxis.label, textcoords='offset points',
                size='large', ha='right', va='center', rotation=90)

# Annotate columns with type (e.g. Centralized, public, private)
for ax, col in zip(axs[0], ['CENTRALIZED', 'PUBLIC', 'PRIVATE']):
    ax.annotate(col, xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                size='large', ha='center', va='baseline')


metric = 'ACCURACY'

# plt.setp(axs.flatten, xscale='log')
for _d_idx, _dataset in enumerate(DATASETS):
    # global_centralized = baseline_results[(_dataset, MODE)]
    for j, _agg_type in enumerate(['CENTRALIZED', 'PUBLIC', 'PRIVATE']):
        ax = axs[_d_idx, j]

        ax.set_xscale('log')

        # Get data
        # TODO: Correct modes
        query = (df['ENVIRONMENT'] == 'equal') & (df['AGG'] == _agg_type) & (df['DATASET'] == _dataset) & (df['BATCH_SIZE'] == 10) & (df['BUDGET'] == 10)  # & (df['MODE'] == 'rc')
        sub_df = df[query]

        no_sync_row = sub_df[sub_df['SYNC'] == 'nosync']

        no_sync_val = None
        if len(no_sync_row) > 0:
            no_sync_val_normal = 1 - float(no_sync_row[no_sync_row['MODE'] == 'normal'][metric])
            no_sync_val_rc = 1 - float(no_sync_row[no_sync_row['MODE'] == 'rc'][metric])

            ax.axhline(no_sync_val_normal, linestyle='--', label='NoSync', color='black')
            ax.axhline(no_sync_val_rc, linestyle='-.', label='RC-NoSync', color='black')

        res = []

        periodic_rows = sub_df[sub_df['SYNC'] == 'periodic']
        res.extend(extract_periodic(periodic_rows, metric))

        dynamic_rows = sub_df[sub_df['SYNC'] == 'dynamic']
        res.extend(extract_dynamic(dynamic_rows, metric))

        metric_communication_plot(ax, res, no_sync_loss=no_sync_val)

        print(no_sync_row)

        print(sub_df)

labelx = -0.3  # axes coords

for j in range(3):
    axs[j, 0].yaxis.set_label_coords(labelx, 0.5)

# plt.legend()
handles, labels = ax.get_legend_handles_labels()
fig.legend(handles, labels, loc=7, ncol=2)
fig.subplots_adjust(left=0.1, right=0.6, wspace=0.4, hspace=0.4)

fig.text(0.02, 0.5, 'Missclassificationrate', va='center', rotation='vertical')
fig.text(0.255, 0.04, 'Communication (bytes)', va='center', rotation='horizontal')
# fig.tight_layout()
plt.savefig('/tmp/rc-vs-normal-comm.pdf')
plt.show()
