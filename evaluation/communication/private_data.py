import os

import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.colors import LogNorm
from matplotlib.lines import Line2D

plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}', r'\usepackage{amsmath}', r'\usepackage{amsfonts}']

large_font_size = 40
smaller_font_size = 32
legend_font_size = 14
default_font_size = 22

plt.rcParams.update({
    'text.usetex': True,
    'font.size': default_font_size,
    'font.family': 'lmodern',
    'text.latex.unicode': True,
})

DATASETS = ['COVERTYPE', 'SUSY', 'DOTA2']

NODES = 16

periods = [1, 10, 20, 50.0, 100.0, 200.0]



def extract_periodic(rows, target_metric):
    out = []
    for idx, row in rows.iterrows():
        prefix = 'RC-' if row['MODE'] == 'rc' else ''
        if row['PERIOD'] in periods:
            out.append((row['BYTES_SEND'], 1 - row[target_metric], '{}Periodic(p={})'.format(prefix, row['PERIOD'])))
    return out


def extract_dynamic(rows, target_metric):
    out = []
    for idx, row in rows.iterrows():
        prefix = 'RC-' if row['MODE'] == 'rc' else ''
        out.append((row['BYTES_SEND'], 1 - row[target_metric], '{}Dynamic(p={})'.format(prefix, row['DELTA'])))
    return out


# Metrics vs Communication plot

def get_marker_style(_description):
    if 'RC' in _description:
        if 'Periodic' in _description:
            return 'd'
        elif 'Dynamic' in _description:
            return '*'
        else:
            return '8'
    else:
        if 'Periodic' in _description:
            return 'P'
        elif 'Dynamic' in _description:
            return 'X'
        else:
            return '8'


def metric_communication_plot(_ax, metric_list, title=None, serial_loss=None, no_sync_loss=None):
    # https://matplotlib.org/3.1.1/api/markers_api.html

    # X-Axis = Communication
    # Y-Axis = Error
    for _com, _metric, _label in metric_list:
        _ax.scatter(_com, _metric, marker=get_marker_style(_label), label=_label)

    if serial_loss is not None:
        _ax.axhline(serial_loss, linestyle='-.', label='Centralized', color='black')

    if no_sync_loss is not None:
        _ax.axhline(no_sync_loss, linestyle='--', label='No synchronisation', color='black')

    if title is not None:
        _ax.set_title(title)
    return _ax


def plot_cmap(x, y, z):
    return x, y, z


def plot_color_gradient(x, y, z, colors, ax, cmap, marker, name):
    """
    x,y,z numpy arrays with the same shape x.shape == y.shape == z.shape
    """

    sc = ax.scatter(x, y, c=z, cmap=cmap, norm=colors, marker=marker, s=75)

    return ax, sc


normal_serial_score = {
    'SUSY': 0.731492741935484,
    'COVERTYPE': 0.6677878571024367,
    'DOTA2': 0.5331727460426703,
}

# normal_centralized_scores = {
#     'SUSY': 0.730708333333333,
#     'COVERTYPE': 0.662166666666667,
#     'DOTA2': 0.534166666666667,
# }

normal_centralized_scores = normal_serial_score

xyz_batches = {}
norms = {}
cmaps = {'periodic': 'autumn', 'dynamic': 'winter'}
markers = {('rc', 'dynamic'): "+", ('rc', 'periodic'): "o", ('normal', 'dynamic'): "d", ('normal', 'periodic'): "s"}
rows = DATASETS
pad = 5  # in points

df = pd.read_csv('/home/lukas/workspace/dynamic-rc-averaging/experiment_summary.csv')
# df = pd.read_csv(os.path.join('/tmp', 'experiment_summary.csv'))
# Drop experiments with too many lost ones.
df = df[df['NA_COUNT'] == 0]
query = (df['ENVIRONMENT'] == 'equal') & (df['BATCH_SIZE'] == 10) & (df['BUDGET'] == 10) & (df['NA_COUNT'] == 0)
df = df[query]

for dataset, data in df.groupby('DATASET'):
    tmp_batch = {}
    for agg, agg_data in data.groupby('AGG'):
        agg_batch = {}
        for mode, mode_data in agg_data.groupby("SYNC"):
            agg_batch[mode] = mode_data
        tmp_batch[agg] = agg_batch
    tmp_key = tmp_batch['PRIVATE']
    del tmp_batch['PRIVATE']
    tmp_batch['PRIVATE'] = tmp_key
    xyz_batches[dataset] = tmp_batch

for sync, data in df.groupby('SYNC'):
    if sync == 'dynamic':
        norms[sync] = LogNorm(data['DELTA'].min(), data['DELTA'].max())
    else:
        norms[sync] = LogNorm(data['PERIOD'].min(), data['PERIOD'].max())

plot_elements = {}
for data_name, plots in xyz_batches.items():
    agg_dict = {}
    for agg, agg_data in plots.items():
        tmp_elements = {}
        for mode, mode_data in agg_data.items():
            xyz = {}
            for sync_method, data in mode_data.groupby('MODE'):
                if mode == 'dynamic':
                    data = data[data['PERIOD'] == 20]
                    xyz[sync_method] = (data['BYTES_SEND'], 1 - data['ACCURACY'], data['DELTA'])
                else:
                    xyz[sync_method] = (data['BYTES_SEND'], 1 - data['ACCURACY'], data['PERIOD'])
            tmp_elements[mode] = xyz
        agg_dict[agg] = tmp_elements
    plot_elements[data_name] = agg_dict

fig, axs = plt.subplots(1, 3, figsize=(20, 6))
for ax in axs:
    ax.set_xscale('log')
scs = {}
for i, (data_name, dataset) in enumerate(plot_elements.items()):
    agg = "PRIVATE"
    agg_data = dataset[agg]

    legend_elements = []
    for sync_method, data in agg_data.items():
        if sync_method == 'nosync':
            axs[i].axhline(data['normal'][1].to_numpy(), color="black", linestyle="--")
            axs[i].set_xticks([10e3, 10e4, 10e5, 10e6, 10e7, 10e8, 10e9])

            legend_elements.append(Line2D([0], [0],
                                          color='black',
                                          label=sync_method.capitalize(),
                                          linestyle="--"))

            axs[i].axhline(data['rc'][1].to_numpy(), color="black", linestyle="-.")
            legend_elements.append(Line2D([0], [0],
                                          color='black',
                                          label="RC-" + sync_method.capitalize(),
                                          linestyle="-."))
            axs[i].axhline(1 - normal_centralized_scores[data_name], color="black", linestyle=":")
            legend_elements.append(Line2D([0], [0],
                                          color='black',
                                          # label="Centralized-MRF",
                                          label="Global",
                                          linestyle=":"))
            continue

        for comm, (x, y, z) in data.items():
            ax, sc = plot_color_gradient(x, y, z,
                                         colors=norms[sync_method],
                                         ax=axs[i],
                                         cmap=cmaps[sync_method],
                                         marker=get_marker_style([sync_method.capitalize(), comm.upper()]),
                                         name=comm.capitalize())
            scs[sync_method] = sc
            if comm == "rc":
                prefix = "RC-"
            else:
                prefix = ""
            legend_elements.append(Line2D([0], [0],
                                          color='w',
                                          markerfacecolor='black',
                                          label=prefix + sync_method.capitalize(),
                                          marker=get_marker_style([sync_method.capitalize(), comm.upper()]),
                                          markersize=10))
    legend_elements = sorted(legend_elements, key=lambda x: (x.get_linestyle(), x.get_label()))
    ax.legend(handles=legend_elements, loc='upper right', prop={'size': legend_font_size})
    ax.set_title(data_name.lower().capitalize(), fontsize=large_font_size, pad=17.5)
fig.subplots_adjust(right=0.8)

cbar_ax1 = fig.add_axes([0.82, 0.15, 0.02, 0.7])
cbar_ax1.set_xlabel("Delta", fontsize=smaller_font_size, labelpad=15)
fig.colorbar(scs['dynamic'], cax=cbar_ax1)
cbar_ax = fig.add_axes([0.89, 0.15, 0.02, 0.7])
cbar_ax.set_xlabel("Period", fontsize=smaller_font_size, labelpad=15)
fig.colorbar(scs["periodic"], cax=cbar_ax)
fig.text(0.38, -0.02, 'Communication (bytes)', va='center', rotation='horizontal', fontsize=large_font_size)
# fig.suptitle("Privacy Preserving Averaging")
axs[0].set_ylabel("Missclassification rate", fontsize=large_font_size)

fig.savefig(os.path.join("DataSets" + "_" + "private.pdf"), bbox_inches="tight")
exit()
