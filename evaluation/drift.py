import matplotlib.pyplot as plt
import numpy as np


def get_drift_timings(drift_seed, drift_prob, duration):
    drift_random_state = np.random.RandomState(drift_seed)
    drifts = []
    for i in range(duration):
        if drift_random_state.uniform() < drift_prob:
            drifts.append(i)
    return np.array(drifts)


# Metrics vs Communication plot

def get_marker_style(_description):
    if 'Periodic' in _description:
        return 'o'
    elif 'Dynamic' in _description:
        return 'x'
    else:
        return '8'


def metric_communication_plot(_ax, metric_list, title=None, serial_loss=None, no_sync_loss=None):
    # https://matplotlib.org/3.1.1/api/markers_api.html

    # X-Axis = Communication
    # Y-Axis = Error
    for _com, _metric, _label in metric_list:
        _ax.scatter(_com, _metric, marker=get_marker_style(_label), label=_label)

    if serial_loss is not None:
        _ax.axhline(serial_loss, linestyle='-.', label='Centralized', color='black')

    if no_sync_loss is not None:
        _ax.axhline(no_sync_loss, linestyle='--', label='No synchronisation', color='black')

    if title is not None:
        _ax.set_title(title)
    return _ax


if __name__ == '__main__':
    x = [
        (50000, 200, 'Periodic(p=12)'),
        (15232, 300, 'Dynamic(delta=0.1)')
    ]

    ax = metric_communication_plot(plt.gca(), x, serial_loss=150, no_sync_loss=350, title='Test')

    plt.show()
