import os

import matplotlib.pyplot as plt

from exp_result import ExperimentResult, baseline_results, delta_dicts

TYPE = 'equal'

DATASETS = ['COVERTYPE', 'SUSY', 'DOTA2']

NODES = 16

periods = [1, 10, 20, 50.0, 100.0, 200.0]


def make_plot(_ax, agg_mechanism, dataset, exp_type, fn):
    # Draw periodic with different periods
    for _period in periods:
        try:
            _exp_periodic_centralized = ExperimentResult.get_result(TYPE, agg_mechanism, dataset, exp_type, 'periodic', NODES, 1500, period=_period, batch_size=10, budget=10)
            _ax.plot(getattr(_exp_periodic_centralized, fn)(), label='Periodic(p={})'.format(_period))
        except:
            print('P=', _period, 'not found.', dataset, agg_mechanism, exp_type)

    for _delta in delta_dicts[TYPE][(dataset, exp_type, agg_mechanism)]:
        try:
            _exp_dynamic_centralized = ExperimentResult.get_result(TYPE, agg_mechanism, dataset, exp_type, 'dynamic', NODES, 1500, period=20, batch_size=10, budget=10,
                                                                   delta=_delta)
            _ax.plot(getattr(_exp_dynamic_centralized, fn)(), label='Dynamic(d={})'.format(_delta))
        except:
            print(_delta, 'does not exist.')
            continue


def make_loss_plot(prefix='loss', data_provider_fn=None):
    for dataset_idx, dataset in enumerate(DATASETS):
        for i, EXP_TYPE in enumerate(['normal', 'rc']):
            for j, AGG_MECHANISM in enumerate(['CENTRALIZED', 'PUBLIC', 'PRIVATE']):
                # fig, axs = plt.subplots(111)
                ax = plt.gca()
                # ax = axs[i, j]

                ax.set_title(f'{EXP_TYPE}_{AGG_MECHANISM}')

                # Draw baseline
                ax.axhline(baseline_results[(dataset, EXP_TYPE)], linestyle='--', color='black', label='Centralized')

                if AGG_MECHANISM != 'CENTRALIZED':
                    no_sync = ExperimentResult.get_result(TYPE, AGG_MECHANISM, dataset, EXP_TYPE, 'nosync', NODES, m_samples=1500, period=1, batch_size=10, budget=10)
                    ax.plot(getattr(no_sync, data_provider_fn)(), label='NoSync')

                make_plot(ax, AGG_MECHANISM, dataset, EXP_TYPE, data_provider_fn)

                # ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.25),
                #           fancybox=True, shadow=True)
                ax.set_xlabel('Seen examples')
                ax.set_ylabel('Neg. Avg. Likelihood')

                plt.legend(loc=7)
                plt.subplots_adjust(bottom=0.1, right=0.7)
                plt.tight_layout()

                out_dir = '/tmp/equal/ĺoss'
                if not os.path.exists(out_dir):
                    os.makedirs(out_dir)

                plt.savefig(f'/{out_dir}/{prefix}_{dataset}_{EXP_TYPE}_{AGG_MECHANISM}.pdf')
                plt.clf()


make_loss_plot('equal_loss_holdout', 'mean_holdout_loss')
# make_loss_plot('equal_loss_batch', 'mean_batch_loss')
# make_loss_plot('equal_loss_local', 'mean_local_loss')

for dataset_idx, dataset in enumerate(DATASETS):
    # fig, axs = plt.subplots(2, 3)
    for i, EXP_TYPE in enumerate(['normal', 'rc']):
        for j, AGG_MECHANISM in enumerate(['CENTRALIZED', 'PUBLIC', 'PRIVATE']):
            # ax = plt.gca()
            # ax = axs[i, j]
            ax = plt.gca()

            ax.set_title(f'{EXP_TYPE}_{AGG_MECHANISM}')
            ax.set_ylim(top=1.0)

            if AGG_MECHANISM != 'CENTRALIZED':
                no_sync = ExperimentResult.get_result(TYPE, AGG_MECHANISM, dataset, EXP_TYPE, 'nosync', NODES, m_samples=1500, period=1, batch_size=10, budget=10)
                ax.plot(no_sync.mean_cum_acc(), label='NoSync')
            make_plot(ax, AGG_MECHANISM, dataset, EXP_TYPE, 'mean_cum_acc')

            # Legend
            ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.25), fancybox=True, shadow=True)
            plt.subplots_adjust(bottom=0.1)
            plt.tight_layout()

            ax.set_xlabel('Seen examples')
            ax.set_ylabel('Cumulative mean accuracy')

            out_dir = '/tmp/equal/accuracy'
            if not os.path.exists(out_dir):
                os.makedirs(out_dir)

            plt.savefig(f'/{out_dir}/equal_accuracy_{dataset}_{EXP_TYPE}_{AGG_MECHANISM}.pdf')
            plt.clf()
