import os

import matplotlib.pyplot as plt

from exp_result import ExperimentResult, delta_dicts, baseline_results

TYPE = 'spatial'

DATASETS = ['COVERTYPE', 'SUSY']  # 'DOTA2',

NODES = 16

periods = [1, 10, 20]


def plot_mechanism_fn(ax, agg_mechanism, dataset, exp_type, fn):
    for _period in periods:
        try:
            _exp_periodic = ExperimentResult.get_result(TYPE, agg_mechanism, dataset, exp_type, 'periodic', NODES, 1500, _period, 1)
            ax.plot(getattr(_exp_periodic, fn)(), label='Periodic(p={})'.format(_period))
        except:
            print('period', _period, 'not found')

    for delta in delta_dicts[TYPE][(dataset, exp_type, agg_mechanism)]:
        try:
            exp_dynamic_p1 = ExperimentResult.get_result(TYPE, agg_mechanism, dataset, exp_type, 'dynamic', NODES, 1500, 1, 1, delta=delta)
            ax.plot(getattr(exp_dynamic_p1, fn)(), label='Dynamic(d={})'.format(delta))
        except:
            print(delta, 'not found.')
            continue


def make_loss_plot(prefix='loss', data_provider_fn=None):
    for dataset_idx, dataset in enumerate(DATASETS):
        # fig, axs = plt.subplots(2, 3)
        for i, EXP_TYPE in enumerate(['normal', 'rc']):
            for j, AGG_MECHANISM in enumerate(['CENTRALIZED', 'PUBLIC', 'PRIVATE']):
                ax = plt.gca()
                # ax = axs[i, j]

                ax.set_title(f'{EXP_TYPE}_{AGG_MECHANISM}')

                # Draw baseline
                ax.axhline(baseline_results[(dataset, EXP_TYPE)], linestyle='--', color='black', label='Centralized')

                if AGG_MECHANISM != 'CENTRALIZED':
                    no_sync = ExperimentResult.get_result(TYPE, AGG_MECHANISM, dataset, EXP_TYPE, 'nosync', NODES, 1500, 1, 1)
                    ax.plot(getattr(no_sync, data_provider_fn)(), label='NoSync')

                plot_mechanism_fn(ax, AGG_MECHANISM, dataset, EXP_TYPE, data_provider_fn)
                ax.set_xlabel('Seen examples')
                ax.set_ylabel('Neg. Avg. Likelihood')

                # For a shared plot, untab two times
                # Legend
                ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.25), fancybox=True, shadow=True)
                plt.subplots_adjust(bottom=0.1)
                plt.tight_layout()
                out_dir = '/tmp/spatial/loss'
                if not os.path.exists(out_dir):
                    os.makedirs(out_dir)

                plt.savefig(f'/{out_dir}/{prefix}_{EXP_TYPE}_{AGG_MECHANISM}_{dataset}.pdf')
                plt.clf()


make_loss_plot('spatial_loss_holdout', 'mean_holdout_loss')

for dataset_idx, dataset in enumerate(DATASETS):
    # fig, axs = plt.subplots(2, 3)
    for i, EXP_TYPE in enumerate(['normal', 'rc']):
        for j, AGG_MECHANISM in enumerate(['CENTRALIZED', 'PUBLIC', 'PRIVATE']):
            # ax = axs[i, j]

            ax = plt.gca()

            ax.set_title(f'{EXP_TYPE}_{AGG_MECHANISM}')
            ax.set_ylim(top=1.0)

            # PRIVATE

            if AGG_MECHANISM != 'CENTRALIZED':
                no_sync = ExperimentResult.get_result(TYPE, AGG_MECHANISM, dataset, EXP_TYPE, 'nosync', NODES, 1500, 1, 1)
                ax.plot(no_sync.mean_cum_acc(), label='NoSync')

            plot_mechanism_fn(ax, AGG_MECHANISM, dataset, EXP_TYPE, 'mean_cum_acc')

            ax.set_xlabel('Seen examples')
            ax.set_ylabel('Cumulative mean accuracy')

            # Legend
            ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.25), fancybox=True, shadow=True)
            plt.subplots_adjust(bottom=0.1)
            plt.tight_layout()

            out_dir = '/tmp/spatial/accuracy'
            if not os.path.exists(out_dir):
                os.makedirs(out_dir)

            plt.savefig(f'/{out_dir}/accuracy_{EXP_TYPE}_{AGG_MECHANISM}_{dataset}.pdf')
            plt.clf()
