import matplotlib.pyplot as plt

from drift import get_drift_timings
from exp_result import ExperimentResult

TYPE = 'temporal'
DATASET = 'TimePGM'

drift_x = get_drift_timings(0, 0.0005, 10000)

fig, axs = plt.subplots(2, 2)

periods = [1, 10]
deltas = [1.0]


def mark_drift(_ax):
    for idx, x in enumerate(drift_x):
        kwargs = {'label': 'Drift'} if idx == 0 else {}
        _ax.axvline(x, linestyle='--', color='black', zorder=0, **kwargs)


# mean_cum_acc
def make_plot(_ax, agg_mechanism, dataset, exp_type, fn):
    for _period in periods:
        _exp_periodic_centralized = ExperimentResult.get_result(TYPE, agg_mechanism, dataset, exp_type, 'periodic', 16, 10000, _period, 1)
        _ax.plot(getattr(_exp_periodic_centralized, fn)(), label='Periodic(p={})'.format(_period))

        for _delta in deltas:
            _exp_dynamic_centralized = ExperimentResult.get_result(TYPE, agg_mechanism, dataset, exp_type, 'dynamic', 16, 10000, _period, 1, delta=_delta)
            _ax.plot(getattr(_exp_dynamic_centralized, fn)(), label='Dynamic(p={},d={})'.format(_period, _delta))


for i, EXP_TYPE in enumerate(['normal', 'rc']):
    for j, AGG_MECHANISM in enumerate(['PRIVATE', 'PUBLIC']): # , 'CENTRALIZED'
        print(EXP_TYPE, AGG_MECHANISM)

        ax = axs[i, j]
        mark_drift(ax)

        ax.set_title(f'{EXP_TYPE}_{AGG_MECHANISM}')

        if AGG_MECHANISM != 'CENTRALIZED':
            # No sync
            no_sync = ExperimentResult.get_result(TYPE, AGG_MECHANISM, DATASET, EXP_TYPE, 'nosync', 16, 10000, 1, 1)
            ax.plot(no_sync.mean_cum_acc(), label='NoSync')

        make_plot(ax, AGG_MECHANISM, DATASET, EXP_TYPE, 'mean_cum_acc')

# Legend
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.25), fancybox=True, shadow=True)
plt.subplots_adjust(bottom=0.1)
plt.tight_layout()
plt.savefig(f'/tmp/temporal_accuracy.pdf')
plt.show()
