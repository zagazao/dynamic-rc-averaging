import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, f1_score

from dlplatform_extension.px_dtype_cfg import DATA_SET_DIMENSION

baseline_results = {
    ('SUSY', 'rc'): 4.028405666669150520e+01,
    ('SUSY', 'normal'): 3.427764429308776073e+01,
    ('DOTA2', 'rc'): 5.656380414610265461e+01,
    ('DOTA2', 'normal'): 4.170428214453318105e+01,
    ('COVERTYPE', 'rc'): 3.307903005731182589e+01,
    ('COVERTYPE', 'normal'): 2.428175784334152354e+01
}

delta_dict_spatial = {
    # This is spatial... wow..
    ('SUSY', 'rc', 'CENTRALIZED'): [0.5, 1.0, 1.0],
    ('SUSY', 'rc', 'PUBLIC'): [1.0, 10.0, 15.0, 20.0, 50.0],
    ('SUSY', 'rc', 'PRIVATE'): [50.0, 100.0, 350.0, 450.0, 500.0],
    ('SUSY', 'normal', 'CENTRALIZED'): [0.1, 0.5, 10.0],
    ('SUSY', 'normal', 'PUBLIC'): [1.0, 1.5, 2.0, 5.0],
    ('SUSY', 'normal', 'PRIVATE'): [0.01, 0.1, 0.25, 0.5, 0.75, 1.0],

    ('COVERTYPE', 'rc', 'CENTRALIZED'): [0.1, 0.5, 1.0, 10.0],
    ('COVERTYPE', 'rc', 'PUBLIC'): [10.0, 20.0, 50.0],
    ('COVERTYPE', 'rc', 'PRIVATE'): [350.0, 450.0, 500.0],
    ('COVERTYPE', 'normal', 'CENTRALIZED'): [0.1, 1.0, 10.0],
    ('COVERTYPE', 'normal', 'PUBLIC'): [2.5, 5.0, 7.5, 10.0],
    ('COVERTYPE', 'normal', 'PRIVATE'): [0.1, 0.25, 0.5, 0.75, 1.0, 1.5, 5.0, 10.0],
}

delta_dict_equal = {
    ('SUSY', 'rc', 'CENTRALIZED'): [0.5, 1.0, 10.0],
    ('SUSY', 'rc', 'PUBLIC'): [0.1, 1.0, 1.5, 2.5, 5.0, 7.5],

    ('SUSY', 'normal', 'CENTRALIZED'): [0.1, 1.0],
    ('SUSY', 'normal', 'PUBLIC'): [0.1, 0.25, 0.5, 0.75, 1.0],

    ('COVERTYPE', 'rc', 'CENTRALIZED'): [0.5, 1.0, 10.0],
    ('COVERTYPE', 'rc', 'PUBLIC'): [0.1, 0.25, 0.5, 0.75, 1.0, 2.5, 5.0, 10.0, 25.0, 75.0, 100.0, 150.0, 200.0, 250.0, 350.0, 450.0, 500.0, 750.0, 1000.0, 1250.0, 1500.0, 1750.0],

    ('COVERTYPE', 'normal', 'CENTRALIZED'): [0.1, 0.25, 1.0, 10.0],
    ('COVERTYPE', 'normal', 'PUBLIC'): [0.1, 0.25, 0.5, 0.75, 1.0, 10.0, 50.0, 75.0, 100.0, 200.0, 250.0, 350.0, 450.0, 500.0, 750.0, 1000.0, 1250.0, 1500.0, 1750.0],

    ('SUSY', 'rc', 'PRIVATE'): [10.0, 25.0, 50.0, 75.0, 100.0, 150.0, 200.0, 250.0, 350.0, 450.0, 500.0],  # 0.1 0.5 1.0 10.0 50.0 75.0 100.0 200.0 250.0
    ('SUSY', 'normal', 'PRIVATE'): [0.01, 0.05, 0.1, 0.2, 0.25, 0.5, 1.0, 10.0, 25.0, 50.0, 75.0, 100.0, 200.0, 250.0, 350.0, 450.0, 500.0, 600.0, 700.0, 800.0, 900.0, 1000.0],

    ('COVERTYPE', 'rc', 'PRIVATE'): [10.0, 25.0, 50.0, 75.0, 100.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 750.0, 1000.0, 1250.0, 1500.0, 1750.0, 2000.0, 2250.0, 2500.0],
    ('COVERTYPE', 'normal', 'PRIVATE'): [0.01, 0.05, 0.1, 0.2, 0.25, 0.5, 1.0, 10.0, 50.0, 75.0, 100.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 750.0, 1000.0, 1250.0,
                                         1500.0],

    ('DOTA2', 'rc', 'PRIVATE'): [10.0, 25.0, 50.0, 75.0, 100.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 750.0, 1000.0, 1250.0, 1500.0, 1750.0, 2000.0, 2250.0, 2500.0],
    ('DOTA2', 'normal', 'PRIVATE'): [0.01, 0.05, 0.1, 0.2, 0.25, 0.5, 1.0, 10.0, 50.0, 75.0, 100.0, 200.0, 250.0, 500.0, 750.0, 1000.0, 1250.0, 1500.0],

    ('DOTA2', 'rc', 'CENTRALIZED'): [0.01, 0.05, 0.1, 0.2, 0.25, 0.5, 1.0, 10.0, 25.0, 50.0, 75.0, 100.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0],
    ('DOTA2', 'normal', 'CENTRALIZED'): [0.01, 0.05, 0.1, 0.2, 0.25, 0.5, 1.0, 10.0, 50.0, 75.0, 100.0, 200.0, 250.0, 500.0, 750.0, 1000.0, 1250.0, 1500.0],

    ('DOTA2', 'rc', 'PUBLIC'): [0.01, 0.05, 0.1, 0.2, 0.25, 0.5, 1.0, 10.0, 25.0, 50.0, 75.0, 100.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 750.0, 1000.0, 1250.0, 1500.0,
                                1750.0],
    ('DOTA2', 'normal', 'PUBLIC'): [0.01, 0.05, 0.1, 0.2, 0.25, 0.5, 1.0, 10.0, 50.0, 75.0, 100.0, 200.0, 250.0, 500.0, 750.0, 1000.0, 1250.0, 1500.0],
    # 0.1 0.5 1.0 10.0 50.0 100.0 200.0 250.0 500.0
}

summary = {2000.0, 2250.0, 2500.0, 2750.0, 3000.0}
for key, val in delta_dict_equal.items():
    summary.update(set(val))

summary = sorted(list(summary))

delta_dict_equal = {key: summary for key, val in delta_dict_equal.items()}

print(summary)
print(delta_dict_equal)

delta_dicts = {
    'spatial': delta_dict_spatial,
    'equal': delta_dict_equal,
}

periods = [1, 10, 20]


def extract_com_plot(_env_type, agg_mechanism, dataset, exp_type, nodes, fn):
    data = []  # _com, _metric, _label

    for _period in periods:
        try:
            _exp_periodic = ExperimentResult.get_result(_env_type, agg_mechanism, dataset, exp_type, 'periodic', nodes, 1500, _period, 1)
            data.append((_exp_periodic.bytes_send, getattr(_exp_periodic, fn)(), 'Periodic(p={})'.format(_period)))
        except:
            print('period', _period, 'not found')

    # TODO: We could kick experiments with too high na counts.. (What do we define as too high??)

    for delta in delta_dicts[_env_type][(dataset, exp_type, agg_mechanism)]:
        try:
            exp_dynamic_p1 = ExperimentResult.get_result(_env_type, agg_mechanism, dataset, exp_type, 'dynamic', nodes, 1500, 1, 1, delta=delta)
            data.append((_exp_periodic.bytes_send, getattr(exp_dynamic_p1, fn)(), 'Dynamic(d={})'.format(delta)))
        except:
            print(delta, 'not found.')
            continue
    return data


def texify():
    plt.rcParams['text.latex.preamble'] = [r'\usepackage{lmodern}', r'\usepackage{amsmath}', r'\usepackage{amsfonts}']

    matplotlib.rcParams.update({'errorbar.capsize': 4})
    plt.rcParams.update({
        'text.usetex': True,
        'font.size': 12,
        'font.family': 'lmodern',
        'text.latex.unicode': True,
    })


def extract_losses(path):
    """
    Extract the losses for one experiment.
    :param path:
    :return:
    """

    files = os.listdir(path)
    worker_dirs = [os.path.join(path, f) for f in files if f.startswith('worker')]
    losses_df = []
    for idx, f in enumerate(worker_dirs):
        f = os.path.join(f, 'losses.txt')
        df = pd.read_csv(f, sep='\t', header=None, index_col=None, names=['time', 'loss_local', 'loss_batch', 'loss_val'])
        del df['time']

        df['loss_cum'] = df['loss_batch'].cumsum()
        df = df.add_prefix('node{}_'.format(idx))

        losses_df.append(df)

    return pd.concat(losses_df, axis=1)


def n_learner(path):
    files = os.listdir(path)
    worker_dirs = [os.path.join(path, f) for f in files if f.startswith('worker')]
    return len(worker_dirs)


def extract_predictions(path):
    """
    Extract the losses for one experiment.
    :param path:
    :return:
    """

    files = os.listdir(path)
    worker_dirs = [os.path.join(path, f) for f in files if f.startswith('worker')]
    pred_list = []
    for idx, f in enumerate(worker_dirs):
        f = os.path.join(f, 'predictions.txt')
        df = pd.read_csv(f, sep='\t', header=None, index_col=None, names=['time', 'truth', 'prediction'])
        del df['time']

        df['correct'] = df['truth'] == df['prediction']

        cum_acc = np.cumsum(df['correct']) / np.arange(1, len(df['correct']) + 1)

        df['cum_acc'] = cum_acc

        df = df.add_prefix('node{}_'.format(idx))
        pred_list.append(df)

    pred_df = pd.concat(pred_list, axis=1)

    return pred_df


# def extract_communication_adjusted(experiment_path, dimension, bytes_per_param=8, bytes_per_mu=8, send_mu=False, send_theta=False):
def extract_communication_adjusted(experiment_path, bytes_out_msg, bytes_in_msg):
    cum_communication_bytes = 0
    worker_dirs = [os.path.join(experiment_path, d) for d in os.listdir(experiment_path) if d.startswith('worker')]

    # message_size_bytes = 0
    # if send_mu:
    #     message_size_bytes += dimension * bytes_per_mu
    # if send_theta:
    #     message_size_bytes += dimension * bytes_per_param

    for worker in worker_dirs:
        # Check send parameters.
        if os.path.exists(os.path.join(worker, 'communication', 'violations.txt')):
            with open(os.path.join(worker, 'communication', 'violations.txt'), 'r') as v_file:
                n_violations = len(v_file.readlines())
                cum_communication_bytes += n_violations * bytes_out_msg

        # Check received parameters.
        if os.path.exists(os.path.join(worker, 'communication', 'send_model.txt')):
            with open(os.path.join(worker, 'communication', 'send_model.txt'), 'r') as v_file:
                n_models_received = len(v_file.readlines())
                cum_communication_bytes += n_models_received * bytes_in_msg

        if os.path.exists(os.path.join(worker, 'communication', 'balancing.txt')):
            with open(os.path.join(worker, 'communication', 'balancing.txt'), 'r') as v_file:
                n_balances = len(v_file.readlines())
                cum_communication_bytes += n_balances * bytes_out_msg

    return cum_communication_bytes


BASE_DIR = '/home/lukas/workspace/dynamic-rc-averaging/dynamic-rc-results'


class ExperimentResult(object):

    def __init__(self, path, n_learner=0, bytes_send=0, num_violations=None, predictions=None, losses=None):
        self.path = path
        self.n_learner = n_learner

        self.bytes_send = bytes_send

        self.predictions = predictions
        self.losses = losses

        self.num_violations = num_violations

    def violations(self):
        return self.num_violations

    def mean_violations(self):
        return self.num_violations.mean()

    def total_violations(self):
        return self.num_violations.sum()

    def na_count(self):
        return (len(self.losses) - self.losses.count()).sum() / 4

    def mean_cum_acc(self):
        return self.predictions.filter(regex='cum_acc').mean(axis=1)

    def total_acc(self):
        return self.predictions.filter(regex='correct').mean(axis=0).mean()

    def correct_predictions(self):
        return self.predictions.filter(regex='correct').sum().sum()

    def in_correct_predictions(self):
        return self.predictions.filter(regex='correct').count().sum() - self.correct_predictions()

    def binned_accuracy(self, window_size):
        assert window_size > 0
        return self.predictions.filter(regex='correct').groupby(exp.predictions.index // window_size).mean().mean(axis=1)

    def total_cumulative_loss(self):
        return self.losses.filter(regex='cum').sum().sum()

    def mean_cumulative_loss(self):
        return self.losses.filter(regex='cum').mean(axis=1)

    def mean_local_loss(self):
        return self.losses.filter(regex='local').mean(axis=1)

    def mean_batch_loss(self):
        return self.losses.filter(regex='batch').mean(axis=1)

    def mean_holdout_loss(self):
        return self.losses.filter(regex='val').mean(axis=1)

    def eval_metric(self, metric):
        truth = self.predictions.filter(regex='truth').stack()
        preds = self.predictions.filter(regex='prediction').stack()
        return metric(truth, preds)

    @classmethod
    def from_dir(cls, _dir):
        _losses = extract_losses(_dir)
        # _bytes = extract_communication(_dir)
        _n_learner = n_learner(_dir)
        _preds = extract_predictions(_dir)

        return cls(path=_dir, n_learner=_n_learner, bytes_send=0, predictions=_preds, losses=_losses)

    @classmethod
    def get_result(cls, g_type, agg_type, dataset, exp_type, sync_type, nodes=16, m_samples=1500, period=1, batch_size=1, delta=0.0, budget=1):
        _dir = os.path.join(BASE_DIR, g_type, agg_type, dataset, exp_type, sync_type, f'nodes{nodes}',
                            f'period{period}_examples{m_samples}_batchsize{batch_size}_delta{delta}_budget{budget}')

        # if

        _losses = extract_losses(_dir)
        _bytes_per_param = 8 if exp_type == 'normal' else 3 / 8
        # TODO: Is this right??
        _bytes_per_mu = 8  # if exp_type == 'normal' else 1

        _num_parameter = DATA_SET_DIMENSION[dataset]

        if agg_type == 'CENTRALIZED':
            # We send mu and recv
            bytes_out_msg = _num_parameter * _bytes_per_mu
            bytes_in_msg = _num_parameter * _bytes_per_param
        elif agg_type == 'PRIVATE':
            # We send and recv theta
            bytes_out_msg = _num_parameter * _bytes_per_param
            bytes_in_msg = _num_parameter * _bytes_per_param
        elif agg_type == 'PUBLIC':
            # We send and recv mu and theta.
            bytes_out_msg = _num_parameter * (_bytes_per_param + _bytes_per_mu)
            bytes_in_msg = _num_parameter * (_bytes_per_param + _bytes_per_mu)

        _bytes = extract_communication_adjusted(_dir, bytes_out_msg, bytes_in_msg)
        _n_learner = n_learner(_dir)
        _preds = extract_predictions(_dir)
        _n_violations = num_violations(_dir)

        return cls(path=_dir, n_learner=_n_learner, bytes_send=_bytes, num_violations=_n_violations, predictions=_preds, losses=_losses)


def num_violations(_path):
    worker_dirs = [os.path.join(_path, d) for d in os.listdir(_path) if d.startswith('worker')]

    violations = np.zeros(shape=len(worker_dirs))

    for idx, worker in enumerate(worker_dirs):
        # Check send parameters.
        if os.path.exists(os.path.join(worker, 'communication', 'violations.txt')):
            with open(os.path.join(worker, 'communication', 'violations.txt'), 'r') as v_file:
                violations[idx] = len(v_file.readlines())
    return violations


if __name__ == '__main__':
    exp = ExperimentResult.from_dir(
        '/home/lukas/workspace/dynamic-rc-averaging/dynamic-rc-results/spatial/PRIVATE/DOTA2/rc/periodic/nodes16/period1_examples1500_batchsize1_delta0.0')

    string = '/home/lukas/workspace/dynamic-rc-averaging/dynamic-rc-results/spatial/PRIVATE/DOTA2/rc/periodic/nodes16/period1_examples1500_batchsize1_delta0.0'

    print(exp)

    print(exp.eval_metric(accuracy_score))
    print(exp.eval_metric(lambda x, y: f1_score(x, y, average='weighted')))
    print(exp.total_acc())
    print(exp.correct_predictions())

    # accuracy_score
