import argparse
import os

import matplotlib.pyplot as plt
import numpy as np

from old.plot_util import texify


def get_baseline_result(dir, dataset, type):
    _file_train_mrf = os.path.join(dir, 'baseline', dataset, type, 'train.txt')
    _file_test_mrf = os.path.join(dir, 'baseline', dataset, type, 'test.txt')
    return np.loadtxt(_file_train_mrf, delimiter=','), np.loadtxt(_file_test_mrf, delimiter=',')


if __name__ == '__main__':

    # TODO: Allow subset of datasets??
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--with-train', default=True, help='Also plot train likelihood.')
    parser.add_argument('-o', '--output-dir', default='/tmp', help='Directory to store generated plots.')

    args, unknown = parser.parse_known_args()

    texify()

    # root_dir = os.path.join(os.path.dirname(__file__), '..', '..')
    DATA_DIR = os.path.join('/home/lukas/workspace/dynamic-rc-averaging/dynamic-rc-results')

    print('Reading results from:', DATA_DIR)

    if not os.path.exists(DATA_DIR):
        raise RuntimeError('Result directory -- {} -- does not exist. Please run experiments before evaluation.')

    MODEL_TYPE_DIRS = ['rc', 'normal']

    # Extract data
    for _dataset_dir in os.listdir(DATA_DIR):
        print(_dataset_dir)
        file_train_mrf = os.path.join(DATA_DIR, _dataset_dir, 'normal', 'train.txt')
        file_train_int = os.path.join(DATA_DIR, _dataset_dir, 'rc', 'train.txt')

        file_test_mrf = os.path.join(DATA_DIR, _dataset_dir, 'normal', 'test.txt')
        file_test_int = os.path.join(DATA_DIR, _dataset_dir, 'rc', 'test.txt')

        ll_train_mrf, ll_train_int = get_baseline_result(DATA_DIR, _dataset_dir, 'normal')
        # np.loadtxt(file_train_mrf, delimiter=',')
    ll_train_int = np.loadtxt(file_train_int, delimiter=',')

    ll_test_mrf = np.loadtxt(file_test_mrf, delimiter=',')
    ll_test_int = np.loadtxt(file_test_int, delimiter=',')

    x_values = np.arange(len(ll_train_mrf))

    fig, axs = plt.subplots(2)

    # TODO: Those solutions are ugly atm.. Maybe just do it within tex.
    # Plot title
    # fig.text(0.5, 1.05, 'Centralized Experiments', ha='center')
    # fig.suptitle('Centralized Experiments', y=0.98)

    # Shared X-Label
    fig.text(0.5, 0.01, 'Iterations', ha='center')
    # Shared Y-Label
    fig.text(0, 0.5, 'Neg. avg. Log-Likelihood', va='center', rotation='vertical')

    if args.with_train:
        axs[0].plot(x_values, ll_train_mrf, color='red')
    axs[0].plot(x_values, ll_test_mrf, color='blue')
    axs[0].set_title('Regular MRF')

    if args.with_train:
        axs[1].plot(x_values, ll_train_int, color='red')
    axs[1].plot(x_values, ll_test_int, color='blue')
    axs[1].set_title('Resource-Constrained MRF')

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    out_file = os.path.join(args.output_dir, '{}_baseline.pdf'.format(_dataset_dir))

    # plt.subplots_adjust(top=0.7)
    plt.tight_layout()
    fig.savefig(out_file)
    # Cleanup
