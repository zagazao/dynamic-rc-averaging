import argparse
import os

import numpy as np
import pandas as pd

mode_mapper = {
    'normal': 'modeltype.mrf',
    'rc': 'modeltype.integer',
}

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-dir', help='Path to result directory.', default='/home/lukas/workspace/dynamic-rc-averaging/dynamic-rc-results')
    parser.add_argument('-o', '--output-dir', help='Path to ', default='/home/lukas/workspace/dynamic-rc-averaging/data/')

    args = parser.parse_args()

    data = {}

    for dataset in ['dota2', 'covertype', 'susy']:
        for mode in ['normal', 'rc']:
            mode = mode_mapper[mode]
            for split in ['train', 'test']:
                colname = '_'.join([dataset, mode, split])
                data[colname] = np.loadtxt(os.path.join(args.input_dir, 'baseline', dataset, mode, '{}.txt'.format(split)), delimiter=',')

    df = pd.DataFrame(data)

    df.to_csv(os.path.join(args.output_dir, 'baseline.csv'), index=False, header=True, sep=',')
