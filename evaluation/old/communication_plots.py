import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

for dataset in ['covertype', 'dota2', 'susy']:
    path = f'/home/lukas/workspace/dynamic-rc-averaging/data/{dataset}/communication_df.csv'
    df = pd.read_csv(path, index_col=None)

    # Convert bytes to megabytes
    df = df / 1024.0 / 1024.0

    x_max = len(df)

    df.plot()

    print(df)
    x_ticks = np.power(2, np.arange(0, x_max))

    plt.xticks(np.arange(0, x_max), x_ticks)
    plt.xlabel('Number of nodes')
    plt.ylabel('Communication (MB)')
    plt.title(dataset)
    plt.savefig(f'/tmp/communication_{dataset}.pdf')
    plt.clf()
