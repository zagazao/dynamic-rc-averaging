import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from old.plot_util import texify

if __name__ == '__main__':
    base_dir = '/home/lukas/workspace/dynamic-rc-averaging/data'

    normal_description = 'period1_examples5000_batchsize1_delta0.0'
    rc_description = 'period1_examples5000_batchsize1_delta5.0'
    periodic_description = 'period1_examples10000_batchsize5_delta0.0'

    normal_description = 'period1_examples10000_batchsize5_delta0.1'
    rc_description = 'period1_examples10000_batchsize5_delta1.0'

    nodes = 'nodes128'

    texify()

    for dataset in ['susy', 'dota2', 'covertype']:
        dataset_dir = os.path.join(base_dir, dataset, nodes)

        baseline = pd.read_csv('/home/lukas/workspace/dynamic-rc-averaging/data/baseline.csv', index_col=False, delimiter=',')

        normal_periodic = pd.read_csv(f'{dataset_dir}/losses_normal_periodic_{periodic_description}.csv', index_col=False, delimiter=',')
        rc_periodic = pd.read_csv(f'{dataset_dir}/losses_rc_periodic_{periodic_description}.csv', index_col=False, delimiter=',')

        normal_dynamic = pd.read_csv(f'{dataset_dir}/losses_normal_dynamic_{normal_description}.csv', index_col=False, delimiter=',')
        rc_dynamic = pd.read_csv(f'{dataset_dir}/losses_rc_dynamic_{rc_description}.csv', index_col=False, delimiter=',')

        streamed_baseline_normal = f'{base_dir}/{dataset}/nodes1/losses_normal_periodic_{periodic_description}.csv'
        streamed_baseline_rc = f'{base_dir}/{dataset}/nodes1/losses_rc_periodic_{periodic_description}.csv'

        streamed_baseline_normal = pd.read_csv(streamed_baseline_normal, index_col=False)
        streamed_baseline_rc = pd.read_csv(streamed_baseline_rc, index_col=False)

        streamed_baseline_normal_min = streamed_baseline_normal.min()[0]
        streamed_baseline_rc = streamed_baseline_rc.min()[0]

        print(baseline)

        n = 25

        x = np.arange(len((normal_periodic.mean(axis=1))))

        x_normal = baseline['{}_{}_test'.format(dataset.lower(), 'modeltype.mrf')]
        x_rc = baseline['{}_{}_test'.format(dataset.lower(), 'modeltype.integer')]

        plt.axhline(y=streamed_baseline_normal_min, color='blue', linestyle='--', label='MRF-Stream')
        plt.axhline(y=streamed_baseline_rc, color='green', linestyle='--', label='IntegerMRF-Stream')

        # Periodic
        plt.plot(x[::n], normal_periodic.mean(axis=1)[::n], 'x', color='blue', label='MRF-PA')
        plt.plot(x[::n], rc_periodic.mean(axis=1)[::n], 'x', color='green', label='IntegerMRF-PA')

        # Dynamic
        plt.plot(x[::n], normal_dynamic.mean(axis=1)[::n], '+', color='blue', label='MRF-DA')
        plt.plot(x[::n], rc_dynamic.mean(axis=1)[::n], '+', color='green', label='IntegerMRF-DA')

        plt.ylabel('Neg. avg. Log-Likelihood')
        plt.xlabel('Seen batches')

        plt.legend(loc='upper right')
        plt.savefig(f'/tmp/eval_{dataset}.pdf')

        plt.clf()
        # plt.show()
