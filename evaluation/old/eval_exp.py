import os
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt


class ExperimentResult(object):

    def __init__(self, path, n_learner=0, bytes_send=0, losses=None):
        self.path = path
        self.n_learner = n_learner

        self.bytes_send = bytes_send
        self.losses = losses

    @classmethod
    def from_dir(cls, _dir):
        _losses = extract_losses(_dir)
        _bytes = extract_communication(_dir)
        _n_learner = n_learner(_dir)
        return cls(path=_dir, n_learner=_n_learner, bytes_send=_bytes, losses=_losses)


def n_learner(path):
    files = os.listdir(path)
    worker_dirs = [os.path.join(path, f) for f in files if f.startswith('worker')]
    return len(worker_dirs)


def eval_exp(path):
    files = os.listdir(path)
    worker_dirs = [os.path.join(path, f) for f in files if f.startswith('worker')]
    losses_df = []
    for f in worker_dirs:
        f = os.path.join(f, 'losses.txt')
        df = pd.read_csv(f, sep='\t', header=None, index_col=None, names=['time', 'loss'])
        # df.columns = ['time', 'loss']
        print(len(df))
        losses_df.append(df)

    df_lengths = list(map(len, losses_df))
    all_same = len(set(df_lengths)) == 1
    if not all_same:
        raise RuntimeError('atm only support for equal.')
    df_len = df_lengths[0]
    df_n = len(losses_df)

    losses = np.zeros(shape=(df_n, df_len))
    for idx, df in enumerate(losses_df):
        losses[idx, :] = df['loss']

    print(losses)

    # print(len(losses_df))
    mean_loss = losses_df[0]['loss']
    for i in range(1, len(losses_df)):
        mean_loss = mean_loss + losses_df[i]['loss']
    mean_loss = mean_loss / len(losses_df)
    print(mean_loss / len(losses_df))

    print(losses.mean(axis=0))
    print(losses.std(axis=0))
    print(losses.var(axis=0))
    mean_loss = losses.mean(axis=0)
    mean_std = losses.std(axis=0)

    t = np.arange(32)

    # Plot mean ll with std (maybe make individual ll dashed grey?)
    lower_bound = mean_loss - mean_std
    upper_bound = mean_loss + mean_std

    plt.plot(losses.T, '--', color='grey')
    plt.fill_between(t, lower_bound, upper_bound, facecolor='green', alpha=0.5, label='std loss')
    plt.plot(t, mean_loss, label='mean loss')
    plt.show()


def extract_losses(path):
    print(path)

    files = os.listdir(path)
    print(files)
    worker_dirs = [os.path.join(path, f) for f in files if f.startswith('worker')]
    print(worker_dirs)
    losses_df = []
    for f in worker_dirs:
        f = os.path.join(f, 'losses.txt')
        df = pd.read_csv(f, sep='\t', header=None, index_col=None, names=['time', 'loss'])
        losses_df.append(df)

    df_lengths = list(map(len, losses_df))
    all_same = len(set(df_lengths)) == 1
    if not all_same:
        raise RuntimeError('atm only support for equal.')
    df_len = df_lengths[0]
    df_n = len(losses_df)

    losses = np.zeros(shape=(df_n, df_len))
    for idx, df in enumerate(losses_df):
        losses[idx, :] = df['loss']

    # print(losses)
    return losses


def extract_communication(path):
    # for d in experimentFolders:
    communication_bytes = 0
    for _file in os.listdir(os.path.join(path, "coordinator", "communication")):
        with open(os.path.join(path, "coordinator", "communication", _file), "r") as f:
            for line in f.readlines():
                parsed_line = line[:-1].split("\t")
                if len(parsed_line) == 5:
                    message_size = parsed_line[2].count(".")
                    communication_bytes += float(parsed_line[3]) * message_size
                else:
                    communication_bytes += float(parsed_line[4])
    return communication_bytes


def plot_mean_loss(_losses, _ax, title):
    """ Plots the losses to the given ax.."""
    mean_loss = _losses.mean(axis=0)
    mean_std = _losses.std(axis=0)

    t = np.arange(mean_loss.shape[0])

    # Plot mean ll with std (maybe make individual ll dashed grey?)
    lower_bound = mean_loss - mean_std
    upper_bound = mean_loss + mean_std

    _ax.plot(_losses.T, '--', color='grey')
    _ax.fill_between(t, lower_bound, upper_bound, facecolor='green', alpha=0.5, label='std loss')
    _ax.plot(t, mean_loss, label='mean loss')
    _ax.set_title(title)


if __name__ == '__main__':
    result_dynamic = ExperimentResult.from_dir('/home/lukas/data/mrf_COVERTYPE_dynamic_2020-04-21_09-51-34')
    result_periodic = ExperimentResult.from_dir('/home/lukas/data/mrf_COVERTYPE_periodic_2020-04-21_09-50-44')

    centralized = ExperimentResult.from_dir('/home/lukas/data/mrf_COVERTYPE_periodic_2020-04-21_10-36-36/')

    fig, axs = plt.subplots(3, 1)
    plot_mean_loss(centralized.losses, axs[0], 'centralized')
    plot_mean_loss(result_periodic.losses, axs[1], 'periodic')
    plot_mean_loss(result_dynamic.losses, axs[2], 'dynamic')

    fig.text(0.5, 0.01, 'Seen batches (size=32)', ha='center')
    fig.text(0, 0.5, 'Log Likelihood', va='center', rotation='vertical')
    #

    plt.tight_layout()
    plt.savefig('/home/lukas/fig1.pdf')
    plt.show()
