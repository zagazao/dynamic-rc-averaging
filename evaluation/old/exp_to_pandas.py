import argparse
import itertools
import os
from collections import OrderedDict

import numpy as np
import pandas as pd

from dlplatform_extension.px_dtype_cfg import DATA_SET_DIMENSION
from old.vary_nodes import rec_dd


def extract_losses(path):
    """
    Extract the losses for one experiment.
    :param path:
    :return:
    """

    files = os.listdir(path)
    worker_dirs = [os.path.join(path, f) for f in files if f.startswith('worker')]
    losses_df = []
    for idx, f in enumerate(worker_dirs):
        f = os.path.join(f, 'losses.txt')
        df = pd.read_csv(f, sep='\t', header=None, index_col=None, names=['time', 'loss_local', 'loss_batch', 'loss_val'])
        del df['time']

        df['loss_cum'] = df['loss_batch'].cumsum()
        df = df.add_prefix('node{}_'.format(idx))

        losses_df.append(df)

    return pd.concat(losses_df, axis=1)


def extract_predictions(path):
    """
    Extract the losses for one experiment.
    :param path:
    :return:
    """

    files = os.listdir(path)
    worker_dirs = [os.path.join(path, f) for f in files if f.startswith('worker')]
    pred_list = []
    for idx, f in enumerate(worker_dirs):
        f = os.path.join(f, 'predictions.txt')
        df = pd.read_csv(f, sep='\t', header=None, index_col=None, names=['time', 'truth', 'prediction'])
        del df['time']

        df['correct'] = df['truth'] == df['prediction']

        cum_acc = np.cumsum(df['correct']) / np.arange(1, len(df['correct']) + 1)

        df['cum_acc'] = cum_acc

        df = df.add_prefix('node{}_'.format(idx))
        pred_list.append(df)

    pred_df = pd.concat(pred_list, axis=1)

    return pred_df


def extract_communication_adjusted(experiment_path, dimension, bytes_per_param=64):
    cum_communication_bytes = 0
    worker_dirs = [os.path.join(experiment_path, d) for d in os.listdir(experiment_path) if d.startswith('worker')]

    message_size_bytes = dimension * bytes_per_param

    for worker in worker_dirs:
        # Check send parameters.
        if os.path.exists(os.path.join(worker, 'communication', 'violations.txt')):
            with open(os.path.join(worker, 'communication', 'violations.txt'), 'r') as v_file:
                n_violations = len(v_file.readlines())
                cum_communication_bytes += n_violations * message_size_bytes

        # Check received parameters.
        if os.path.exists(os.path.join(worker, 'communication', 'send_model.txt')):
            with open(os.path.join(worker, 'communication', 'send_model.txt'), 'r') as v_file:
                n_models_received = len(v_file.readlines())
                cum_communication_bytes += n_models_received * message_size_bytes

        if os.path.exists(os.path.join(worker, 'communication', 'balancing.txt')):
            with open(os.path.join(worker, 'communication', 'balancing.txt'), 'r') as v_file:
                n_balances = len(v_file.readlines())
                cum_communication_bytes += n_balances * message_size_bytes

    return cum_communication_bytes


def select_option_subset(_args, _options, _key):
    if getattr(_args, _key) is not None:
        _options[_key] = [getattr(_args, _key)]


def make_comb(options):
    # Dict
    keys = options.keys()
    values = (options[key] for key in keys)
    return [dict(zip(keys, combination)) for combination in itertools.product(*values)]


if __name__ == '__main__':

    options = {
        'dataset': ['DOTA2', 'COVERTYPE', 'SUSY', 'PGM', 'INTPGM', 'CDOTA2', 'CCOVERTYPE', 'CSUSY', 'CPGM', '1', 'TimePGM'],
        # 'dataset': ['INTPGM'],
        'experiment_type': ['normal', 'rc'],
        'sync_type': ['periodic', 'dynamic', 'nosync']
    }

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-dir', help='Path to result directory.', default='/home/lukas/workspace/dynamic-rc-averaging/dynamic-rc-results')
    parser.add_argument('-o', '--output-dir', help='Path to ', default='/home/lukas/workspace/dynamic-rc-averaging/data/')

    parser.add_argument('-s', '--sync-type', choices=options['sync_type'])
    parser.add_argument('-d', '--dataset', choices=options['dataset'])
    parser.add_argument('-e', '--experiment-type', choices=options['experiment_type'])

    args = parser.parse_args()

    select_option_subset(args, options, 'dataset')
    select_option_subset(args, options, 'experiment_type')
    select_option_subset(args, options, 'sync_type')

    combinations = make_comb(options)

    print(combinations)

    bytes_dict = rec_dd()

    comm = OrderedDict()
    # with multiprocessing.Pool(processes=4) as pool:

    for _dataset in options['dataset']:
        for _experiment_type in options['experiment_type']:
            for _sync_type in options['sync_type']:
                exp_path = os.path.join(args.input_dir, _dataset, _experiment_type, _sync_type)

                if not os.path.exists(exp_path):
                    continue
                bytes_dict[_dataset][_experiment_type][_sync_type] = []

                data = []
                # Sort the result directories ascending by number of nodes
                for node_exp in sorted(os.listdir(exp_path), key=lambda dir_name: int(dir_name[5:])):
                    # e.g. nodes[2,4,...]
                    node_exp_path = os.path.join(exp_path, node_exp)

                    for path3 in os.listdir(node_exp_path):
                        # path3: e.g. period1__examples50000_batchsize8

                        out_dir = os.path.join(args.output_dir, _dataset.lower(), node_exp)

                        real_exp_dir = os.path.join(node_exp_path, path3)
                        print(real_exp_dir)
                        # Now determine number of worker
                        n_worker = int(node_exp[5:])

                        losses_df = extract_losses(real_exp_dir)
                        pred_df = extract_predictions(real_exp_dir)

                        losses_df_name = 'losses_{}_{}_{}.csv'.format(_experiment_type, _sync_type, path3)
                        pred_df_name = 'preds_{}_{}_{}.csv'.format(_experiment_type, _sync_type, path3)
                        com_f_name = 'communication_{}_{}_{}.txt'.format(_experiment_type, _sync_type, path3)
                        #
                        # if all([os.path.exists(os.path.join(out_dir, f)) for f in [losses_df_name, pred_df_name, com_f_name]]):
                        #     print('All exists.')
                        #     continue

                        bytes_per_parameter = 1 if _experiment_type == 'rc' else 8
                        n_bytes = extract_communication_adjusted(real_exp_dir, DATA_SET_DIMENSION[_dataset], bytes_per_parameter)

                        print(n_bytes / 1024.0 / 1024.0, 'MB used.')
                        bytes_dict[_dataset][_experiment_type][_sync_type].append(n_bytes)
                        comm[real_exp_dir] = n_bytes

                        out_dir = os.path.join(args.output_dir, _dataset.lower(), node_exp)
                        if not os.path.exists(out_dir):
                            os.makedirs(out_dir)

                        losses_df.to_csv(os.path.join(out_dir, losses_df_name), index=False, header=True)
                        pred_df.to_csv(os.path.join(out_dir, pred_df_name), index=False, header=True)
                        with open(os.path.join(out_dir, com_f_name), 'w') as f:
                            f.write(f'{n_bytes}\n')
