import argparse
import os
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from old.plot_util import texify


def rec_dd():
    return defaultdict(rec_dd)


mode_label_mapping = {
    'normal': 'MRF',
    'rc': 'IntegerMRF'
}

sync_label_mapping = {
    'periodic': 'PA',
    'dynamic': 'DA'
}


def get_delta(_e_type, _s_type):
    if s_type == 'periodic':
        return 0.0
    if e_type == 'rc':
        return 2.0
    else:
        return 0.1


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-dir', default='/home/lukas/workspace/dynamic-rc-averaging/data')

    args = parser.parse_args()

    texify()
    # TODO: Add dota2 as soon as they are ready...
    datasets = ['susy', 'covertype', 'dota2']

    base_dir = args.input_dir

    exp_type = ['normal', 'rc']
    sync_types = ['periodic', 'dynamic']
    sync_period = 1
    examples = 10000
    batchsize = 5
    delta = 1.0

    # result -> dataset -> mode -> sync -> nodes -> (mean, std)
    results = rec_dd()

    for dataset in datasets:
        dataset_dir = os.path.join(base_dir, dataset)

        for nodes_cfg in os.listdir(dataset_dir):
            if not nodes_cfg.startswith('nodes') or '256' in nodes_cfg:
                # Skip communication.csv
                continue
            print(os.path.join(dataset_dir, nodes_cfg))
            n_nodes = int(nodes_cfg[5:])
            print(n_nodes)
            for e_type in exp_type:
                for s_type in sync_types:
                    d = get_delta(e_type, s_type)

                    f_name = 'losses_{}_{}_period{}_examples{}_batchsize{}_delta{}.csv'.format(e_type, s_type, sync_period, examples, batchsize, d)
                    df_path = os.path.join(dataset_dir, nodes_cfg, f_name)
                    df = pd.read_csv(df_path, index_col=False)
                    # print(df)
                    best_losses = df.min(axis=0)
                    best_losses_mean = best_losses.mean()
                    best_losses_std = best_losses.std()

                    results[dataset][e_type][s_type][n_nodes] = (best_losses_mean, best_losses_std)

        print(results)

    for selected_dataset in datasets:
        x_max = 0
        for sync in sync_types:
            for mode in exp_type:

                cfg_result = results[selected_dataset][mode][sync]

                print(cfg_result)

                print(len(cfg_result))

                n = len(cfg_result)
                x_val = np.zeros(n)
                x_mean = np.zeros(n)
                x_stds = np.zeros(n)

                for idx, (key, val) in enumerate(cfg_result.items()):
                    x_val[idx] = key
                    x_mean[idx] = val[0]
                    x_stds[idx] = val[1]

                x_max = max(x_max, x_mean.max())

                print(x_val)
                print(x_mean)
                print(x_stds)

                label = mode_label_mapping[mode] + '-' + sync_label_mapping[sync]

                plt.errorbar(x_val, x_mean, fmt='o', yerr=x_stds, label=label)
        plt.gca().set_xscale('log', basex=2)
        plt.xticks(x_val)

        print(x_max)
        plt.ylim((0, x_max + 5))

        plt.ylabel('Avg. Log-Likelihood')
        plt.xlabel('Number of learner')
        plt.title(dataset)
        plt.legend()
        plt.tight_layout()
        plt.savefig('/tmp/vary_nodes_{}.pdf'.format(selected_dataset))
        plt.show()
        plt.clf()
