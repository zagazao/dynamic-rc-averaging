import matplotlib.pyplot as plt

from drift import metric_communication_plot
from exp_result import ExperimentResult

TYPE = 'spatial'

DATASETS = ['DOTA2', 'COVERTYPE', 'SUSY']

fig, axs = plt.subplots(3, 1)
for dataset_idx, dataset in enumerate(DATASETS):
    metric_list = []
    for i, EXP_TYPE in enumerate(['normal', 'rc']):
        _exp_nosync = ExperimentResult.get_result(TYPE, 'PRIVATE', dataset, EXP_TYPE, 'nosync', 16, 1500, 1, 1)
        print(_exp_nosync.total_cumulative_loss())
        _exp_nosync = ExperimentResult.get_result(TYPE, 'PUBLIC', dataset, EXP_TYPE, 'nosync', 16, 1500, 1, 1)
        print(_exp_nosync.total_cumulative_loss())
        axs[dataset_idx].axhline(_exp_nosync.total_cumulative_loss(), linestyle='--', label='No synchronisation', color='black')
        for j, AGG_MECHANISM in enumerate(['CENTRALIZED', 'PUBLIC', 'PRIVATE']):
            # _com, _metric, _label
            # Read periodic and dynamic
            # Period 1 atm

            _exp_periodic_centralized = ExperimentResult.get_result(TYPE, AGG_MECHANISM, dataset, EXP_TYPE, 'periodic', 16, 1500, 1, 1)
            _exp_dynamic_centralized = ExperimentResult.get_result(TYPE, AGG_MECHANISM, dataset, EXP_TYPE, 'dynamic', 16, 1500, 1, 1, delta=1.0)
            metric_list.append((_exp_periodic_centralized.bytes_send, _exp_periodic_centralized.total_cumulative_loss(), f'Periodic-{EXP_TYPE}-{AGG_MECHANISM}'))
            metric_list.append((_exp_dynamic_centralized.bytes_send, _exp_dynamic_centralized.total_cumulative_loss(), f'Dynamic-{EXP_TYPE}-{AGG_MECHANISM}'))

    print(metric_list)
    # axs[dataset_idx].axhline(_exp_nosync.total_cumulative_loss(), linestyle='--', label='No synchronisation', color='black')
    metric_communication_plot(axs[dataset_idx], metric_list, dataset)

handles, labels = plt.gca().get_legend_handles_labels()
fig.legend(handles, labels, loc=8, ncol=3)

fig.tight_layout()
fig.subplots_adjust(bottom=0.3)

# axs[1].legend(loc='center left', bbox_to_anchor=(1, 0.5), fancybox=True, shadow=True, ncol=1)
# plt.subplots_adjust(right=0.6)
# fig.text(0.5, 0.01, 'Seen batches (size=32)', ha='center')
# fig.text(0, 0.5, 'Log Likelihood', va='center', rotation='vertical')

# plt.tight_layout(pad=3.0)
plt.savefig('/tmp/testplot.pdf')
# plt.show()
