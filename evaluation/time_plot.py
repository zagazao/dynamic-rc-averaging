import matplotlib.pyplot as plt
import seaborn as sns

from exp_result import ExperimentResult, baseline_results, delta_dicts

sns.set_palette(sns.color_palette("hls", 30))

palette = sns.color_palette("hls", 40)

# Default figsize is 6.4, 4.8

fig, axs = plt.subplots(3, 3, sharey='row', sharex='col', figsize=(10, 5))  # width, height

TYPE = 'equal'

DATASETS = ['COVERTYPE', 'SUSY', 'DOTA2']

NODES = 16

periods = [1, 10, 20, 50.0, 100.0, 200.0]

LIMITS = {
    'normal': {
        'COVERTYPE': (20, 60),
        'SUSY': (32, 43),
        'DOTA2': (40, 130), },
    'rc': {
        'COVERTYPE': (30, 58),
        'SUSY': (39, 42.5),
        'DOTA2': (52, 130),
    }
}

# periodic_palette = sns.light_palette("orange", n_colors=4)
# dynamic_palette = sns.light_palette("orange", n_colors=4)
#
# label_color_map = {
#     'NoSync': 'black',
#     'Global': 'black',
#     'Periodic(p=1)': periodic_palette[0],
#     'Periodic(p=2)': periodic_palette[1],
#     'Periodic(p=10)': periodic_palette[2],
#     'Periodic(p=20)': periodic_palette[3],
#
#     # 'Dynamic(p=200)': palette[13],
# }

fn = 'mean_holdout_loss'
# fn = 'mean_cum_acc'

# MODE = 'rc'
MODE = 'normal'

rows = DATASETS

pad = 5  # in points
burn_in_begin = 0

# https://stackoverflow.com/questions/25812255/row-and-column-headers-in-matplotlibs-subplots
# Annotate the rows with dataset names
for ax, row in zip(axs[:, 0], rows):
    ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                xycoords=ax.yaxis.label, textcoords='offset points',
                size='large', ha='right', va='center', rotation=90)

# Annotate columns with type (e.g. Centralized, public, private)
for ax, col in zip(axs[0], ['CENTRALIZED', 'PUBLIC', 'PRIVATE']):
    ax.annotate(col, xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                size='large', ha='center', va='baseline')

for _d_idx, _dataset in enumerate(DATASETS):
    global_centralized = baseline_results[(_dataset, MODE)]
    for j, _agg_type in enumerate(['CENTRALIZED', 'PUBLIC', 'PRIVATE']):

        ax = axs[_d_idx, j]
        # ax.grid(True) (nur

        ax.axhline(global_centralized, linestyle='-', color='black', label='Global.')

        ax.set_ylim(LIMITS[MODE][_dataset])

        if _agg_type != 'CENTRALIZED':
            # Read nosync.
            no_sync = ExperimentResult.get_result(TYPE, _agg_type, _dataset, MODE, 'nosync', NODES, m_samples=1500, period=1, batch_size=10, budget=10)
            ax.plot(getattr(no_sync, fn)()[burn_in_begin:], '--', label='NoSync', color='black')

        for _period in periods:
            try:
                _exp_periodic_centralized = ExperimentResult.get_result(TYPE, _agg_type, _dataset, MODE, 'periodic', NODES, 1500, period=_period, batch_size=10, budget=10)
                label = f'Periodic(p={_period})'  #
                ax.plot(getattr(_exp_periodic_centralized, fn)()[burn_in_begin:], label='Periodic(p={})'.format(_period))  # color=label_color_map[label],
            except:
                pass

        for _delta in delta_dicts['equal'][(_dataset, MODE, _agg_type)]:
            try:
                _exp_dynamic = ExperimentResult.get_result(TYPE, _agg_type, _dataset, MODE, 'dynamic', NODES, 1500, period=20, batch_size=10, budget=10, delta=_delta)
                ax.plot(getattr(_exp_dynamic, fn)()[burn_in_begin:], label='Dynamic(d={})'.format(_delta))  # linestyle='-.',
            except:
                pass

# labelx = -0.3  # axes coords
#
# for j in range(3):
#     axs[j, 0].yaxis.set_label_coords(labelx, 0.5)

# plt.legend()
handles, labels = ax.get_legend_handles_labels()
fig.legend(handles, labels, loc=7, ncol=2)
fig.text(0, 0.5, 'Mean. Neg. Avg. Likelihood (Holdout)', va='center', rotation='vertical')
fig.text(0.3, 0.02, 'Processed batches', va='center', rotation='horizontal')
fig.tight_layout()
fig.subplots_adjust(left=0.1, bottom=0.1, right=0.6, wspace=0.1, hspace=0.1)

plt.savefig(f'/tmp/{MODE}_mean_holdout_likelihood.pdf')
plt.show()
