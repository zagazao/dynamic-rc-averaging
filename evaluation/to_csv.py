import itertools

import pandas as pd

from exp_result import ExperimentResult, delta_dicts

NODES = 16

PERIODS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 50, 100, 200]

budgets = [1, 10, 18, 116, 54]
batch_size = [1, 10]

comb = list(itertools.product(budgets, batch_size))
print(comb)


def extract_stats(_experiment):
    return [_experiment.bytes_send, _experiment.total_acc(), _experiment.correct_predictions(),
            _experiment.in_correct_predictions(), _experiment.total_cumulative_loss(),
            _experiment.na_count(), _experiment.total_violations(), _experiment.mean_violations()]


if __name__ == '__main__':
    # pd.

    # out = pd.DataFrame()

    out_data = []

    cols = ['ENVIRONMENT', 'AGG', 'DATASET', 'MODE', 'SYNC', 'PERIOD', 'DELTA', 'BATCH_SIZE', 'BUDGET', 'BYTES_SEND', 'ACCURACY', 'CORRECT_PREDS', 'WRONG_PREDS',
            'CUMULATIVE_BATCH_LOSS', 'NA_COUNT', 'TOTAL_VIOLATIONS', 'MEAN_VIOLATIONS']

    for _env in ['equal']: # , 'spatial'
        for _agg in ['CENTRALIZED', 'PUBLIC', 'PRIVATE']:
            for _dataset in ['SUSY', 'COVERTYPE', 'DOTA2']:
                for _exp_type in ['normal', 'rc']:

                    # Read nosync
                    if _agg != 'CENTRALIZED':
                        for budget, _batch_size in comb:
                            # Centralized has no budget => only batchsize
                            try:
                                no_sync = ExperimentResult.get_result(_env, _agg, _dataset, _exp_type, 'nosync', NODES, 1500, 1, batch_size=_batch_size, budget=budget)

                                no_sync_line = [_env, _agg, _dataset, _exp_type, 'nosync', 0, 0.0, _batch_size, budget] + extract_stats(no_sync)

                                out_data.append(no_sync_line)
                            except FileNotFoundError:
                                pass

                    for period in PERIODS:
                        for budget, _batch_size in comb:
                            try:
                                _periodic = ExperimentResult.get_result(_env, _agg, _dataset, _exp_type, 'periodic', NODES, 1500, period, batch_size=_batch_size, budget=budget)
                                out_data.append(
                                    [_env, _agg, _dataset, _exp_type, 'periodic', period, 0.0, _batch_size, budget] + extract_stats(_periodic))
                            except FileNotFoundError:
                                pass
                            except ValueError:
                                print('WHY')

                    for period in [1, 20]:
                        for delta in delta_dicts[_env][(_dataset, _exp_type, _agg)]:
                            for budget, _batch_size in comb:
                                try:
                                    _dynamic = ExperimentResult.get_result(_env, _agg, _dataset, _exp_type, 'dynamic', NODES, 1500, period=period, batch_size=_batch_size,
                                                                           delta=delta, budget=budget)
                                    out_data.append(
                                        [_env, _agg, _dataset, _exp_type, 'dynamic', period, delta, _batch_size, budget] + extract_stats(_dynamic))
                                except FileNotFoundError:
                                    pass
    out = pd.DataFrame(out_data, columns=cols)

    # out.sort_values(by='BYTES_SEND')
    out.to_csv('/tmp/experiment_summary.csv', header=True, index=False)
    print(out)
