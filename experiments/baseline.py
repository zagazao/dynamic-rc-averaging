import argparse
import os
import signal
import sys
from collections import defaultdict, OrderedDict

import numpy as np
import pandas as pd
import pxpy
from networkx import fast_gnp_random_graph
from sklearn.metrics import accuracy_score
from sklearn.model_selection import KFold

from dlplatform_extension.callback import MultiCallbackWrapper, WeightsLoggerCallback, LikelihoodEvaluator
from dlplatform_extension.learner.px_helper import network_x_to_edgelist, estimate_structure, generate_model
from dlplatform_extension.px_dtype_cfg import STATES, DATA_DIR

results = {
    ('CSUSY', 'rc'): 4.028405666669150520e+01,
    ('CSUSY', 'normal'): 3.427764429308776073e+01,
    ('CDTOA2', 'rc'): 5.656380414610265461e+01,
    ('CDOTA2', 'normal'): 4.170428214453318105e+01,
    ('CCOVERTYPE', 'rc'): 3.307903005731182589e+01,
    ('CCOVERTYPE', 'normal'): 2.428175784334152354e+01
}


def signal_handler(sig, frame):
    sys.exit(0)


def predict_holdout(model, _df_holdout):
    correct = 0
    _df_copy = _df_holdout.copy()
    _df_copy[0] = pxpy.MISSING_VALUE

    _truth = _df_holdout[0]

    x = np.ascontiguousarray(_df_copy, dtype=np.uint16)
    model.predict(x)
    preds = x[:, 0]
    return accuracy_score(y_true=np.array(_truth), y_pred=preds)
    #
    # for index, row in _df_holdout.iterrows():
    #     true = row[0]
    #     x = np.ascontiguousarray(row, dtype=np.uint16)
    #     x[0] = pxpy.MISSING_VALUE
    #     model.predict(x)
    #     correct += (x[0] == true)
    # return correct / len(_df_holdout)


if __name__ == '__main__':

    # Without the signal handler we can't exit the native calls nicely..
    signal.signal(signal.SIGINT, signal_handler)

    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--int', action='store_true')
    group.add_argument('--mrf', action='store_true')

    parser.add_argument('-k', type=int, help='Number of bits for integer parameter.', default=8)
    parser.add_argument('-i', '--max-iter', type=int, help='Max GD iterations.', default=10000)
    parser.add_argument('-f', '--train-fraction', type=float, help='Fraction of data for training.', default=0.8)
    parser.add_argument('--seed', type=int, help='Seed for shuffling and split.', default=876)

    parser.add_argument('-r', '--root-dir', type=str, default=DATA_DIR, help='Root dir for datasets.')
    parser.add_argument('-o', '--out-dir', type=str, default='/tmp/out/', help='Directory to store the results.')

    args, unknown = parser.parse_known_args()

    # By default run both model types.
    modes = [pxpy.ModelType.mrf, pxpy.ModelType.integer]
    if args.int:
        modes = [pxpy.ModelType.integer]
    elif args.mrf:
        modes = [pxpy.ModelType.mrf]

    max_iter = args.max_iter
    train_fraction = args.train_fraction

    k = args.k

    np.random.seed(args.seed)
    pxpy.set_seed(args.seed)

    data = OrderedDict({
        'SUSY': 'SUSY/SUSY.disc.csv',
        'DOTA2': 'DOTA2/DOTA2.disc.csv',
        'COVERTYPE': 'COVERTYPE/COVERTYPE.disc.csv',
    })

    data = {key: os.path.join(args.root_dir, val) for key, val in data.items()}

    lls = defaultdict(list)

    for key, val in data.items():
        print('{}{} -- Loading dataset from {}.'.format(os.linesep, key, val))

        # 10.000 samples for the
        df_holdout = pd.read_csv(val + '.holdout', header=None, index_col=None)

        data = pd.read_csv(val, header=None, index_col=None)

        n_samples, n_variables = data.shape

        idx = np.arange(n_samples)
        np.random.shuffle(idx)

        first_test = int(train_fraction * n_samples)
        train_idx, test_idx = idx[:first_test], idx[first_test:]

        holdout_data = np.ascontiguousarray(df_holdout.values, dtype=np.uint16)

        train_data = np.ascontiguousarray(data.iloc[train_idx], dtype=np.uint16)
        test_data = np.ascontiguousarray(data.iloc[test_idx], dtype=np.uint16)

        if key in ['PGM', 'INTPGM']:
            SEED = 876

            n = 25
            p = 0.3
            g = fast_gnp_random_graph(n, p, seed=SEED)
            edge_list = network_x_to_edgelist(g)
        else:
            # Estimate the graph structure on a holdout set.
            edge_list = estimate_structure(holdout_data)

        for mode in modes:
            print(mode)

            cb_wrapper = MultiCallbackWrapper({'weight_cb': WeightsLoggerCallback()})

            scores = []
            for train_idx, test_idx in KFold(n_splits=5, shuffle=True).split(data):
                print('--' * 20)
                train = np.ascontiguousarray(data.iloc[train_idx], dtype=np.uint16)

                _m = generate_model(train_data, STATES[key], mode, edge_list, k=k)

                # print(_m.num_instances)
                _m = pxpy.train(mode=mode, iters=max_iter, input_model=_m, opt_progress_hook=cb_wrapper, k=k)
                print('Fit ready.')

                acc = predict_holdout(_m, data.iloc[test_idx])
                print(acc)
                scores.append(acc)

                # print('Accuracy:', predict_holdout(_m, df_holdout))
            print('Mean acc:', np.array(scores).mean())

            _m = generate_model(train_data, STATES[key], mode, edge_list, k=k)
            print('Training on {} instances. Model dimension is {}'.format(_m.num_instances, _m.dim))

            # print(_m.num_instances)
            _m = pxpy.train(mode=mode, iters=max_iter, input_model=_m, opt_progress_hook=cb_wrapper, k=k)
            print('Fit ready.')

            print('Accuracy:', predict_holdout(_m, df_holdout))

            likelihood_evaluator = LikelihoodEvaluator(cb_wrapper['weight_cb'].weights, edge_list, STATES[key], scale_weights=mode == pxpy.ModelType.integer)

            train_ll = likelihood_evaluator.eval(train_data)
            test_ll = likelihood_evaluator.eval(test_data)

            # I don't trust .obj...
            print('Obj (train):', _m.obj)

            print(train_ll)
            print(test_ll)
            print('Obj (train):', train_ll[-1])
            print('Obj (test):', test_ll[-1])

            # Write LL results to ${out_dir}/baseline/dataset/mode/{train,test}.txt

            BASE_DIR = os.path.join(args.out_dir, 'baseline')
            CURR_DIR = os.path.join(BASE_DIR, key, str(mode)).lower()

            # Assert dir exists
            os.makedirs(CURR_DIR, exist_ok=True)

            print('Saving results to {}.'.format(CURR_DIR))

            np.savetxt(os.path.join(CURR_DIR, 'train.txt'), train_ll, delimiter=",")
            np.savetxt(os.path.join(CURR_DIR, 'test.txt'), test_ll, delimiter=",")
