import math
import os

import pandas as pd

from DLplatform.coordinator import InitializationHandler
from DLplatform.stopping import MaxAmountExamples
from dlplatform_extension import LocalExperiment
from dlplatform_extension.averaging.centralized_average import *
from dlplatform_extension.datasource.decoder import CSVTypedDecoder
from dlplatform_extension.datasource.spatial_dataset import SpatialDataSetProvider
from dlplatform_extension.datasource.standardDataSourceFactories import FileDataSourceFactory
from dlplatform_extension.learner.px_helper import estimate_mu, estimate_structure
from dlplatform_extension.px_dtype_cfg import STATES
from dlplatform_extension.utils import log_str, get_sync_op, EXPERIMENT_TYPES, SYNC_TYPES, get_base_parser, select_option_subset, make_filtered_combinations, get_averaging_op, \
    learner_factory_selector


class SpatialExperimentRunner(object):

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def run(self):
        log_str('Starting experiment...\n')

        experiment_type = self.experiment_type
        sync_op = get_sync_op(self.sync_type, self.delta)

        # dataset_dir, dataset_name, num_nodes,
        spatial_dataset_provider = SpatialDataSetProvider(self.root_dir, self.dataset, self.nodes)
        df_path, hold_out_df_path = spatial_dataset_provider.give()

        hold_out_df = pd.read_csv(hold_out_df_path, index_col=None, header=None)

        edge_list = estimate_structure(hold_out_df)
        holdout_mu = estimate_mu(hold_out_df, STATES[self.dataset], edge_list)

        fs_factory = FileDataSourceFactory(filename=df_path, decoder=CSVTypedDecoder(labelCol=0, dtype=np.uint16),
                                           numberOfNodes=self.nodes, cache=False)

        learner_factory = learner_factory_selector[experiment_type](states=STATES[self.dataset],
                                                                    edge_list=edge_list,
                                                                    model_impl=self.aggregation_scheme,
                                                                    batch_size=self.batch_size,
                                                                    sync_period=self.sync_period,
                                                                    holdout_mu=holdout_mu,
                                                                    holdout_instances=len(hold_out_df))

        avg_op = get_averaging_op(self.experiment_type, self.aggregation_scheme)
        if self.aggregation_scheme == 'CENTRALIZED':
            avg_op = avg_op(edge_list, STATES[self.dataset], 1000)
        else:
            avg_op = avg_op()

        exp = LocalExperiment(messengerHost=self.rabbitmq_host, messengerPort=self.rabbitmq_port,
                              numberOfNodes=self.nodes, sync=sync_op,
                              aggregator=avg_op, learnerFactory=learner_factory,
                              dataSourceFactory=fs_factory, stoppingCriterion=MaxAmountExamples(self.max_samples),
                              initHandler=InitializationHandler(), sleepTime=0)

        # Write results to /${out_dir}/${experiment_type}_${dataset}_${sync_op}
        exp.run(os.path.join(self.out_dir, 'spatial', self.aggregation_scheme, self.dataset, self.experiment_type, self.sync_type, 'nodes{}'.format(self.nodes),
                             'period{}_examples{}_batchsize{}_delta{}'.format(self.sync_period, self.max_samples, self.batch_size, self.delta)))

        return self


if __name__ == '__main__':
    options = {
        'dataset': ['DOTA2', 'COVERTYPE', 'SUSY'],
        'experiment_type': EXPERIMENT_TYPES,
        'sync_type': SYNC_TYPES
    }

    parser = get_base_parser()

    parser.add_argument('-d', '--dataset', choices=options['dataset'])
    args = parser.parse_args()

    print('args', args)

    options['nodes'] = np.power(2, np.arange(args.nodes_lower, args.nodes_upper))

    # Subset selection of the options to rerun experiments.
    for arg_name in ['dataset', 'experiment_type', 'sync_type', 'nodes', 'sync_period', 'delta', 'aggregation_scheme', 'batch_size']:
        select_option_subset(args, options, arg_name)

    # Create combinations from command line
    combinations = make_filtered_combinations(options)

    experiment_cfg = vars(args)
    log_str('Running {} configurations in total.'.format(len(combinations)))

    for _idx, combination in enumerate(combinations):
        experiment_cfg.update(combination)
        log_str('[{}] - Running the following configuration : {}'.format(str(_idx).zfill(int(math.log10(len(combinations))) + 1), combination))

        if not args.dry_run:
            runnner = SpatialExperimentRunner(**experiment_cfg)
            runnner.run()
