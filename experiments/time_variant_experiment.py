import math
import os

import numpy as np

from DLplatform.coordinator import InitializationHandler
from DLplatform.stopping import MaxAmountExamples
from dlplatform_extension import LocalExperiment
from dlplatform_extension.datasource.pgmdatasource import TimeVariantPgmDataSourceFactory
from dlplatform_extension.learner.px_helper import generate_bayes_tree, calc_dim
from dlplatform_extension.utils import log_str, get_sync_op, get_base_parser, select_option_subset, make_filtered_combinations, learner_factory_selector, get_averaging_op, \
    EXPERIMENT_TYPES, SYNC_TYPES


class ExperimentRunner(object):

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def run(self):
        log_str(' Starting experiment...\n')

        experiment_type = self.experiment_type
        sync_op = get_sync_op(self.sync_type, self.delta)
        edge_list = generate_bayes_tree(self.vertices).astype(np.uint64)

        # Limit target node to two.
        states = np.full(shape=self.vertices, fill_value=self.states, dtype=np.uint64)
        states[0] = 2

        fs_factory = TimeVariantPgmDataSourceFactory(edgelist=edge_list, states=states,
                                                     pertubation_val=self.pertub_val, drift_prob=self.drift_prob, pertub_seed=0)

        learner_factory = learner_factory_selector[experiment_type](states=states,
                                                                    edge_list=edge_list,
                                                                    model_impl=self.aggregation_scheme,
                                                                    batch_size=self.batch_size,
                                                                    sync_period=self.sync_period,
                                                                    holdout_mu=np.zeros(calc_dim(edge_list, states)),
                                                                    holdout_instances=1)

        avg_op = get_averaging_op(self.experiment_type, self.aggregation_scheme)
        if self.aggregation_scheme == 'CENTRALIZED':
            avg_op = avg_op(edge_list, states, 1000)
        else:
            avg_op = avg_op()
        exp = LocalExperiment(messengerHost=self.rabbitmq_host, messengerPort=self.rabbitmq_port,
                              numberOfNodes=self.nodes, sync=sync_op,
                              aggregator=avg_op, learnerFactory=learner_factory,
                              dataSourceFactory=fs_factory, stoppingCriterion=MaxAmountExamples(self.max_samples),
                              initHandler=InitializationHandler(), sleepTime=0)

        # Write results to /${out_dir}/${experiment_type}_${dataset}_${sync_op}
        exp.run(os.path.join(self.out_dir, 'temporal', self.aggregation_scheme, self.dataset, self.experiment_type, self.sync_type, 'nodes{}'.format(self.nodes),
                             'period{}_examples{}_batchsize{}_delta{}'.format(self.sync_period, self.max_samples, self.batch_size, self.delta)))

        return self


if __name__ == '__main__':
    options = {
        'dataset': ['TimePGM'],
        'experiment_type': EXPERIMENT_TYPES,
        'sync_type': SYNC_TYPES
    }

    parser = get_base_parser()

    # Graph config
    parser.add_argument('--states', default=10, type=int, help='Number of states for each node of the graph.')
    parser.add_argument('--vertices', default=25, type=int, help='Number of vertices of the graph.')

    parser.add_argument('--pertub-val', default=0.25, type=float, help='Hyperparameter for PAM-Sampling.')
    parser.add_argument('--drift-prob', default=0.005, type=float, help='Hyperparamter to control drift probability.')

    parser.add_argument('-d', '--dataset', choices=options['dataset'])
    args = parser.parse_args()

    print('args', args)

    options['nodes'] = np.power(2, np.arange(args.nodes_lower, args.nodes_upper))

    # Subset selection of the options to rerun experiments.
    for arg_name in ['dataset', 'experiment_type', 'sync_type', 'nodes', 'sync_period', 'delta', 'aggregation_scheme', 'batch_size']:
        select_option_subset(args, options, arg_name)

    # Make combinations
    combinations = make_filtered_combinations(options)

    experiment_cfg = vars(args)
    log_str('Running {} configurations in total.'.format(len(combinations)))

    for _idx, combination in enumerate(combinations):
        experiment_cfg.update(combination)
        log_str('[{}] - Running the following configuration : {}'.format(str(_idx).zfill(int(math.log10(len(combinations))) + 1), combination))

        if not args.dry_run:
            runnner = ExperimentRunner(**experiment_cfg)
            runnner.run()
