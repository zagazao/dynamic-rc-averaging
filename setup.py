from distutils.core import setup

from setuptools import find_packages

setup(
    name='Dynamic-RC-Extension',
    version='0.1dev',
    packages=find_packages(where='./src'),
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.md').read(),
    package_dir={'': 'src'},
    zip_safe=True,
    scripts=['src/bin/prep_datasets',
             'src/bin/makepgmdataset',
             'src/bin/makeintpgmdataset',
             'src/bin/cluster_dataset.py']
)
