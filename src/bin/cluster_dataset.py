#!/usr/bin/env python

import argparse
import datetime
import os

import numpy as np
import pandas as pd
from sklearn.cluster import KMeans


def log_str(_str):
    print(datetime.datetime.now().strftime('%d/%m/%Y-%H:%M:%S:'), _str)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-k', '--num-cluster', default=16, type=int, help='Number of cluster')
    parser.add_argument('--holdout-per-cluster', default=600, type=int)

    parser.add_argument('-i', '--input-dir', default='/home/lukas/data/dynamic-rc-data', type=str, help='Path to input dataset')
    parser.add_argument('-o', '--output-dir', default='/home/lukas/data/dynamic-rc-data', type=str, help='Path to output dataset')

    parser.add_argument('-p', '--prefix', default='C', type=str, help='Prefix for clustered dataset.')

    parser.add_argument('-d', '--dataset', type=str, help='Dataset to process.')

    args = parser.parse_args()

    df_dir = os.path.join(args.input_dir, args.dataset)
    log_str('Reading data from {}.'.format(df_dir))

    df = pd.read_csv(os.path.join(df_dir, '{}.disc.csv'.format(args.dataset)), header=None, index_col=None)
    df_holdout = pd.read_csv(os.path.join(df_dir, '{}.disc.csv.holdout'.format(args.dataset)), header=None, index_col=None)

    df = pd.concat([df, df_holdout])

    n_cluster = args.num_cluster

    log_str('Fitting k-means with k = {}.'.format(n_cluster))

    k_means = KMeans(n_clusters=n_cluster)
    k_means.fit(df)

    log_str('Computing spatial distribution.')

    cluster_assignments = k_means.labels_
    idx_sorted = np.argsort(cluster_assignments)

    # Count samples per cluster
    bin_counts = np.bincount(cluster_assignments)
    # Select minimum number of assignment along all clusters.
    min_bin_count = np.min(bin_counts)

    out = np.zeros(min_bin_count * n_cluster)
    idx = 0

    for i in range(n_cluster):
        data_avail = bin_counts[i]
        out[i * min_bin_count:i * min_bin_count + min_bin_count] = idx_sorted[idx:idx + min_bin_count]
        idx += data_avail

    idx = out.astype(int).reshape(n_cluster, min_bin_count).T.flatten()

    # Reorder dataframe "rounbrobin" indices
    df = df.iloc[idx]

    n_holdout_new = args.holdout_per_cluster * n_cluster

    out_dir = os.path.join(args.output_dir, f'{args.prefix}{args.dataset}')

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    log_str('Holdout vs. Train [{} - {}]'.format(n_holdout_new, len(df) - n_holdout_new))

    df[:n_holdout_new].to_csv(os.path.join(out_dir, f'{args.prefix}{args.dataset}.disc.csv.holdout'), index=None, header=None)
    df[n_holdout_new:].to_csv(os.path.join(out_dir, f'{args.prefix}{args.dataset}.disc.csv'), index=None, header=None)
