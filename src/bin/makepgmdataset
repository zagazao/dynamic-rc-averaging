#!/usr/bin/env python

import argparse
import os

import numpy as np
import pxpy
from networkx.generators.random_graphs import fast_gnp_random_graph
from tqdm import tqdm

from dlplatform_extension.learner.px_helper import set_weights, network_x_to_edgelist

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--seed', default=876)
    parser.add_argument('--nodes', default=25)
    parser.add_argument('--pedge', default=0.3)
    parser.add_argument('--states', default=10)

    parser.add_argument('--pertub-eps', default=0.1)

    parser.add_argument('--out-file', default='/home/lukas/data/dynamic-rc-data/PGM/PGM.disc.csv')

    parser.add_argument('--samples', default=100000)

    args = parser.parse_args()

    SEED = int(args.seed)

    pxpy.set_seed(SEED)

    n = args.nodes
    p = args.pedge
    g = fast_gnp_random_graph(n, p, seed=SEED)

    edgelist = network_x_to_edgelist(g)

    G = pxpy.create_graph(edgelist)
    states = args.states
    dim = len(edgelist) * states * states
    weights = np.random.RandomState(SEED).rand(dim)

    ctypes_weights = np.ctypeslib.as_array(weights, shape=dim)

    model = pxpy.Model(weights, G, states)

    old_eps = np.zeros(dim)

    gumbel_random_state = np.random.RandomState(SEED)

    print('Dimension of the model is', dim)

    out = args.out_file

    if not os.path.exists(os.path.dirname(out)):
        os.makedirs(os.path.dirname(out))

    with open(out, 'a+') as out_file:
        for i in tqdm(range(args.samples)):
            eps = gumbel_random_state.gumbel(loc=0.0, scale=args.pertub_eps, size=dim)
            set_weights(model, weights + eps)
            sample = model.MAP()
            np.savetxt(out_file, sample, fmt='%u', delimiter=",")
