from typing import List

import numpy as np
import pxpy

from DLplatform.aggregating import Aggregator
from dlplatform_extension.learner.px_helper import set_stats, set_stats_int, generate_model
from dlplatform_extension.paramneu import MuThetaParameter


def accumulate_mu(params: List[MuThetaParameter]):
    _mu = np.zeros(shape=params[0].dim)
    _n_samples = 0
    for theta_mu in params:
        _mu += theta_mu.mu
        _n_samples += theta_mu.mu_samples
    return _mu, _n_samples


def accumulate_theta(params: List[MuThetaParameter]):
    _theta = np.zeros(shape=params[0].dim)
    for param in params:
        _theta += param.theta
    return _theta


"""
Transmit dataset to coordinator, learn model and distribute model
"""


class CentralizedThetaLearner(Aggregator):

    def __init__(self, name="Aggregator"):
        super().__init__(name)

    def calculateDivergence(self, param1: MuThetaParameter, param2: MuThetaParameter):
        """
        Calculate the squared L2-distance between sufficient statistics.
        """
        if type(param1) is not MuThetaParameter or type(param2) is not MuThetaParameter:
            raise TypeError('params have to be of type MuThetaParameter')
        if param1.mu_samples <= 0: # or param2.mu_samples <= 0:
            return 0.0
        diff = np.linalg.norm(param1.mu / param1.mu_samples - param2.mu / max(param2.mu_samples, 1)) ** 2
        return diff


def cleanup_and_extract_w(model):
    _w = np.copy(model.weights)
    model.graph.delete()
    model.delete()
    return _w


class CentralizedMRFThetaMuAverage(CentralizedThetaLearner):

    def __init__(self, structure, states, iterations=250, name="ThetaMuAveraging"):
        """

        :param structure:
        :param states:
        :param iterations:
        :param name:
        """
        Aggregator.__init__(self, name=name)
        self.structure = structure
        self.states = states
        self.iterations = iterations

    def __call__(self, params: List[MuThetaParameter]) -> MuThetaParameter:
        """
        Aggregate mus
        :param params:
        :return:
        """
        # Extract sources
        src = [param.src for param in params]
        mu, num_instances = accumulate_mu(params)

        model = generate_model(np.array([]), self.states, pxpy.ModelType.mrf, self.structure)

        set_stats(model, mu)
        model.num_instances = num_instances

        model = pxpy.train(input_model=model, iters=self.iterations)

        return MuThetaParameter(cleanup_and_extract_w(model), mu, num_instances, src)


class CentralizedIntegerMRFThetaMuAverage(CentralizedThetaLearner):

    def __init__(self, structure, states, iterations=500, k=8, name="IntegerMRFThetaMuAverage"):
        Aggregator.__init__(self, name=name)
        self.structure = structure
        self.states = states
        self.k = k
        self.iterations = iterations

    def __call__(self, params: List[MuThetaParameter]) -> MuThetaParameter:
        src = [param.src for param in params]
        mu, num_instances = accumulate_mu(params)

        model = generate_model(np.array([]), self.states, pxpy.ModelType.integer, self.structure, k=self.k)

        set_stats_int(model, mu.astype(np.uint64))
        model.num_instances = num_instances

        model = pxpy.train(input_model=model, iters=self.iterations, mode=pxpy.ModelType.integer, k=self.k)

        return MuThetaParameter(cleanup_and_extract_w(model).astype(np.int64), mu.astype(np.uint64), num_instances, src)


"""
Weight only average
"""


class ThetaWeightAverage(Aggregator):

    def __init__(self, name="Aggregator"):
        super().__init__(name)

    def calculateDivergence(self, param1: MuThetaParameter, param2: MuThetaParameter):
        """
        Calculate the squared L2-distance between sufficient statistics.
        """
        if type(param1) is not MuThetaParameter or type(param2) is not MuThetaParameter:
            raise TypeError('params have to be of type MuThetaParameter')
        return np.linalg.norm(param1.theta - param2.theta) ** 2


class MrfWeightAverage(ThetaWeightAverage):

    def __init__(self, name="Aggregator"):
        super().__init__(name)

    def __call__(self, params: List[MuThetaParameter]) -> MuThetaParameter:
        # Extract sources
        src = [param.src for param in params]
        theta_new = accumulate_theta(params)
        theta_new = theta_new / len(params)
        return MuThetaParameter(theta_new, None, 0, src)


class IntegerMrfWeightAverage(ThetaWeightAverage):

    def __init__(self, name="Aggregator"):
        super().__init__(name)

    def __call__(self, params: List[MuThetaParameter]) -> MuThetaParameter:
        # Extract sources
        src = [param.src for param in params]
        theta_new = accumulate_theta(params)
        theta_new = np.floor_divide(theta_new, len(params))
        return MuThetaParameter(theta_new.astype(np.int64), None, 0, src)


"""
Accumulate parameters and 
"""


class MuThetaAverageAccumulator(Aggregator):

    def __init__(self, name="Aggregator"):
        super().__init__(name)

    def calculateDivergence(self, param1: MuThetaParameter, param2: MuThetaParameter):
        """
        Calculate the squared L2-distance between sufficient statistics.
        """
        if type(param1) is not MuThetaParameter or type(param2) is not MuThetaParameter:
            raise TypeError('params have to be of type MuThetaParameter')
        euclid_theta = np.linalg.norm(param1.theta - param2.theta) ** 2
        if param1.mu_samples <= 0 or param2.mu_samples <= 0:
            return euclid_theta
        euclid_mu = np.linalg.norm(param1.mu / param1.mu_samples - param2.mu / param2.mu_samples) ** 2
        return euclid_mu + euclid_theta


class MrfMuThetaAverageAccumulator(MuThetaAverageAccumulator):

    def __init__(self, name="Aggregator"):
        super().__init__(name)

    def __call__(self, params: List[MuThetaParameter]) -> MuThetaParameter:
        src = [param.src for param in params]
        theta_new = accumulate_theta(params)
        mu, num_instances = accumulate_mu(params)
        theta_new = theta_new / len(params)

        return MuThetaParameter(theta_new, mu, num_instances, src)


class IntegerMrfMuThetaAverageAccumulator(MuThetaAverageAccumulator):

    def __init__(self, name="Aggregator"):
        super().__init__(name)

    def __call__(self, params: List[MuThetaParameter]) -> MuThetaParameter:
        src = [param.src for param in params]
        theta_new = accumulate_theta(params)
        mu, num_instances = accumulate_mu(params)

        theta_new = np.floor_divide(theta_new, len(params))

        return MuThetaParameter(theta_new.astype(np.int64), mu.astype(np.int64), num_instances, src)
