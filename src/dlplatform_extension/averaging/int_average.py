import copy
from typing import List

import numpy as np
from DLplatform.aggregating import Aggregator
from DLplatform.parameters import Parameters
from DLplatform.parameters.vectorParameters import VectorParameter


class IntegerAverageFast(Aggregator):
    def __init__(self, name="IntegerAverageFast"):
        """

        :param name:
        """
        Aggregator.__init__(self, name=name)

    def calculateDivergence(self, param1, param2):
        if type(param1) is np.ndarray:
            return np.linalg.norm(param1 - param2) ** 2
        else:
            return param1.distance(param2) ** 2

    def __call__(self, params: List[Parameters]) -> Parameters:
        """
        Merges a list of 2^i where i is a natural number, using only integer arithmethic.

        :param params:
        :return:
        """

        # print('Integer Average with {} models.'.format(len(params)))

        if len(params) == 1:
            return params[0]

        if len(params) % 2 != 0:
            raise RuntimeError("List has to be of size 2^i, where i is a natural number. - " + str(len(params)))

        dim = params[0].dim
        out = np.zeros(shape=dim, dtype=np.uint64)
        for vec in params:
            out += vec.get()

        out = out / len(params)
        out = np.floor(out)
        return VectorParameter(out.astype(np.uint64))

    def __str__(self):
        return "IntegerAveragingFast"


# TODO: This recursive aggregartion introduces additional errors, so we use the easy above
class IntegerAverage(Aggregator):
    """
    Provides a method to calculate an averaged model from n individual models (using the integer average operation (a & b) + ((a ^ b) >> 1) )
    """

    def __init__(self, name="IntegerAverage"):
        """

        :param name:
        """
        Aggregator.__init__(self, name=name)

    def calculateDivergence(self, param1, param2):
        if type(param1) is np.ndarray:
            return np.linalg.norm(param1 - param2) ** 2
        else:
            return param1.distance(param2) ** 2

    def __call__(self, params: List[Parameters]) -> Parameters:
        """
        Merges a list of 2^i where i is a natural number, using only integer arithmetic.

        :param params:
        :return:
        """

        # print('Integer Average with {} models.'.format(len(params)))

        if len(params) == 1:
            return params[0]

        if len(params) % 2 != 0:
            raise RuntimeError("List has to be of size 2^i, where i is a natural number. - " + str(len(params)))

        # We want params to be unchanged, so make a copy (only the first half will be overwritten)
        out = copy.deepcopy(params)

        current_len = len(out)
        while current_len > 0:
            for i in range(current_len // 2):
                off = i + current_len // 2
                v1, v2 = out[i].get(), out[off].get()
                out[i] = VectorParameter(np.bitwise_and(v1, v2) + np.right_shift(np.bitwise_xor(v1, v2), 1))  # p2[i] + p2[off]
            current_len = current_len // 2
        return out[0]

    def __str__(self):
        return "IntegerAveraging"


class ErrorIntegerAverage(IntegerAverage):

    def __call__(self, params: List[Parameters]) -> Parameters:
        """
        Merges a list of n parameters using only integer arithmetic.

        :param params:
        :return:
        """

        if len(params) == 0:
            raise RuntimeError("Empty list..")

        out = np.zeros(params[0].dim, dtype=np.uint64)

        for param in params:
            out += param.get()

        out = np.right_shift(out, ff(len(params)))
        return VectorParameter(out)

    def __str__(self):
        return "ErrorIntegerAveraging"


def is_two_power(x):
    return (x & (x - 1)) == 0


def ff(x):
    """
    This function computes the number of bits for a right shift, which has minimal error, if x isn't a power of two.

    Computes the lower and upper power of two for the value x. Afterwards the distance from x to both values is computed.
    Lastly, log2() of the minimum is calculated. This represents the number of bits for a rightshift.
    """
    if is_two_power(x):
        return x
    upper = upper_power_of_two(x)
    lower = upper >> 1

    # This should be no problem with integer only.
    dist_lower = x - lower
    dist_upper = upper - x

    # Same as above..
    if dist_lower < dist_upper:
        return int(np.log2(lower))
    else:
        return int(np.log2(upper))


def upper_power_of_two(v):
    # https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
    v = v - 1
    v |= v >> 1
    v |= v >> 2
    v |= v >> 4
    v |= v >> 8
    v |= v >> 16
    v |= v >> 32
    v = v + 1
    return v
