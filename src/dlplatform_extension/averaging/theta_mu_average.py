from typing import List

import numpy as np
from DLplatform.aggregating import Aggregator

from dlplatform_extension.paramneu import MuThetaParameter


class MRFThetaMuAverage(Aggregator):
    '''
    Provides a method to calculate an averaged model from n individual models (using the arithmetic mean)
    '''

    def __init__(self, name="ThetaMuAveraging"):
        '''

        Returns
        -------
        None
        '''
        Aggregator.__init__(self, name=name)

    def calculateDivergence(self, param1, param2):
        if type(param1) is MuThetaParameter and type(param1) is MuThetaParameter:
            return param1.distance(param2)
        else:
            raise TypeError('params have to be of type MuThetaParameter')

    def __call__(self, params: List[MuThetaParameter]) -> MuThetaParameter:
        """
        This aggregator takes n lists of model parameters and returns a list of component-wise arithmetic means.
         """
        new_mu = np.zeros(shape=params[0].dim)
        new_theta = np.zeros(shape=params[0].dim)
        new_n_samples = 0

        src = []

        for theta_mu in params:
            new_theta += theta_mu.theta
            new_mu += theta_mu.mu
            new_n_samples += theta_mu.mu_samples

            src.extend(theta_mu.src)

        if len(params) > 1:
            new_theta = new_theta / len(params)

        return MuThetaParameter(new_theta, new_mu, new_n_samples, src)

    def __str__(self):
        return "ThetaMuAveraging"


class IntegerMRFThetaMuAverage(Aggregator):
    """
    Provides a method to calculate an averaged model from n individual models (using the arithmetic mean)
    """

    def __init__(self, name="IntegerMRFThetaMuAverage"):
        """

        :param name:
        """
        Aggregator.__init__(self, name=name)

    def calculateDivergence(self, param1, param2):
        if type(param1) is MuThetaParameter and type(param1) is MuThetaParameter:
            return param1.distance(param2)
        else:
            raise TypeError('params have to be of type MuThetaParameter')

    def __call__(self, params: List[MuThetaParameter]) -> MuThetaParameter:
        """

        :param params:
        :return:
        """

        new_mu = np.zeros(shape=params[0].dim)
        new_theta = np.zeros(shape=params[0].dim)
        new_n_samples = 0

        src = []

        for theta_mu in params:
            new_theta += theta_mu.theta
            new_mu += theta_mu.mu
            new_n_samples += theta_mu.mu_samples

            src.extend(theta_mu.src)

        if len(params) > 1:
            new_theta = new_theta / len(params)
            new_theta = np.floor(new_theta)

        return MuThetaParameter(new_theta.astype(np.int64), new_mu.astype(np.int64), new_n_samples, src)

    def __str__(self):
        return "IntegerMRFThetaMuAverage"
