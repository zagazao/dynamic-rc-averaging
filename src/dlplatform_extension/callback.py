import numpy as np
import pxpy

from dlplatform_extension.learner.px_helper import set_weights, generate_model


class MultiCallbackWrapper(object):
    """
    This class allows to use multiple callbacks in one call by forwarding the optimization state to registered callbacks.

    Callbacks can be registered using the constructor or by using the add_callback function.
    """

    def __init__(self, cb_dict):
        self.cb_dict = cb_dict

    def __call__(self, *args, **kwargs):
        for _cb_name, _cb_object in self.cb_dict.items():
            _cb_object(*args, **kwargs)

    def __getitem__(self, item):
        return self.cb_dict[item]

    def add_callback(self, _cb_name, _cb_object):
        self.cb_dict[_cb_name] = _cb_object


# TODO: Implement this..
class LikelihoodLoggerCallback(object):
    def __init__(self):
        self.likelihoods = []


class WeightsLoggerCallback(object):
    """
    Logs the weights during an optimization run.

    Allows the reconstruction of Train / Test Likelihood via @LikelihoodEvaluator
    """

    def __init__(self):
        self.weights = []

    def __call__(self, *args, **kwargs):
        opt_state_p = args[0]
        contents = opt_state_p.contents
        # print(contents.model.obj)
        self.weights.append(np.ascontiguousarray(np.copy(contents.weights)))


class LikelihoodEvaluator(object):
    """
    Evaluates the neg. avg. log-likelihood for a list of weigths..
    """

    def __init__(self, w_list, _edge_list, _states, scale_weights=False):
        # Store the list of weights as np-array
        self.w_list = np.array(w_list, dtype=w_list[0].dtype)

        # If we have an integer model, we rescale the weights...
        if scale_weights:
            self.w_list = np.log(2) * self.w_list

        # Model properties
        self.edge_list = _edge_list
        self.states = _states

    def eval(self, dataset):
        # Generate model for a given structure and state space configuration
        model = generate_model(dataset, self.states, pxpy.ModelType.mrf, self.edge_list)

        # Allocate result
        ll = np.zeros(len(self.w_list))

        # "Replay" training procedure..
        for _idx in range(self.w_list.shape[0]):
            # Select subset
            w_vec = self.w_list[_idx]
            # Update weights
            set_weights(model, w_vec)
            log_likelihood = model.eval_ll()
            ll[_idx] = log_likelihood  # a - np.dot(w_vec, model.statistics)
        # Cleanup
        model.delete()
        return ll
