import numpy as np
import pxpy

from DLplatform.dataprovisioning import DataSource
from DLplatform.dataprovisioning.dataSourceFactory import DataSourceFactory
from dlplatform_extension.learner.px_helper import set_weights, calc_dim


class PGMDataSource(DataSource):

    def __init__(self, edgelist, states, weights, buffersize=100, name="PGMDataSource"):
        super().__init__(name)
        self.model = pxpy.Model(weights, pxpy.create_graph(edgelist), states)
        self.buffersize = buffersize
        self.buffer = None
        self.current = 0

    def prepare(self):
        self.current = 0
        self.buffer = self.model.sample(num_samples=self.buffersize, sampler=pxpy.SamplerType.apx_perturb_and_map, perturbation=1, iterations=5)
        return self

    def getNext(self):
        if self.current == self.buffersize:
            self.prepare()

        x = self.buffer[self.current]
        self.current += 1
        return x, x[0]


class PGMDataSourceFactory(DataSourceFactory):

    def __init__(self, edgelist, states, weights, buffersize=100):
        super().__init__()
        self.edgelist = edgelist
        self.states = states
        self.weights = weights
        self.buffersize = buffersize

    def getDataSource(self, nodeId) -> DataSource:
        return PGMDataSource(self.edgelist, self.states, self.weights, self.buffersize)


class TimeVariantPgmDatasource(DataSource):

    def __init__(self, edgelist, states, pertubation_val=0.1,
                 drift_prob=0.05,
                 pertub_seed=42,
                 weight_seed=87,
                 drift_seed=0,
                 name="TimeVariantPgmDatasource"):
        super().__init__(name)
        self.states = states
        self.edgelist = edgelist
        self.pertubation_val = pertubation_val
        self.dim = calc_dim(self.edgelist, self.states)

        # Setup seeds
        self.pertub_random_state = np.random.RandomState(pertub_seed)
        self.weight_random_state = np.random.RandomState(weight_seed)
        self.drift_random_state = np.random.RandomState(drift_seed)
        # Weights = np.random.
        self.current_weights = self.weight_random_state.gumbel(loc=0.0, scale=1, size=self.dim)
        self.model = pxpy.Model(self.current_weights, pxpy.create_graph(self.edgelist), self.states)

        self.drift_prob = drift_prob

        self.seeds = (pertub_seed, weight_seed, drift_seed)

    def prepare(self):
        return self

    def getNext(self):
        # TODO: Is this a nice parameter sampling distribution?=
        # Check for drift
        if self.drift_random_state.uniform() < self.drift_prob:
            self.current_weights = self.weight_random_state.gumbel(loc=0.0, scale=1, size=self.dim)

        eps = self.pertub_random_state.gumbel(loc=0.0, scale=self.pertubation_val, size=self.dim)
        set_weights(self.model, self.current_weights + eps)
        sample = self.model.MAP()
        return sample[0], sample[0][0]

    def __str__(self) -> str:
        # TODO: Seeds would be interesting.
        return f'TimeVariantPgmDatasource(dim={self.dim}, nodes={len(self.edgelist) + 1}, perturb_val={self.pertubation_val}, seeds={self.seeds}'


class TimeVariantPgmDataSourceFactory(DataSourceFactory):

    def __init__(self, edgelist, states, pertubation_val=0.1,
                 drift_prob=0.05,
                 pertub_seed=42,
                 weight_seed=87,
                 drift_seed=0):
        super().__init__()
        self.edgelist = edgelist
        self.states = states
        self.pertubation_val = pertubation_val
        self.drift_prob = drift_prob
        self.pertub_seed = pertub_seed
        self.weight_seed = weight_seed
        self.drift_seed = drift_seed

    def getDataSource(self, nodeId) -> DataSource:
        return TimeVariantPgmDatasource(self.edgelist, self.states,
                                        self.pertubation_val, self.drift_prob,
                                        self.pertub_seed + nodeId, self.weight_seed, self.drift_seed)
