import itertools
import multiprocessing
import os

import numpy as np
import pandas as pd
from sklearn.cluster import KMeans

from dlplatform_extension.utils import log_str


class SpatialDataSetProvider(object):

    def __init__(self, dataset_dir, dataset_name, num_nodes, prefix='C', target_holdout=10000):
        self.dataset_dir = dataset_dir
        self.dataset_name = dataset_name
        self.num_nodes = num_nodes
        self.prefix = prefix

        self.target_holdout = target_holdout

    def give(self):
        self.assert_base_exists()

        spatial_dataset_name = f'{self.prefix}{self.num_nodes}{self.dataset_name}'
        spatial_dir = os.path.join(self.dataset_dir, spatial_dataset_name)

        if not self.have_to_work():
            spatial_train = os.path.join(spatial_dir, f'{spatial_dataset_name}.disc.csv')
            spatial_holdout = os.path.join(spatial_dir, f'{spatial_dataset_name}.disc.csv.holdout')
            return spatial_train, spatial_holdout

            # df = pd.read_csv(os.path.join(spatial_dir, '{}.disc.csv'.format(spatial_dataset_name)), header=None, index_col=None)
            # df_holdout = pd.read_csv(os.path.join(spatial_dir, '{}.disc.csv.holdout'.format(spatial_dataset_name)), header=None, index_col=None)
            # return df, df_holdout

        # Read original data
        org_dataset_dir = os.path.join(self.dataset_dir, self.dataset_name)
        org_train = os.path.join(org_dataset_dir, f'{self.dataset_name}.disc.csv')
        org_holdout = os.path.join(org_dataset_dir, f'{self.dataset_name}.disc.csv.holdout')

        df = pd.concat([pd.read_csv(_df, header=None, index_col=None) for _df in [org_train, org_holdout]])

        df = self.spatial_round_robin_sort(df)

        # Compute holdout
        num_holdout = self.target_holdout - (self.target_holdout % self.num_nodes)
        log_str('Holdout vs. Train [{} - {}]'.format(num_holdout, len(df) - num_holdout))

        if not os.path.exists(spatial_dir):
            os.makedirs(spatial_dir)

        spatial_train = os.path.join(spatial_dir, f'{spatial_dataset_name}.disc.csv')
        spatial_holdout = os.path.join(spatial_dir, f'{spatial_dataset_name}.disc.csv.holdout')

        df[:num_holdout].to_csv(spatial_holdout, index=None, header=None)
        df[num_holdout:].to_csv(spatial_train, index=None, header=None)

        return spatial_train, spatial_holdout
        # return df[num_holdout:], df[:num_holdout]

    def spatial_round_robin_sort(self, df):
        log_str('Fitting k-means with k = {}.'.format(self.num_nodes))
        k_means = KMeans(n_clusters=self.num_nodes)
        k_means.fit(df)

        log_str('Computing spatial distribution.')

        cluster_assignments = k_means.labels_
        idx_sorted = np.argsort(cluster_assignments)

        # Count samples per cluster
        bin_counts = np.bincount(cluster_assignments)
        # Select minimum number of assignment along all clusters.
        min_bin_count = np.min(bin_counts)

        out = np.zeros(min_bin_count * self.num_nodes)
        idx = 0

        for i in range(self.num_nodes):
            data_avail = bin_counts[i]
            out[i * min_bin_count:i * min_bin_count + min_bin_count] = idx_sorted[idx:idx + min_bin_count]
            idx += data_avail

        idx = out.astype(int).reshape(self.num_nodes, min_bin_count).T.flatten()

        # Reorder dataframe "rounbrobin" indices
        df = df.iloc[idx]
        return df

    def have_to_work(self):
        spatial_dataset_name = f'{self.prefix}{self.num_nodes}{self.dataset_name}'
        spatial_train_dir = os.path.join(self.dataset_dir, spatial_dataset_name)
        if not os.path.exists(spatial_train_dir):
            return True

        spatial_train = os.path.join(spatial_train_dir, f'{spatial_dataset_name}.disc.csv')
        spatial_holdout = os.path.join(spatial_train_dir, f'{spatial_dataset_name}.disc.csv.holdout')

        if not all([os.path.exists(org) for org in [spatial_train, spatial_holdout]]):
            return True
        # If all required files exist, we don't recompute this.
        return False

    def assert_base_exists(self):
        # Check if original dataset exists:
        org_dataset_dir = os.path.join(self.dataset_dir, self.dataset_name)
        if not os.path.exists(org_dataset_dir):
            raise RuntimeError("Base dataset does not exist.")

        org_train = os.path.join(org_dataset_dir, f'{self.dataset_name}.disc.csv')
        org_holdout = os.path.join(org_dataset_dir, f'{self.dataset_name}.disc.csv.holdout')

        if not all([os.path.exists(org) for org in [org_train, org_holdout]]):
            raise RuntimeError("Base dataset files do not exists.")


def fn(dataset_dir, dataset_name, nodes):
    prov = SpatialDataSetProvider(dataset_dir=dataset_dir, dataset_name=dataset_name, num_nodes=nodes)
    prov.give()


if __name__ == '__main__':
    combs = list(itertools.product(*[['COVERTYPE'], [2, 4, 8, 16, 32, 64, 128]]))
    print(list(combs))

    combs = [('/home/lukas/data/dynamic-rc-data/', *comb) for comb in list(combs)]
    print(list(combs))

    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        pool.starmap(fn, combs)
