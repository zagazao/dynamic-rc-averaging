import os

from DLplatform.dataprovisioning.dataSourceFactory import DataSourceFactory

from dlplatform_extension.datasource.standardDataSources import FileDataSource


class FileDataSourceFactory(DataSourceFactory):

    def __init__(self, filename, decoder, numberOfNodes, indices='roundRobin', shuffle=False, cache=False):
        super().__init__()
        self.filename = filename
        self.decoder = decoder
        self.numberOfNodes = numberOfNodes
        self.indices = indices
        self.shuffle = shuffle
        self.cache = cache

    def getDataSource(self, nodeId):
        if nodeId >= self.numberOfNodes:
            raise RuntimeError("nodeId >= self.numberOfNodes. ({} >= {})".format(nodeId, self.numberOfNodes))

        name = os.path.basename(self.filename).split('.')[0]
        dataSource = FileDataSource(filename=self.filename, decodeLine=self.decoder, name=name,
                                    indices=self.indices, nodeId=nodeId, numberOfNodes=self.numberOfNodes,
                                    shuffle=self.shuffle, cache=self.cache)
        return dataSource

    def __str__(self):
        return "File DataSource, filename " + self.filename + ", decoder " + str(self.decoder) + ", data distribution " + self.indices + ", shuffle " + str(
            self.shuffle) + ", cache " + str(self.cache)
