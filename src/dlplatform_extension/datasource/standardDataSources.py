import numpy as np
from DLplatform.dataprovisioning import DataSource

'''
Generic DataSource for reading examples from a file
The function that creates examples with labels is passed as a parameter

'''


class FileDataSource(DataSource):
    def __init__(self, filename, decodeLine, name, indices, nodeId, numberOfNodes, shuffle=False, cache=False):
        DataSource.__init__(self, name=name)

        self._filename = filename
        self._cache = cache
        self._decode_example = decodeLine
        self._indices = getattr(self, indices)(nodeId, numberOfNodes)
        self._shuffle = shuffle
        if self._shuffle:
            np.random.shuffle(self._indices)
        self._examplesCounter = -1
        self._usedExamplesCounter = 0

    def prepare(self):
        if self._cache:
            self._cachedData = []
            for l in open(self._filename, "r").readlines():
                if len(l) > 2:
                    self._cachedData.append(l)
        else:
            self._inputFileHandle = open(self._filename, "r")
        return self

    def readLine(self):
        try:
            current_line = self._inputFileHandle.__next__()
        except StopIteration as _err:
            # if current_line == "\n":
            self._inputFileHandle.close()
            self._inputFileHandle = open(self._filename, "r")
            current_line = self._inputFileHandle.__next__()
            self._examplesCounter = -1
            self.checkEpochEnd()
        return current_line

    def getNext(self):
        if self._cache:
            current_line = self._cachedData[self._indices[self._usedExamplesCounter]]
        else:
            while not self._examplesCounter == self._indices[self._usedExamplesCounter]:
                current_line = self.readLine()
                self._examplesCounter += 1
        self._usedExamplesCounter += 1
        self.checkEpochEnd()
        example = self._decode_example(current_line)
        return example

    def checkEpochEnd(self):
        if self._usedExamplesCounter == len(self._indices):
            self._usedExamplesCounter = 0
            if self._shuffle:
                np.random.shuffle(self._indices)

    def roundRobin(self, nodeIndexNumber, numberOfNodes):
        indices = []
        counter = 0
        with open(self._filename, "r") as fp:
            for line in fp.readlines():
                if len(line) > 2 and counter % numberOfNodes == nodeIndexNumber:
                    indices.append(counter)
                counter += 1
        return indices

    def parallelRun(self, nodeIndexNumber, numberOfNodes):
        indices = []
        counter = 0
        fp = open(self._filename, "r")
        for l in fp.readlines():
            if len(l) > 2:
                indices.append(counter)
            counter += 1
        fp.close()
        return indices
