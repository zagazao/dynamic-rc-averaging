import numpy as np
import pxpy

from DLplatform.learning import IncrementalLearner
from dlplatform_extension.learner.px_helper import calc_dim, set_stats, set_weights, generate_model


def check_likelihoods(likelihood: tuple):
    for l in likelihood:
        if l <= 0.0:
            print(likelihood)
        assert l > 0.0


class BaseMRF(IncrementalLearner):

    def __init__(self, n_states, edge_list, holdout_mu, holdout_instances, opt_budget, batchSize: int, syncPeriod: int, name="IncrementalLearner", identifier=""):
        super().__init__(batchSize, syncPeriod, name, identifier)

        # Model properties
        self.n_states = n_states
        self.edge_list = edge_list

        self.dim = calc_dim(edge_list, n_states)

        # We did not see anything yet
        self.local_mu = np.zeros(self.dim)
        self.global_mu = np.zeros(self.dim)

        # Holdout statistics
        self.holdout_mu = holdout_mu
        self.holdout_instances = holdout_instances

        # This should be instantiated by specific class
        self.model = None

        # Generate evaluation model
        self.evaluation_model = generate_model(np.array([]), n_states, pxpy.ModelType.mrf, edge_list)

        # Counters.
        self.local_samples = 0
        self.global_samples = 0
        self.seen_batches = 0

        self.opt_budget = opt_budget

    def eval_current_likelihoods(self, batch_mu, batch_instances, _weights):
        # TODO: What about global?

        current_ll = self.eval_subset_likelihood(self.local_mu + self.global_mu, self.local_samples + self.global_samples, _weights)

        # current_ll = self.eval_subset_likelihood(self.local_mu, self.local_samples, _weights)
        batch_ll = self.eval_subset_likelihood(batch_mu, batch_instances, _weights)
        holdout_ll = self.eval_subset_likelihood(self.holdout_mu, self.holdout_instances, _weights)
        return current_ll, batch_ll, holdout_ll

    def eval_subset_likelihood(self, _mu, _mu_instances, _weights):
        self.evaluation_model.num_instances = _mu_instances
        set_stats(self.evaluation_model, _mu)
        set_weights(self.evaluation_model, _weights)
        _likelihood = self.evaluation_model.eval_ll()
        return _likelihood

    def predict(self, batch):
        predictions = []
        for _x in batch:
            _x[0] = pxpy.MISSING_VALUE
            self.model.predict(_x)
            predictions.append(_x[0])
        return predictions

    def checkLocalConditionHolds(self) -> (float, bool):
        self._syncCounter += 1
        if self._syncCounter == self._syncPeriod:
            self._syncCounter = 0
            return self._synchronizer.evaluateLocal(self.getParameters(), self._referenceModel)

        return 0, True
