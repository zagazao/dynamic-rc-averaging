import logging
import os
from typing import List

import numpy as np
import pxpy

from DLplatform.learning import learner, LearnerFactory
from DLplatform.learning.learner import Learner
from dlplatform_extension.learner.basemrf import BaseMRF, check_likelihoods
from dlplatform_extension.learner.px_helper import set_weights_int, set_stats_int, generate_model, set_stats, set_weights
from dlplatform_extension.paramneu import MuThetaParameter


class IntegerMRFIncrementalLearner(learner.IncrementalLearner):

    def __init__(self, n_states, structure, k=64, holdout_mu=None, holdout_instances=0,
                 batchSize: int = 32, syncPeriod: int = 1,
                 name="IntegerMRFIncrementalLearner", identifier="0"):
        """

        :param n_states:    Either integer, if all nodes share the same state space. Or np.ndarray, which contains the state space for each node.
        :param structure:   edgelist of the graph: np.array of shape (n_edges, 2)
        :param ref_emp:     Empirical stats of global_emp.  If we pass model.statistics, this is already scaled by num instances, so we can instantiate a model with num_instances = 1
        :param k:           Number of bits for each parameter.
        """
        super().__init__(batchSize, syncPeriod, name, identifier)
        self.k = k

        # Create an empty local model
        self.model = generate_model(np.array([]), n_states, mode=pxpy.ModelType.integer, structure=structure, k=k)

        # We did not see anything yet
        self.local_mu = np.zeros(self.model.dimension)
        self.global_mu = np.zeros(self.model.dimension)

        self.holdout_mu = holdout_mu
        self.holdout_instances = holdout_instances

        self.local_samples = 0
        self.global_samples = 0
        self.seen_batches = 0

        # Base e evaluation model
        self.scaled_model = generate_model(np.array([]), n_states, pxpy.ModelType.mrf, structure=structure)

        # Integer Coordinate Descent Index..
        self.update_counter = 0

    def update(self, data: List) -> List:

        self.seen_batches += 1
        # Extract X
        x = [np.array(record[0]) for record in data]

        batch_mu = self.model.phi_faster(x)
        self.local_mu += batch_mu
        # Set new observations for model
        set_stats_int(self.model, (self.global_mu + self.local_mu).astype(np.uint64))

        # Update local samples for averaging
        self.local_samples += len(data)
        # Update
        self.model.num_instances += len(data)

        # Train for one iteration with newly observed data
        self.model = pxpy.train(input_model=self.model, mode=pxpy.ModelType.integer, iters=self.model.graph.edges, k=self.k, igd_counter=self.update_counter)
        self.update_counter += self.model.graph.edges

        predictions = []

        for _x in x:
            _x[0] = pxpy.MISSING_VALUE
            self.model.predict(_x)
            predictions.append(_x[0])

        _scaled_ll = self.__eval_scaled_ll()
        _batch_ll = self.eval_batch_ll(batch_mu, self._batchSize)
        holdout_ll = self.eval_batch_ll(self.holdout_mu, self.holdout_instances)

        return [(_scaled_ll, _batch_ll, holdout_ll), predictions]

    def __eval_scaled_ll(self):
        self.scaled_model.num_instances = self.model.num_instances
        set_stats(self.scaled_model, (self.global_mu + self.local_mu))
        set_weights(self.scaled_model, self.model.weights * np.log(2))
        ref = self.scaled_model.eval_ll()
        return ref

    def eval_batch_ll(self, mu_batch, num_instances):
        self.scaled_model.num_instances = num_instances
        set_stats(self.scaled_model, mu_batch)
        set_weights(self.scaled_model, self.model.weights * np.log(2))
        batch_likelihood = self.scaled_model.eval_ll()
        return batch_likelihood

    def checkLocalConditionHolds(self) -> (float, bool):
        self._syncCounter += 1
        if self._syncCounter == self._syncPeriod:
            self._syncCounter = 0
            return self._synchronizer.evaluateLocal(self.getParameters(), self._referenceModel)
        return 0, True

    def setParameters(self, param: MuThetaParameter):
        set_weights_int(self.model, param.theta.astype(np.uint64))

    def getParameters(self) -> MuThetaParameter:
        return MuThetaParameter(self.model.weights.astype(np.int64), self.local_mu.astype(np.int64), self.local_samples, self._identifier)


class PassiveIntMRF(BaseMRF):

    def __init__(self, n_states, edge_list, holdout_mu, holdout_instances, opt_budget, k, batchSize: int, syncPeriod: int, name="PassiveIntMRF", identifier=""):
        super().__init__(n_states, edge_list, holdout_mu, holdout_instances, opt_budget, batchSize, syncPeriod, name, identifier)

        self.k = k
        self.model = generate_model(np.array([]), self.n_states, mode=pxpy.ModelType.integer, structure=edge_list, k=k)

    def update(self, data: List) -> List:
        """
        We update statistics and make predictions
        :param data:
        :return:
        """
        self.seen_batches += 1
        # Extract X
        x = [np.array(record[0]) for record in data]

        # Update sufficient stats.
        batch_mu = self.model.phi_faster(x)
        self.local_mu += batch_mu

        # Update local samples for averaging
        self.local_samples += len(data)

        predictions = self.predict(x)
        likelihoods = self.eval_current_likelihoods(batch_mu, len(x), self.model.weights * np.log(2))
        check_likelihoods(likelihoods)

        return [likelihoods, predictions]

    def setParameters(self, param: MuThetaParameter):
        # Gets theta
        set_weights_int(self.model, param.theta.astype(np.uint64))

    def getParameters(self) -> MuThetaParameter:
        # Reports theta
        #  theta, mu, mu_samples, src
        return MuThetaParameter(np.zeros(self.model.dim), self.local_mu, self.local_samples, self._identifier)


class PrivacyIntegerMrf(BaseMRF):

    def __init__(self, n_states, edge_list, holdout_mu, holdout_instances, opt_budget, k, batchSize: int, syncPeriod: int, name="PrivacyIntegerMrf", identifier=""):
        super().__init__(n_states, edge_list, holdout_mu, holdout_instances, opt_budget, batchSize, syncPeriod, name, identifier)

        self.k = k
        self.model = generate_model(np.array([]), self.n_states, mode=pxpy.ModelType.integer, structure=edge_list, k=k)
        self.igd_counter = 0

    def update(self, data: List) -> List:
        self.seen_batches += 1
        # Extract X
        x = [np.array(record[0]) for record in data]

        # Update sufficient stats.
        batch_mu = self.model.phi_faster(x)
        self.local_mu += batch_mu

        # Update local samples for averaging
        self.local_samples += len(data)

        # Set new observations for model
        set_stats_int(self.model, self.local_mu.astype(np.uint64))
        self.model.num_instances = self.local_samples

        # Train for budget with newly observed data
        self.model = pxpy.train(input_model=self.model, mode=pxpy.ModelType.integer, iters=self.opt_budget, k=self.k, igd_counter=self.igd_counter)
        self.igd_counter += self.opt_budget

        predictions = self.predict(x)
        likelihoods = self.eval_current_likelihoods(batch_mu, len(x), self.model.weights * np.log(2))
        check_likelihoods(likelihoods)

        return [likelihoods, predictions]

    def setParameters(self, param: MuThetaParameter):
        set_weights_int(self.model, param.theta.astype(np.uint64))

    def getParameters(self) -> MuThetaParameter:
        return MuThetaParameter(self.model.weights.astype(np.int64), np.zeros(self.model.dim), 0, self._identifier)


class LocalSharedIntegerMrf(BaseMRF):

    def __init__(self, n_states, edge_list, holdout_mu, holdout_instances, opt_budget, k, batchSize: int, syncPeriod: int, name="LocalSharedIntegerMrf", identifier=""):
        super().__init__(n_states, edge_list, holdout_mu, holdout_instances, opt_budget, batchSize, syncPeriod, name, identifier)
        self.k = k
        self.model = generate_model(np.array([]), self.n_states, mode=pxpy.ModelType.integer, structure=edge_list, k=k)
        self.igd_counter = 0

    def update(self, data: List) -> List:
        self.seen_batches += 1
        # Extract X
        x = [np.array(record[0]) for record in data]

        # Update sufficient stats.
        batch_mu = self.model.phi_faster(x)
        self.local_mu += batch_mu

        # Update local samples for averaging
        self.local_samples += len(data)

        # Set new observations for model
        set_stats_int(self.model, (self.local_mu + self.global_mu).astype(np.uint64))
        self.model.num_instances = self.local_samples + self.global_samples

        # Train for budget with newly observed data
        self.model = pxpy.train(input_model=self.model, mode=pxpy.ModelType.integer, iters=self.opt_budget, k=self.k, igd_counter=self.igd_counter)
        self.igd_counter += self.opt_budget

        predictions = self.predict(x)
        likelihoods = self.eval_current_likelihoods(batch_mu, len(x), self.model.weights * np.log(2))
        check_likelihoods(likelihoods)

        return [likelihoods, predictions]

    def setParameters(self, param: MuThetaParameter):
        set_weights_int(self.model, param.theta.astype(np.uint64))

        self.global_mu = param.mu
        self.global_samples = param.mu_samples

        # If our parameters are included, remove those.
        if self._identifier in param.src:
            # Remove my stuff...
            self.global_mu = self.global_mu - self.local_mu
            self.global_samples = self.global_samples - self.local_samples

        set_stats_int(self.model, (self.global_mu + self.local_mu).astype(np.uint64))
        self.model.num_instances = self.global_samples + self.local_samples

    def getParameters(self) -> MuThetaParameter:
        return MuThetaParameter(self.model.weights.astype(np.int64), self.local_mu, self.local_samples, self._identifier)


class IntegerLearnerFactory(LearnerFactory):

    def __init__(self, states, edge_list, model_impl, batch_size=32, sync_period=1, holdout_mu=None, holdout_instances=0, opt_budget=1, k=8):
        super().__init__()
        self.states = states
        self.edge_list = edge_list
        self.k = k
        self.batch_size = batch_size
        self.sync_period = sync_period
        self.holdout_mu = holdout_mu
        self.holdout_instances = holdout_instances

        self.opt_budget = opt_budget

        self.classes = {
            'CENTRALIZED': PassiveIntMRF,
            'PUBLIC': LocalSharedIntegerMrf,
            'PRIVATE': PrivacyIntegerMrf
        }

        self.model_impl = self.classes[model_impl]

    def getLearner(self) -> Learner:
        return self.model_impl(n_states=self.states, edge_list=self.edge_list,
                               holdout_mu=self.holdout_mu, holdout_instances=self.holdout_instances,
                               k=self.k, batchSize=self.batch_size, syncPeriod=self.sync_period,
                               opt_budget=self.opt_budget,
                               identifier=str(os.getpid()))

    def __str__(self):
        return 'IntegerLearnerFactory(impl={})'.format(str(self.model_impl))


class IntegerMRFIncrementalLearnerFactory(LearnerFactory):

    def __init__(self, states, edge_list, k=8, batch_size=32, sync_period=1, holdout_mu=None, holdout_instances=0):
        super().__init__()
        self.n_states = states
        self.edge_list = edge_list

        self.k = k

        self.batch_size = batch_size
        self.sync_period = sync_period
        self.holdout_mu = holdout_mu
        self.holdout_instances = holdout_instances

    def getLearner(self) -> Learner:
        logging.debug('Generating learner.')
        return IntegerMRFIncrementalLearner(self.n_states, self.edge_list, self.k,
                                            self.holdout_mu, self.holdout_instances,
                                            self.batch_size, self.sync_period,
                                            identifier=str(os.getpid()))
