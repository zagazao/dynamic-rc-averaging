import os
from typing import List

import numpy as np
import pxpy

from DLplatform.learning import learner, LearnerFactory
from DLplatform.learning.learner import Learner
from dlplatform_extension.learner.basemrf import BaseMRF, check_likelihoods
from dlplatform_extension.learner.px_helper import set_stats, generate_model, set_weights
from dlplatform_extension.paramneu import MuThetaParameter


class MRFIncrementalLearner(learner.IncrementalLearner):

    def __init__(self, n_states, edge_list, holdout_mu, holdout_instances, batchSize: int, syncPeriod: int, name="MRFIncrementalLearner", identifier="0"):
        """

        :param n_states:  Number of states for each node
        :param edge_list: edgelist of the graph: np.array of shape (n_edges, 2)
        :param ref_emp:  Empirical stats of global_emp.  If we pass model.statistics, this is already scaled by num instances, so we can instantiate a model with num_instances = 1
        """
        super().__init__(batchSize, syncPeriod, name, identifier)

        # Create an empty local model
        # self.n_states = n_states
        self.model = generate_model(np.array([]), n_states, pxpy.ModelType.mrf, edge_list)
        self.batch_evaluation_model = generate_model(np.array([]), n_states, pxpy.ModelType.mrf, edge_list)

        self.mu = np.zeros(self.model.dimension)

        # We did not see anything yet
        self.local_mu = np.zeros(self.model.dimension)
        self.global_mu = np.zeros(self.model.dimension)

        self.holdout_mu = holdout_mu
        self.holdout_instances = holdout_instances

        self.local_samples = 0
        self.global_samples = 0
        self.seen_batches = 0

    def update(self, data: List) -> List:
        self.seen_batches += 1
        # Extract X
        x = [np.array(record[0]) for record in data]

        # Update sufficient stats.
        batch_mu = self.model.phi_faster(x)
        self.local_mu += batch_mu
        # Set new observations for model
        set_stats(self.model, self.global_mu + self.local_mu)

        # Update local samples for averaging
        self.local_samples += len(data)
        # Update
        self.model.num_instances += len(data)

        # Train for one iteration with newly observed data
        self.model = pxpy.train(input_model=self.model, iters=1)

        predictions = []
        for _x in x:
            _x[0] = pxpy.MISSING_VALUE
            self.model.predict(_x)
            predictions.append(_x[0])

        current_ll = self.model.eval_ll()
        batch_ll = self.eval_batch_ll(batch_mu, self._batchSize)
        holdout_ll = self.eval_batch_ll(self.holdout_mu, self.holdout_instances)

        return [(current_ll, batch_ll, holdout_ll), predictions]

    def checkLocalConditionHolds(self) -> (float, bool):
        self._syncCounter += 1
        if self._syncCounter == self._syncPeriod:
            self._syncCounter = 0
            return self._synchronizer.evaluateLocal(self.getParameters(), self._referenceModel)

        return 0, True

    def eval_batch_ll(self, mu_batch, num_instances):
        set_stats(self.batch_evaluation_model, mu_batch)
        set_weights(self.batch_evaluation_model, self.model.weights)
        self.batch_evaluation_model.num_instances = num_instances
        batch_likelihood = self.batch_evaluation_model.eval_ll()
        return batch_likelihood

    def setParameters(self, param: MuThetaParameter):
        set_weights(self.model, param.theta)

    def getParameters(self) -> MuThetaParameter:
        return MuThetaParameter(self.model.weights, self.local_mu, self.local_samples, self._identifier)


class PassiveRegularMRF(BaseMRF):

    def __init__(self, n_states, edge_list, holdout_mu, holdout_instances, opt_budget, batchSize: int, syncPeriod: int, name="IncrementalLearner", identifier=""):
        super().__init__(n_states, edge_list, holdout_mu, holdout_instances, opt_budget, batchSize, syncPeriod, name, identifier)

        self.model = generate_model(np.array([]), self.n_states, pxpy.ModelType.mrf, self.edge_list)

    def update(self, data: List) -> List:
        """
        We update statistics and make predictions
        :param data:
        :return:
        """
        self.seen_batches += 1
        # Extract X
        x = [np.array(record[0]) for record in data]

        # Update sufficient stats.
        batch_mu = self.model.phi_faster(x)
        self.local_mu += batch_mu

        # Update local samples for averaging
        self.local_samples += len(data)

        predictions = self.predict(x)
        likelihoods = self.eval_current_likelihoods(batch_mu, len(x), self.model.weights)
        check_likelihoods(likelihoods)

        return [likelihoods, predictions]

    def setParameters(self, param: MuThetaParameter):
        # Gets theta
        set_weights(self.model, param.theta)

    def getParameters(self) -> MuThetaParameter:
        #  theta, mu, mu_samples, src
        return MuThetaParameter(np.zeros(self.dim), self.local_mu, self.local_samples, self._identifier)


class PrivacyRegularMrf(BaseMRF):

    def __init__(self, n_states, edge_list, holdout_mu, holdout_instances, opt_budget, batchSize: int, syncPeriod: int, name="IncrementalLearner", identifier=""):
        super().__init__(n_states, edge_list, holdout_mu, holdout_instances, opt_budget, batchSize, syncPeriod, name, identifier)

        self.model = generate_model(np.array([]), self.n_states, pxpy.ModelType.mrf, self.edge_list)

    def update(self, data: List) -> List:
        self.seen_batches += 1
        # Extract X
        x = [np.array(record[0]) for record in data]

        # Update sufficient stats.
        batch_mu = self.model.phi_faster(x)
        self.local_mu += batch_mu

        # Update local samples for averaging
        self.local_samples += len(data)

        # Set new observations for model
        set_stats(self.model, self.local_mu)
        self.model.num_instances = self.local_samples

        # Train for one iteration with newly observed data
        self.model = pxpy.train(input_model=self.model, iters=self.opt_budget)

        predictions = self.predict(x)
        likelihoods = self.eval_current_likelihoods(batch_mu, len(x), self.model.weights)
        check_likelihoods(likelihoods)

        return [likelihoods, predictions]

    def setParameters(self, param: MuThetaParameter):
        set_weights(self.model, param.theta)

    def getParameters(self) -> MuThetaParameter:
        return MuThetaParameter(self.model.weights, np.array([]), 0, self._identifier)


class LocalSharedRegularMrf(BaseMRF):

    def __init__(self, n_states, edge_list, holdout_mu, holdout_instances, opt_budget, batchSize: int, syncPeriod: int, name="IncrementalLearner", identifier=""):
        super().__init__(n_states, edge_list, holdout_mu, holdout_instances, opt_budget, batchSize, syncPeriod, name, identifier)

        self.model = generate_model(np.array([]), self.n_states, pxpy.ModelType.mrf, self.edge_list)

    def update(self, data: List) -> List:
        self.seen_batches += 1
        # Extract X
        x = [np.array(record[0]) for record in data]

        # Update sufficient stats.
        batch_mu = self.model.phi_faster(x)
        self.local_mu += batch_mu

        # Update local samples for averaging
        self.local_samples += len(data)

        # Set new observations for model
        set_stats(self.model, self.local_mu + self.global_mu)
        self.model.num_instances = self.local_samples + self.global_samples

        # Train for one iteration with newly observed data
        self.model = pxpy.train(input_model=self.model, iters=self.opt_budget)

        predictions = self.predict(x)
        likelihoods = self.eval_current_likelihoods(batch_mu, len(x), self.model.weights)
        check_likelihoods(likelihoods)

        return [likelihoods, predictions]

    def setParameters(self, param: MuThetaParameter):
        set_weights(self.model, param.theta)

        self.global_mu = param.mu
        self.global_samples = param.mu_samples

        # If our parameters are included, remove those.
        if self._identifier in param.src:
            # Remove my stuff...
            self.global_mu = self.global_mu - self.local_mu
            self.global_samples = self.global_samples - self.local_samples

        set_stats(self.model, self.global_mu + self.local_mu)
        self.model.num_instances = self.global_samples + self.local_samples

    def getParameters(self) -> MuThetaParameter:
        return MuThetaParameter(self.model.weights, self.local_mu, self.local_samples, self._identifier)


class RegularLearnerFactory(LearnerFactory):

    def __init__(self, states, edge_list, model_impl, k=8, batch_size=32, sync_period=1, holdout_mu=None, holdout_instances=0, opt_budget=1):
        super().__init__()
        self.states = states
        self.edge_list = edge_list

        self.batch_size = batch_size
        self.sync_period = sync_period
        self.holdout_mu = holdout_mu
        self.holdout_instances = holdout_instances

        self.opt_budget = opt_budget

        self.classes = {
            'CENTRALIZED': PassiveRegularMRF,
            'PUBLIC': LocalSharedRegularMrf,
            'PRIVATE': PrivacyRegularMrf
        }

        self.model_impl = self.classes[model_impl]

    def getLearner(self) -> Learner:
        return self.model_impl(n_states=self.states, edge_list=self.edge_list,
                               holdout_mu=self.holdout_mu, holdout_instances=self.holdout_instances,
                               batchSize=self.batch_size, syncPeriod=self.sync_period,
                               opt_budget=self.opt_budget,
                               identifier=str(os.getpid()))

    def __str__(self):
        return 'RegularLearnerFactory(impl={})'.format(str(self.model_impl))


class MRFIncrementalLearnerFactory(LearnerFactory):

    def __init__(self, states, edge_list, batch_size=32, sync_period=1, holdout_mu=None, holdout_instances=0):
        super().__init__()
        self.n_states = states
        self.edge_list = edge_list

        self.batch_size = batch_size
        self.sync_period = sync_period
        self.holdout_mu = holdout_mu
        self.holdout_instances = holdout_instances

    def getLearner(self) -> Learner:
        return MRFIncrementalLearner(self.n_states, self.edge_list, self.holdout_mu, self.holdout_instances, self.batch_size, self.sync_period, identifier=str(os.getpid()))
