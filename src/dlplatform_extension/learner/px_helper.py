import numpy as np
import pandas as pd
import pxpy
from networkx import fast_gnp_random_graph


def calc_dim(edge_list, states):
    """
    Computes the dimension for pairwise pgm.
    :param edge_list: ndarray
    :param states:    states for each node
    :return:          dimension of the undirected pgm
    """
    dim = 0
    for s, t in edge_list:
        dim += states[s] * states[t]
    return int(dim)


def generate_model(data, states, mode, structure, k=8):
    """
    Create a new model from the dataset -data-.

    :param data: Dataset
    :param states:  Either an integer or an array which specifies the state space for each node.
    :param mode:    MRF or integer
    :param structure: edgelist
    :param k:       Number of bits for integer model parameters.
    :return:        Model with provided structure, estimated empirical marginals, desired states spaces.
    """
    if isinstance(states, int):
        states = np.full(shape=(data.shape[0]), fill_value=states)

    _states = states.reshape(1, -1) - 1

    if data is None or data.shape[0] == 0:
        _data = _states.astype(np.uint16)
    else:
        _data = np.ascontiguousarray(np.concatenate([data, _states]), dtype=np.uint16)

    _model = pxpy.train(data=_data, mode=mode, graph=pxpy.create_graph(structure), iters=0, k=k)

    # Remove state sample.
    _model.num_instances = _model.num_instances - 1

    _phi_states = _model.phi(_states[0])
    if mode == pxpy.ModelType.mrf:
        _curr_stats = np.ctypeslib.as_array(_model.empirical_stats, shape=(_model.dimension,))
        _curr_stats_cpy = np.copy(_curr_stats)
        _curr_stats_cpy = _curr_stats_cpy - _phi_states
        set_stats(_model, _curr_stats_cpy)
        set_weights(_model, np.zeros(shape=_model.dimension))
    else:
        _curr_stats = np.ctypeslib.as_array(_model.empirical_stats, shape=(_model.dimension,)).view(np.uint64)
        _curr_stats_cpy = np.copy(_curr_stats)
        _curr_stats_cpy = _curr_stats_cpy - _phi_states.astype(np.uint64)
        set_stats_int(_model, _curr_stats_cpy)
        set_weights_int(_model, np.zeros(shape=_model.dimension, dtype=np.uint64))

    return _model


def random_structure(n, p, seed):
    g = fast_gnp_random_graph(n, p, seed=seed)
    edge_list = network_x_to_edgelist(g)
    return edge_list


def generate_bayes_tree(num_nodes):
    """
    Generate a tree, where the target node is connected to each other node, e.g.

       /--x
      /---x
     /----x
    x ----x
     \----x
      \---x
       \--x

    :param num_nodes: Number of nodes for the graph
    :return: edgelist as np.ndarray(shape=(num_nodes-1, 2)
    """
    edge_list = np.zeros(shape=(num_nodes - 1, 2), dtype=np.uint64)
    root = 0
    for edge_idx, node_idx in enumerate(range(1, num_nodes)):
        edge_list[edge_idx] = (root, node_idx)
    return edge_list


def estimate_structure(_holdout):
    """
    Estimates the model structure for the given holdout dataset.
    :param _holdout: dataset, which is used to estimated the graph structure with the chow liu algorithm.
    :param return_mu:
    :return: edgelist of the graph.
    """
    if isinstance(_holdout, pd.DataFrame):
        _data = np.ascontiguousarray(_holdout.values, dtype=np.uint16)
    else:
        _data = np.ascontiguousarray(_holdout, dtype=np.uint16)

    _model = pxpy.train(data=_data, mode=pxpy.ModelType.mrf, graph=pxpy.GraphType.auto_tree, iters=0)
    _edge_list = np.copy(_model.graph.edgelist)
    _model.graph.delete()
    _model.delete()
    return _edge_list


def estimate_mu(_holdout, states, structure):
    """
    Estimate mu for given dataset and given graph structure.
    :param _holdout:    Dataset for which mu should be computed
    :param states:      States for each node of the graph
    :param structure:   Structure of the graph
    :return:            Unnormalized empirical counts
    """
    _model = generate_model(_holdout, states, mode=pxpy.ModelType.mrf, structure=structure)
    holdout_mu = np.copy(np.ctypeslib.as_array(_model.empirical_stats, shape=(_model.dimension,)))
    _model.graph.delete()
    _model.delete()
    return holdout_mu


def calc_stats(_data, _model):
    """
    Computes phi(x) for each sample and returns the sum.

    :param _data: np array of shape (n_samples, n_vars)
    :param _model: PX-Model
    :return: Sum d \in _data : phi(d) # This is not normalized!!
    """
    _stats = np.zeros(_model.dimension)
    for sample in _data:
        _stats += _model.phi(sample)
    return _stats


def set_weights(_model, new_w):
    """

    :param _model:
    :param new_w:
    :return:
    """
    _curr_w = np.ctypeslib.as_array(_model.weights, shape=(_model.dimension,))
    np.copyto(_curr_w, new_w)


def set_stats(_model, new_stats):
    """

    :param _model:
    :param new_stats:
    :return:
    """
    _curr_stats = np.ctypeslib.as_array(_model.empirical_stats, shape=(_model.dimension,))
    np.copyto(_curr_stats, new_stats)


def set_weights_int(_model, new_w):
    """
    Updates the weights for an integer MRF.
    :param _model: PX-Model
    :param new_w:  New weights (dtype???)
    :return:
    """
    _curr_w = np.ctypeslib.as_array(_model.weights, shape=(_model.dimension,)).view(np.uint64)
    np.copyto(_curr_w, new_w)


def set_stats_int(_model, new_stats):
    """
    Updates the empirical stats for an integer MRF.
    :param _model:    PX-Model to update.
    :param new_stats:
    :return:
    """
    _curr_stats = np.ctypeslib.as_array(_model.empirical_stats, shape=(_model.dimension,)).view(np.uint64)
    np.copyto(_curr_stats, new_stats)


def network_x_to_edgelist(_graph):
    _edgelist = np.zeros(shape=(len(_graph.edges), 2), dtype=np.uint64)
    for _idx, _edge in enumerate(_graph.edges):
        _edgelist[_idx] = _edge
    return _edgelist
