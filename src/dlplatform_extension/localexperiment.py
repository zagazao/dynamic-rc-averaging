import os
import time
from enum import Enum
from multiprocessing import Process, Barrier
from uuid import uuid4

from DLplatform.communicating import RabbitMQComm
from DLplatform.coordinator import Coordinator, InitializationHandler
from DLplatform.dataprovisioning import IntervalDataScheduler
from DLplatform.worker import Worker

from dlplatform_extension.logging.TripleLossLogger import TripleLossLogger
# Taken from https://github.com/fraunhofer-iais/dlapplication/blob/master/environments/local_environment/experiment.py
# with slight adjustments..
# - Removed unnecessary attributes, e.g. gpu device selection
# - Added barrier to wait until all workers are ready.
from dlplatform_extension.utils import log_str


class ExperimentState(Enum):
    RUNNING = 1
    WAITING = 2


def logging_time_stamp():
    return time.strftime("%d/%m/%Y-%H:%M:%S")


class LocalExperiment(object):
    def __init__(self, messengerHost, messengerPort, numberOfNodes, sync, aggregator, learnerFactory, dataSourceFactory, stoppingCriterion,
                 initHandler=InitializationHandler(), sleepTime=5):

        self.messengerHost = messengerHost
        self.messengerPort = messengerPort
        self.numberOfNodes = numberOfNodes
        self.sync = sync
        self.aggregator = aggregator
        self.learnerFactory = learnerFactory
        self.dataSourceFactory = dataSourceFactory
        self.stoppingCriterion = stoppingCriterion
        self.initHandler = initHandler
        # UUID might be better with docker
        self._uniqueId = str(uuid4())
        self.sleepTime = sleepTime

        # With the barrier we can sync the start of the processes (good for integer averaging, so we won't end up with only 3 models and increase error.)
        self.b = Barrier(numberOfNodes, timeout=None)

        self.state = ExperimentState.RUNNING

    def run(self, name):
        self.start_time = time.time()
        exp_path = name  # + "_" + self.getTimestamp()
        # print(exp_path)
        if os.path.exists(exp_path):
            exp_path = name + "_" + self.getTimestamp()
            print(logging_time_stamp(), 'Experiment already done')
            return

        os.makedirs(exp_path, exist_ok=True)
        # os.mkdir(exp_path)
        self.writeExperimentSummary(exp_path, name)

        # Start the coordinator.
        t = Process(target=self.createCoordinator, args=(exp_path,), name='coordinator')
        t.start()

        # TODO: Add timeouts to avoid hanging endlessly...

        jobs = [t]
        time.sleep(self.sleepTime)
        for taskid in range(self.numberOfNodes):
            t = Process(target=self.createWorker, args=(taskid, exp_path,), name="worker_" + str(taskid))

            t.start()
            jobs.append(t)
            time.sleep(self.sleepTime)

        GLOBAL_TIME_OUT = 60 * self.numberOfNodes * 10

        # print(GLOBAL_TIME_OUT)

        # Avoid endless hanging of dynamic processes
        # https://stackoverflow.com/questions/26063877/python-multiprocessing-module-join-processes-with-timeout
        start = time.time()
        while time.time() - start <= GLOBAL_TIME_OUT:
            states = [not p.is_alive() for p in jobs]

            pct_done = sum(states) / len(jobs)

            if any(states):
                # If one of the processes finished, we will wait 5 minutes for the rest.
                if self.state == ExperimentState.RUNNING:
                    log_str('Switch to Waiting for finish state.')
                    # Going into waiting state
                    self.state = ExperimentState.WAITING
                    GLOBAL_TIME_OUT = 60 * 5
                    start = time.time()

            if pct_done == 1.0:
                # All the processes are done, break now.
                break

            time.sleep(1)  # Just to avoid hogging the CPU
        else:
            # We only enter this if we didn't 'break' above.
            log_str('timed out, killing all processes')
            for p in jobs:
                p.terminate()
                p.join()

        for job in jobs:
            job.join()
        log_str('Experiment done')

    def createCoordinator(self, exp_path):
        coordinator = Coordinator()
        coordinator.setInitHandler(self.initHandler)
        comm = RabbitMQComm(hostname=self.messengerHost, port=self.messengerPort, user='nutzer32', password='passwort87', uniqueId=self._uniqueId)
        os.mkdir(os.path.join(exp_path, 'coordinator'))
        commLogger = TripleLossLogger(path=os.path.join(exp_path, 'coordinator'), id="communication", level='INFO')
        comm.setLearningLogger(commLogger)
        coordinator.setCommunicator(comm)
        self.sync.setAggregator(self.aggregator)
        coordinator.setSynchronizer(self.sync)
        logger = TripleLossLogger(path=exp_path, id="coordinator", level='INFO')
        coordinator.setLearningLogger(logger)
        coordinator.run()

    def createWorker(self, id, exp_path):
        # print("start creating worker" + str(id))
        nodeId = str(id)  # self._uniqueId + '_' +

        w = Worker(nodeId)
        dataScheduler = IntervalDataScheduler()
        dataSource = self.dataSourceFactory.getDataSource(nodeId=id)
        dataScheduler.setDataSource(source=dataSource)
        w.setDataScheduler(dataScheduler)
        comm = RabbitMQComm(hostname=self.messengerHost, port=self.messengerPort, user='nutzer32', password='passwort87', uniqueId=self._uniqueId)
        os.mkdir(os.path.join(exp_path, "worker" + str(id)))
        commLogger = TripleLossLogger(path=os.path.join(exp_path, "worker" + str(id)), id="communication", level='INFO')
        comm.setLearningLogger(commLogger)
        w.setCommunicator(comm)
        logger = TripleLossLogger(path=exp_path, id="worker" + str(id), level='INFO')
        learner = self.learnerFactory.getLearner()
        learner.setLearningLogger(logger)
        learner.setStoppingCriterion(self.stoppingCriterion)
        self.sync.setAggregator(self.aggregator)
        learner.setSynchronizer(self.sync)
        w.setLearner(learner)

        barrier_id = self.b.wait()
        if barrier_id == 0:
            log_str('Barrier done.\n')

        w.run()

    def getTimestamp(self):
        return time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime(time.time()))

    def writeExperimentSummary(self, path, name):
        outString = "Experiment " + name + " Summary:\n\n"
        outString += "Start:\t" + str(self.start_time) + "\n"
        outString += "Number of Nodes:\t" + str(self.numberOfNodes) + "\n"
        outString += "Learner:\t\t\t" + str(self.learnerFactory) + "\n"
        outString += "Data source:\t\t" + str(self.dataSourceFactory) + "\n"
        outString += "Sync:\t\t\t" + str(self.sync) + "\n"
        outString += "Aggregator:\t\t" + str(self.aggregator) + "\n"
        outString += "Stopping criterion:\t" + str(self.stoppingCriterion) + "\n"
        outString += "Messenger Host:\t\t" + str(self.messengerHost) + "\n"
        outString += "Messenger Port:\t\t" + str(self.messengerPort) + "\n"

        with open(os.path.join(path, "summary.txt"), 'w') as f:
            f.write(outString)
