import os
import time

from DLplatform.learningLogger import LearningLogger


class TripleLossLogger(LearningLogger):

    def __init__(self, path: str, id, level='NORMAL'):
        super().__init__(path, id, level)
        self.log_file_path = os.path.join(self._logpath, self._learnerLossFile)

    def logLearnerTripleLoss(self, lossValue: tuple):
        '''
        Logs loss suffered by a worker

        Parameters
        ----------
        lossValue
        :param lossValue:
        '''
        logFilePath = os.path.join(self._logpath, self._learnerLossFile)
        with open(logFilePath, 'a') as output:
            output.write('%.3f\t%.8f\t%.8f\t%.8f\n' % (time.time(), lossValue[0], lossValue[1], lossValue[2]))

    def logLearnerLoss(self, lossValue: tuple):
        """
        Log the (possible multiple) losses of a learner to a file.
        :param lossValue: 
        :return: 
        """

        with open(self.log_file_path, 'a') as output:
            s = '%.3f' % time.time()
            for elem in lossValue:
                s += '\t%.8f' % elem
            s += os.linesep
            output.write(s)
