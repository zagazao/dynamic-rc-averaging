import numpy
import numpy as np
from DLplatform.parameters import Parameters


class MuThetaParameter(Parameters):

    # Theta,
    # MU
    # Dimension
    # From

    def __init__(self, theta, mu, mu_samples, src):
        self.theta = theta
        self.mu = mu
        self.mu_samples = mu_samples

        self.dim = theta.shape[0]

        if isinstance(src, list):
            self.src = src
        else:
            self.src = [src]

    def getCopy(self):
        return MuThetaParameter(np.copy(self.theta), np.copy(self.mu), self.mu_samples, list(self.src))

    def distance(self, other):
        if not isinstance(other, MuThetaParameter):
            raise ValueError("Other parameters needs to be MuThetaParamter as well, instead it is " + str(type(other)))
        # Calculate distance between theta
        euclid_theta = np.linalg.norm(self.theta - other.theta) ** 2
        return euclid_theta

    def set(self):
        pass

    def get(self):
        pass

    def add(self, other):
        pass

    def scalarMultiply(self, scalar):
        pass

    def toVector(self) -> numpy.array:
        pass

    def fromVector(self, v: numpy.array):
        pass
