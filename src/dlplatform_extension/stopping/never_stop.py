from DLplatform.stopping.stopping_criterion import StoppingCriterion


class NeverStoppingCriterion(StoppingCriterion):
    """
    This stopping criteria stream samples forever.
    """

    def __init__(self, name="NeverStoppingCriterion"):
        StoppingCriterion.__init__(self, name=name)

    def __call__(self, seenExamples: int, currentTimestamp: int) -> bool:
        return False

    def __str__(self):
        return "NeverStoppingCriterion"
