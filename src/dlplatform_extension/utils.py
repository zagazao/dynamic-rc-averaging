import argparse
import datetime
import itertools

from DLplatform.synchronizing import PeriodicSync
from dlplatform_extension.averaging.centralized_average import CentralizedMRFThetaMuAverage, MrfWeightAverage, MrfMuThetaAverageAccumulator, CentralizedIntegerMRFThetaMuAverage, \
    IntegerMrfWeightAverage, IntegerMrfMuThetaAverageAccumulator
from dlplatform_extension.learner.intmrf import IntegerLearnerFactory
from dlplatform_extension.learner.mrf import RegularLearnerFactory
from dlplatform_extension.sync import BinaryHedgeSync
from dlplatform_extension.sync.never import NeverSync

EXPERIMENT_TYPES = ['normal', 'rc']

SYNC_TYPES = ['periodic', 'dynamic', 'nosync']

AGGREGATION_TYPE = ['CENTRALIZED', 'PUBLIC', 'PRIVATE']

learner_factory_selector = {
    'normal': RegularLearnerFactory,
    'rc': IntegerLearnerFactory
}


def get_base_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--root-dir', default='/home/lukas/data/dynamic-rc-data/')
    parser.add_argument('-o', '--out-dir', default='/tmp/dynamic-rc/', type=str, help='Base directory for results.')

    # RabbitMQ setup.
    parser.add_argument('--rabbitmq-host', default='localhost', type=str, help='IP-Address of RabbitMQ-Host.')
    parser.add_argument('--rabbitmq-port', default=5672, type=int, help='Port of RabbitMQ-Host.')

    # General config
    parser.add_argument('-e', '--experiment-type', choices=EXPERIMENT_TYPES)

    parser.add_argument('-m', '--max-samples', default=10000, type=int, help='Number of streamed samples for each learner.')
    parser.add_argument('-b', '--batch-size', nargs='*', default=[32], type=int)

    parser.add_argument('--budget', nargs='*', default=[1], type=int, help='Optimization budget for updates.')

    # Sync config
    parser.add_argument('-t', '--delta', nargs='*', default=[1.0], type=float)
    parser.add_argument('-s', '--sync-type', nargs='*', choices=SYNC_TYPES, default=None)
    parser.add_argument('-p', '--sync-period', nargs='*', default=[1], type=int)

    # This flag allows to override lower and upper
    parser.add_argument('-n', '--nodes', default=None, type=int)
    parser.add_argument('-l', '--nodes-lower', default=0, type=int, help='Exponent for nodes lower.')
    parser.add_argument('-u', '--nodes-upper', default=5, type=int, help='Exponent for nodes upper.')

    parser.add_argument('--aggregation-scheme', nargs='*', default=AGGREGATION_TYPE)

    # Debug
    parser.add_argument('--dry-run', action='store_true', help='Log configurations, does not execute the experiments.')
    return parser


def select_option_subset(_args, _options, _key):
    if getattr(_args, _key) is not None:
        if isinstance(getattr(_args, _key), list):
            _options[_key] = getattr(_args, _key)
        else:
            _options[_key] = [getattr(_args, _key)]


def make_combinations(options_dict):
    # Dict
    keys = options_dict.keys()
    values = (options_dict[key] for key in keys)
    combinations = [dict(zip(keys, combination)) for combination in itertools.product(*values)]
    return combinations


def filter_non_used_deltas(_combinations):
    for combination in _combinations:
        # Filter out deltas for periodic and no sync
        if combination['sync_type'] in ['nosync']:  # Also allow dynamic with periods 'dynamic',
            combination['sync_period'] = 1

        if combination['sync_type'] in ['periodic', 'nosync']:
            combination['delta'] = 0.0

    # Remove the generated duplicates
    _combinations = [dict(t) for t in {tuple(d.items()) for d in _combinations}]

    # Filter out nonsense
    _combinations = [c for c in _combinations if not (c['aggregation_scheme'] == 'CENTRALIZED' and c['sync_type'] == 'nosync')]

    # Make a nice order.
    _combinations = sorted(_combinations, key=lambda d: (d['dataset'], d['experiment_type'], d['sync_type']))  # , d['nodes'], d['delta']
    return _combinations


def make_filtered_combinations(options_dict):
    _combinations = make_combinations(options_dict)
    _combinations = filter_non_used_deltas(_combinations)
    return _combinations


def get_sync_op(_sync_str, _delta=0.0):
    if _sync_str not in ['nosync', 'periodic', 'dynamic']:
        raise RuntimeError('Invalid sync string:', _sync_str)
    if _sync_str == 'nosync':
        return NeverSync()
    if _sync_str == 'periodic':
        return PeriodicSync()
    if _sync_str == 'dynamic':
        return BinaryHedgeSync(_delta)


def get_averaging_op(_mode, _agg):
    ops = {
        'normal': {
            'CENTRALIZED': CentralizedMRFThetaMuAverage,
            'PRIVATE': MrfWeightAverage,
            'PUBLIC': MrfMuThetaAverageAccumulator, },
        'rc': {
            'CENTRALIZED': CentralizedIntegerMRFThetaMuAverage,
            'PRIVATE': IntegerMrfWeightAverage,
            'PUBLIC': IntegerMrfMuThetaAverageAccumulator, }}
    return ops[_mode][_agg]


def log_str(_str):
    print(datetime.datetime.now().strftime('%d/%m/%Y-%H:%M:%S:'), _str)
